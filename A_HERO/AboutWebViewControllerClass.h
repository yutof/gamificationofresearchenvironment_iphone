//
//  AboutWebViewControllerClass.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 10/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * AboutWebViewControllerClass class. 
 * Manage an instance of UIWebview class
 * that displays a webpage in a screen
 * display the website for mobile device
 * within the application.
 *******************************************/

#import <UIKit/UIKit.h>

@interface AboutWebViewControllerClass : UIViewController<UIWebViewDelegate>
{
     UIWebView *aboutApp;
    
}

@property(retain, nonatomic)UIWebView *aboutApp;

-(void)setUpAboutView;
-(void)reloadRequest;
@end
