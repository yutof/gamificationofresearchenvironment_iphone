//
//  A_HEROAppDelegate.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 10/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * Delegete class. 
 * Store and initialize default values for 
 * application variables. Initialize the 
 * application at launch time.
 * It also checkes whether there is unsubmitted
 * reports or not
 *******************************************/
#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "ReachabilityUpdator.h"
#import "SubmissionRequestor.h"

@interface A_HEROAppDelegate : NSObject <UIApplicationDelegate, UINavigationControllerDelegate> {
    UIWindow *window;
    MenuViewController *menu;
    BOOL reporting;
    UINavigationController *rootNav;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) MenuViewController *menu;
@property (nonatomic, retain)UINavigationController *rootNav;

-(void)initVariables;
-(void)saveVariables;
-(void)pendingCheck_AND_update;

#pragma mark Navigation control methods
-(void)activateGameView;
-(void)updateReportTreeID;
@end
