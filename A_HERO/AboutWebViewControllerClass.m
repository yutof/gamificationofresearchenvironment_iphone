//
//  AboutWebViewControllerClass.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 10/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * AboutWebViewControllerClass class. 
 * Manage an instance of UIWebview class
 * that displays a webpage in a screen
 * display the website for mobile device
 * within the application.
 *******************************************/


#import "AboutWebViewControllerClass.h"

@implementation AboutWebViewControllerClass
@synthesize aboutApp;


-(void)dealloc
{
if(aboutApp)
    [aboutApp release];
    
    [super dealloc];
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setUpAboutView];
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setUpAboutView
{

    UIView *contentView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
	contentView.backgroundColor = [UIColor whiteColor];
	
	// important for view orientation rotation
	contentView.autoresizesSubviews = YES;
	contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);	
	self.view = contentView;
	[contentView release];
	
	CGRect webFrame = [[UIScreen mainScreen] applicationFrame];
	self.aboutApp = [[UIWebView alloc] initWithFrame:webFrame];
	self.aboutApp.backgroundColor = [UIColor whiteColor];
	self.aboutApp.scalesPageToFit = NO;//YES;
	self.aboutApp.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	self.aboutApp.delegate = self;
	
	[self.view addSubview: self.aboutApp];
    [self.aboutApp loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://yourdomain.com/setsuden/"]]];


    UIImage *refreshImg = [UIImage imageNamed:@"Refresh_smaller.png"];	
    UIButton *reload =  [[UIButton alloc] initWithFrame:CGRectMake(0, 0, refreshImg.size.width-15, refreshImg.size.height-15)];
    reload.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    reload.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [reload setBackgroundImage:refreshImg forState:UIControlStateNormal];
    [reload addTarget:self action:@selector(reloadRequest) forControlEvents:UIControlEventTouchDown];
    reload.alpha = 0.8;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:reload];
    [self navigationItem].rightBarButtonItem = barButton;
    
}


- (void)viewDidUnload
{
    if(aboutApp)
        [aboutApp release];
    
    
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark Reload operation
-(void)reloadRequest{
 [self.aboutApp reload];
}
 @end
