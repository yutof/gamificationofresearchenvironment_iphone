//
//  ReportList.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReportList class. 
 * Provides GUI for table that lists the
 * reports of requested type
 *******************************************/

#import "ReportList.h"

@implementation ReportList

@synthesize reportListingTable;
@synthesize currentTag;
@synthesize reportArray;
@synthesize gameView;


-(void)dealloc{
    if(reportArray)
    {
        [reportArray release];
    }
    
    if(reportListingTable)
    [reportListingTable release];


    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(id)initWithReportListTag:(enum ReportListTag)tag
{
    currentTag = tag;
    [self setUpReportList];
    return self;
}
-(void)setUpReportList
{

    SQLManager *sqlmngr = [[[SQLManager alloc]init]autorelease];
    self.reportArray = [sqlmngr getReportList:currentTag];//[[ NSMutableArray alloc] init];
    
    self.reportListingTable = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStyleGrouped];	
	self.reportListingTable.delegate = self;
	self.reportListingTable.dataSource = self;
	self.reportListingTable.autoresizesSubviews = YES;
	[self.view addSubview:self.reportListingTable];
	self.reportListingTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 00.0, 320.0, 90.0)];
}

-(void)resetUpReportList:(enum ReportListTag)tag
{
    SQLManager *sqlmngr = [[[SQLManager alloc]init]autorelease];
    if(self.reportArray)
        {
            [self.reportArray release];
            self.reportArray = nil;
        }
    currentTag = tag;
    self.reportArray = [sqlmngr getReportList:currentTag];//[[ NSMutableArray alloc] init];
    [self.reportListingTable reloadData];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




#pragma mark UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.reportArray count];
    
}




// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"cell";
    
    ReportListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ReportListCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
        
    if(indexPath.row < [self.reportArray count])
    {
        
        ReportItem *repo = [self.reportArray objectAtIndex:indexPath.row];
        NSString* reponame = repo.name;
        if([reponame length]== 0)
            reponame = @"No Name";
        cell.textLabel.text = [NSString stringWithFormat:@"By %@", reponame];
        [cell setData:repo];
    }
        
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = @"";
    return title;
	
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row < [self.reportArray count])
    {
        ReportItem *repo = [self.reportArray objectAtIndex:indexPath.row];
        
        if(!gameView)
            gameView = [GameViewController getInstance];
        
        if([gameView reorganizeForReportID:repo])
        [self.navigationController pushViewController:gameView animated:YES];
        else
        {
            UIAlertView *alert;
            alert = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry. Report detail information could not be extracted." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];      
            
            [alert show];
            [alert release];
            
            SQLManager *sq = [[[SQLManager alloc] init] autorelease];
            NSString* statement = [NSString stringWithFormat:@"DELETE FROM ReportList WHERE id=%d;", repo.reportID];
            [sq query:statement];
            
        }
    }
        
}//end of didSelect


@end
