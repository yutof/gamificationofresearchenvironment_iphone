//
//  ReportList.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReportList class. 
 * Provides GUI for table that lists the
 * reports of requested type
 *******************************************/

#import <UIKit/UIKit.h>
#import "SQLManager.h"
#import "ReportItem.h"
#import "GameViewController.h"
#import "ReportListCell.h"

enum ReportListTag{
    RL_General=0,
    RL_Stepwise
};

@interface ReportList : UIViewController<UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate>
{
    UITableView *reportListingTable;
    enum ReportListTag currentTag;
    NSMutableArray *reportArray;
    GameViewController *gameView;
}
@property(retain, nonatomic) UITableView *reportListingTable;
@property(retain, nonatomic) NSMutableArray *reportArray;
@property enum ReportListTag currentTag;
@property(retain, nonatomic) GameViewController *gameView;

-(id)initWithReportListTag:(enum ReportListTag)tag;
-(void)setUpReportList;
-(void)resetUpReportList:(enum ReportListTag) tag;
@end
