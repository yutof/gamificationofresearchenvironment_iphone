//
//  GameConfigurationController.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/5/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameStageController.h"
/*
enum uiButtonTag{
    upButtonTag =0,
    downButtonTag
};
*/



@protocol GameConfigurationDelegate;
@class GameStageController;
@interface GameConfigurationController : UIViewController 
<UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate, GameStageControllerDelegate, UITextFieldDelegate > 
{
    	UITableView *gameCfgTableView;  
    	UISlider	*noNodesSlider;
        UIButton    *upButton, *downButton;
        UILabel     *nodeNumLabel, *stageNumLabel;
        NSArray     *levelArray;//, *cfmArray;
        UISwitch    *gameCenterSwitch;
        GameStageController *stgController;
        id <GameConfigurationDelegate> delegate;
        UISwitch *autoReportingSwitch;
        UITextField *reporterName;
}
@property (retain, nonatomic) 	UITableView *gameCfgTableView;  
@property (retain, nonatomic)   UISlider	*noNodesSlider;
@property (retain, nonatomic)   UILabel *nodeNumLabel, *stageNumLabel;;
@property (retain, nonatomic)   NSArray *levelArray;//,*cfmArray;
@property (retain, nonatomic)   UIButton    *upButton, *downButton;
@property (retain, nonatomic)   UISwitch    *gameCenterSwitch;//, *confirmationSwitch;
@property (nonatomic, assign) id <GameConfigurationDelegate> delegate;

@property(retain, nonatomic) GameStageController *stgController;

@property(retain, nonatomic) UISwitch *autoReportingSwitch;
@property(retain, nonatomic) UITextField *reporterName;

-(void)setUpCfgView;
-(void)updateNodesNum;
-(void)moveNodeNum:(id)sender;
-(void)gameCenterSwichChanged;
-(void)autoReportSwitchChamged;
-(void)randomStageEnabled:(BOOL)tag;
-(void)updateStageNumLabel;
-(void)updateNodesNum_ForStage;
//-(void)confirmationSwichChanged;

-(void)updateStageTable;
+(void)setNodeNumForStage;
@end

@protocol GameConfigurationDelegate <NSObject>
@optional
-(void)gameCenterConfigChanged:(GameConfigurationController*)gm_cfg;

@end

