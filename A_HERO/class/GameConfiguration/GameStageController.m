//
//  GameStageController.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 8/19/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//


#import "GameStageController.h"
#import "Constants.h"
@implementation GameStageController
@synthesize gameStgTableView, delegate;

@synthesize /*checkMark,*/ question, currentStage;
@synthesize mgr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self initStages];
    }
    return self;
}

-(void)releaseObjects
{
    if(gameStgTableView)   
    {
        [gameStgTableView release];
        gameStgTableView = nil;

    }
  
    for(int i =0; i< 4; i++)
        medalArray[i] = nil;
    
    question = nil;
    currentStage = nil;

    if(mgr)
    [mgr release];


}

-(void)initStages
{
        self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    //self.view.backgroundColor = [UIColor brownColor];
    
self.gameStgTableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStyleGrouped];	
	self.gameStgTableView.delegate = self;
	self.gameStgTableView.dataSource = self;
	self.gameStgTableView.autoresizesSubviews = YES;
    self.gameStgTableView.rowHeight = 50.0f;
	[self.view addSubview:self.gameStgTableView];
	self.gameStgTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 90.0)];
	[self.gameStgTableView release];
    
//    self.checkMark = [UIImage imageNamed:@"CHECK-MARK-GREEN.gif"];
    
    medalArray[BronzMedal] = [UIImage imageNamed:@"Medal_Bronz.png"];
    medalArray[SilberMedal] = [UIImage imageNamed:@"Medal_Silver.png"];
    medalArray[GoldenMedal] = [UIImage imageNamed:@"Medal_Golden.png"];
    medalArray[EmeraldBlueMedal] = [UIImage imageNamed:@"Medal_EmeraldBlue.png"];
    
    self.question = [UIImage imageNamed:@"Question.png"];
    self.currentStage = [UIImage imageNamed:@"red_circle.png"];
 //   prevCleared = false;
    
    /*
    dispacth_group_t group = disptach_group_create();
    disptach_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    unsigned int i = 20;
    while (i--) {disptch_group_async(group, queue, block);}
    */
    
  //  int i;// =0;
    dispatch_block_t block = ^{
        /*for(int i=0; i<HighestStageNumber; i++)
        {
            status[i] = [[UIImageView alloc] initWithImage:self.question];
        }*/

        
        [self copyStageInfo];
        


//    NSLog(@"block ended");
        
    };
 //     dispatch_block_t block2 = ^{
 //   };
    //NSLog(@"%@", [NSThread currentThread]);
    
    dispatch_group_t grp = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_group_async(grp, queue, block);
    
//    for(i =0; i< MAX_Node_Num; i++)
//     {       }
//    dispatch_group_async(grp, queue, block2);
/*dispatch_async(queue, ^{});*/
    
    dispatch_group_wait(grp, DISPATCH_TIME_FOREVER);
    
    /*
    for(int i=0; i<HighestStageNumber; i++)
    {
        
            status[i].image = self.checkMark;
        
        
    }*/
   
}

-(void)copyStageInfo
{
//    BOOL stageCleared[100];

    if(!mgr)
    {
    self.mgr = [[SQLManager alloc] init];
    }
    [mgr copyStageResult:clearedStage score:clearedScore ];
    /*
    for(int i=0; i<HighestStageNumber; i++)
    {
        
        if(!stageCleared[i])
        status[i].image = self.question; 
        else
        status[i].image = self.checkMark;
        
        NSLog(@"Stage cleared? %d", stageCleared[i]);
        
    }*/

    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidUnload
{
    [self releaseObjects];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)dealloc{

    [self releaseObjects];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;//was 3
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return 100;
            break;
            
        default:
            return 1;
            break;
    }
}

/*
 - (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return UITableViewCellEditingStyleNone;
 }*/





// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        NSArray *cellIdentifier = [[[NSArray alloc] initWithObjects:@"cell1",@"cell2",   nil] autorelease];
      
    
    //UITableViewCell 
    GameStageCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifier objectAtIndex:indexPath.section]];
    if (cell == nil) {
        //cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:[cellIdentifier objectAtIndex:indexPath.section]] autorelease];
        cell = [[[GameStageCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[cellIdentifier objectAtIndex:indexPath.section]]autorelease];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }//end of if
    
    //[cell.contentView removeFromSuperview];
    /*
    NSArray *subviews = [[NSArray alloc] initWithArray:cell.contentView.subviews];
	for (UIView *subview in subviews) {
		[subview removeFromSuperview];
	}
	[subviews release];
    */
    /*
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }*/
    
	if(indexPath.section == 0)
    {
    if(stageNum == -1)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else 
        cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = @"Random Stage";
    }
    else
    {
    
        if(indexPath.row == stageNum)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
//        [cell removeFromSuperview];
//        [cell.contentView removeFromSuperview];
//        [cell.contentView addSubview:status[indexPath.row]];
//        status[indexPath.row].center = markCenter;
//        cell.imageView.frame = cell.frame;//CGRectMake(0, 0, 300.0, 100.0);

        if(clearedStage[indexPath.row])
        {
            //cell.imageView.image = self.checkMark;//status[indexPath.row].image;
            if(clearedScore[indexPath.row] >0)
                cell.imageView.image = medalArray[EmeraldBlueMedal];
            else if (clearedScore[indexPath.row] == 0)
                cell.imageView.image = medalArray[GoldenMedal];
            else if(clearedScore[indexPath.row] > -15.0)
                cell.imageView.image = medalArray[SilberMedal];
            else
                cell.imageView.image = medalArray[BronzMedal];
            
            cell.imageView.alpha = 1;
            cell.textLabel.alpha = 1;
            if(indexPath.row > lastClearedStage)
                lastClearedStage = indexPath.row;
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^{
                //UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
                
                if(!self.mgr)
                {
                self.mgr = [[SQLManager alloc] init] ;
                }
                //NSLog(@"hash check");
                NSString *hash = [mgr getText:[NSString stringWithFormat:@"SELECT secretHash FROM STAGE WHERE Stage_ID=%d AND secretHash IS NOT NULL",indexPath.row]];
               // NSString *getStoredHash = 
                
            
                if([hash length] >0 && hash != NULL)
                {
                    
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                   
                    
                    [cell setData:hash percentage:[NSString stringWithFormat:@"%2.4f%%",clearedScore[indexPath.row]]];
                    cell.passwordLabel.hidden = NO;
                    cell.percentageLabel.hidden = NO;                    
                    
                });
                
                } else
                {
                    if(cell.passwordLabel)
                    {
                        cell.passwordLabel.hidden = YES;
                        cell.percentageLabel.hidden = YES;
                    }
                
                }
                
                
            });
            
            //prevCleared = YES;
        }
        else if(indexPath.row == lastClearedStage+1)
        {
            cell.imageView.image = self.currentStage;//status[indexPath.row].image;
            cell.imageView.alpha = 1;
            cell.textLabel.alpha = 1;
            
            if(cell.passwordLabel)
            {
                cell.passwordLabel.hidden = YES;
                cell.percentageLabel.hidden = YES;
            }

            //prevCleared = NO;
        }
        else
        {
            
            cell.imageView.image = self.question;
            cell.alpha = 0.5;
            cell.imageView.alpha = 0.5;
            cell.textLabel.alpha = 0.5;
            
            if(cell.passwordLabel)
            {
                cell.passwordLabel.hidden = YES;
                cell.percentageLabel.hidden = YES;
            }

        }
        
        //cell.imageView.center = cell.center;//CGPointMake(100.0, 30.0);
        
    cell.textLabel.text = [NSString stringWithFormat:@"Stage No.   %d", indexPath.row + 1];
    }
        
    
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
  	//NSLog(@"titleForHeaderInSection method in  SettingTableViewController.m is properly called");     
    NSString *title = nil;
    switch (section) {
        case 0:
            title = NSLocalizedString(@"Random Stage", @"randomStage");
            break;
        case 1:
            title = NSLocalizedString(@"Regular Stage", @"regStage");
            break;
            
        default:
            break;
    }
    return title;
	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;                
   
    if(indexPath.section == 1   && indexPath.row <= lastClearedStage + 1)
    {
        if(stageNum != -1)
        cell = [self.gameStgTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:stageNum inSection:1]];
        else
        cell = [self.gameStgTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //NSLog(@"stage num? %d section should be 1=%d? and row %d",stageNum, indexPath.section, indexPath.row);
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell = [self.gameStgTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:1]];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        stageNum = indexPath.row;
   //     [tableView reloadData];

    }
    else if(indexPath.section == 0 && stageNum != -1)
    {
        cell = [self.gameStgTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:stageNum inSection:1]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell = [self.gameStgTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        stageNum = -1;
        //NSLog(@"section should be 0=%d? and row %d",indexPath.section, indexPath.row);
    }
    //[self initRandomSeed:stageNum];
    [[self delegate]stageChanged:self];
    
    
}//end of didSelect


#pragma mark GUI update
-(void)updateStageInfo_AND_ReloadTable
{
    [self copyStageInfo];
    [self.gameStgTableView reloadData];
    
    if(stageNum > lastClearedStage)
        lastClearedStage = stageNum;
    
//    NSLog(@"last clearedStage Num %d", lastClearedStage);
}
/*
#pragma mark Utility methods
-(void)initRandomSeed:(int)seed
{
if(seed < 0)
    srandom(time(NULL));
else
    srandom(seed);
    
}*/

@end
