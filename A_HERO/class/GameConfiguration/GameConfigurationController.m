//
//  GameConfigurationController.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/5/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "GameConfigurationController.h"
#import "Constants.h"
#import "GameStageController.h"


@implementation GameConfigurationController
@synthesize gameCfgTableView, noNodesSlider, levelArray, nodeNumLabel;//, cfmArray;
@synthesize upButton, downButton, stageNumLabel, stgController;;
@synthesize gameCenterSwitch, delegate;//, confirmationSwitch;

@synthesize reporterName, autoReportingSwitch;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setUpCfgView];
        
    }
    return self;
}

- (void)dealloc
{
    
    if(gameCfgTableView)
        [gameCfgTableView release];
    
    if(noNodesSlider)
        [noNodesSlider release];
    
    if(levelArray)
        [levelArray release]; 
    
    if(nodeNumLabel)
        [nodeNumLabel release];
    
    if(gameCenterSwitch)
        [gameCenterSwitch release];
    if(upButton)
        [upButton release];
    if(downButton)
        [downButton release];
    
    
    if(stageNumLabel)
        [stageNumLabel release];
    
    if(stgController)
        [stgController release];
        
    
    if(reporterName)
        [reporterName release];
    
    if(autoReportingSwitch)
        [autoReportingSwitch release];
    
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle

-(void)setUpCfgView
{
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
	self.gameCfgTableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStyleGrouped];	
	self.gameCfgTableView.delegate = self;
	self.gameCfgTableView.dataSource = self;
	self.gameCfgTableView.autoresizesSubviews = YES;
	[self.view addSubview:self.gameCfgTableView];
	self.gameCfgTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 90.0)];

    CGRect frame = CGRectMake(0.0, 0.0, 170.0, 30.0);
    self.noNodesSlider = [[UISlider alloc] initWithFrame:frame];
    [self.noNodesSlider addTarget:self action:@selector(updateNodesNum) forControlEvents:UIControlEventValueChanged];
    self.noNodesSlider.backgroundColor = [UIColor clearColor];
        
    self.noNodesSlider.minimumValue = Min_Node_Num;//3.0;
    self.noNodesSlider.maximumValue = MAX_Node_Num;//31.0;
    self.noNodesSlider.continuous = YES;
    
    self.noNodesSlider.value = NextTree_NoNodes;  
    [self.noNodesSlider setAccessibilityLabel:NSLocalizedString(@"StandardSlider", @"")];
		
	self.nodeNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 30.0)];
    self.nodeNumLabel.font = [UIFont systemFontOfSize:20.0];
	self.nodeNumLabel.textAlignment = UITextAlignmentCenter;
    self.nodeNumLabel.text = [NSString stringWithFormat:@"%d", NextTree_NoNodes];
	self.nodeNumLabel.backgroundColor = [UIColor clearColor];
	self.nodeNumLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;

	self.stageNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 260.0, 30.0)];
    self.stageNumLabel.font = [UIFont systemFontOfSize:20.0];
	self.stageNumLabel.textAlignment = UITextAlignmentLeft;
    if(stageNum > -1)
    self.stageNumLabel.text = [NSString stringWithFormat:@"Stage No.%d", stageNum+1];
    else
    self.stageNumLabel.text = [NSString stringWithFormat:@"Random Stage"];        
    
	self.stageNumLabel.backgroundColor = [UIColor clearColor];
	self.stageNumLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;

    
    self.levelArray = [[NSArray alloc] initWithObjects:@"Lv.3 Hard", @"Lv.2 Medium", @"Lv.1 Easy", nil];
                     
    CGRect openButtonFrame = CGRectMake(0.0, 0.0, 20.0f, 20.0f);
    
    UIImage *btnImage = [UIImage imageNamed:@"up.png"];
    
    UIButton *btn1 =  [[UIButton alloc] initWithFrame:openButtonFrame];
    btn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    btn1.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(moveNodeNum:) forControlEvents:UIControlEventTouchDown];
    btn1.alpha = 0.8;
    self.upButton = btn1;
    [btn1 release];
    
    btnImage = [UIImage imageNamed:@"down.png"];
    UIButton *btn2 =  [[UIButton alloc] initWithFrame:openButtonFrame];
    btn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    btn2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [btn2 setBackgroundImage:btnImage forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(moveNodeNum:) forControlEvents:UIControlEventTouchDown];
    btn2.alpha = 0.8;
    self.downButton = btn2;
    
    [btn2 release];
    
    //94.0, 27.0
    CGRect switchframe = CGRectMake(0, 0, self.view.bounds.size.width/3.40425, self.view.bounds.size.height/17.7777);
    self.gameCenterSwitch = [[UISwitch alloc] initWithFrame:switchframe];
    [gameCenterSwitch addTarget:self action:@selector(gameCenterSwichChanged) forControlEvents:UIControlEventValueChanged];
    
    // in case the parent view draws with a custom color or gradient, use a transparent color
    gameCenterSwitch.backgroundColor = [UIColor clearColor];
    
    [gameCenterSwitch setAccessibilityLabel:NSLocalizedString(@"GameCenterSwitch", @"")];

    if(gameCenterAvailable == NO)
        {
            gameCenterSwitch.on = NO;
            gameCenterSwitch.enabled = NO;
            gameCenterOff = YES;
        }//in case gamecenter is not available
        else
        {
        gameCenterSwitch.on = gameCenterOff? NO: YES;
        }
    
    
    self.autoReportingSwitch = [[UISwitch alloc] initWithFrame:switchframe];
    [self.autoReportingSwitch addTarget:self action:@selector(autoReportSwitchChamged) forControlEvents:UIControlEventValueChanged];
    self.autoReportingSwitch.backgroundColor = [UIColor clearColor];
    
    
     frame = CGRectMake(0, 0.0, 180, 30);
    self.reporterName = [[UITextField alloc] initWithFrame:frame];
    self.reporterName.borderStyle = UITextBorderStyleRoundedRect;
    self.reporterName.textColor = [UIColor blackColor];
    self.reporterName.font = [UIFont systemFontOfSize:15.0];
    self.reporterName.placeholder = @"Optional";
    self.reporterName.backgroundColor = [UIColor whiteColor];
    self.reporterName.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
    self.reporterName.keyboardType = UIKeyboardTypeDefault;
    self.reporterName.returnKeyType = UIReturnKeyDone;
    self.reporterName.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
    self.reporterName.delegate = self;

    if([registeredUserName length] == 0)
    {
        self.reporterName.text = @"Anonymous";
    }
    else
    {
        self.reporterName.text = registeredUserName;
    }
    
    if(autoReporting)
    {
        autoReportingSwitch.on = YES;        
    }
    else
    {
        self.reporterName.enabled = NO;
        self.reporterName.alpha = 0.5;
    }
    
}//end of setUpCfgView


- (void)viewDidUnload
{
    if(gameCfgTableView)
        [gameCfgTableView release];
    
    if(noNodesSlider)
        [noNodesSlider release];
    
    if(levelArray)
        [levelArray release]; 
    
    if(nodeNumLabel)
        [nodeNumLabel release];
    
    if(gameCenterSwitch)
        [gameCenterSwitch release];
    if(upButton)
        [upButton release];
    if(downButton)
        [downButton release];
    
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;//was 3
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
            
        case 2:
            return [self.levelArray count];
            break;
        case 4:
            return 2;
            break;
            
        default:
            return 1;
            break;
    }
}





// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *cellIdentifier = [[[NSArray alloc] initWithObjects:@"cell1",@"cell2", @"cell3",@"cell4",@"cell5", nil] autorelease];
    
    static NSString *gameCenterStr = @"Game Center";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifier objectAtIndex:indexPath.section]];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[cellIdentifier objectAtIndex:indexPath.section]]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }//end of if
	
	if(indexPath.section == 0)
	{
        //CGPoint bestPoint = CGPointMake(250, 20);
		switch(indexPath.row)
		{
			case 0:
                [cell addSubview:self.nodeNumLabel];
                [cell addSubview:noNodesSlider];
                [cell addSubview:self.upButton];
                [cell addSubview:self.downButton];
                self.upButton.center = CGPointMake(290, 20);
                noNodesSlider.center = CGPointMake(185, 20);
                self.nodeNumLabel.center = CGPointMake(50, 20);
                self.downButton.center = CGPointMake(80, 20);
                
				break;	
			default:
				//
				break;
		}//end of switch indexPath.row
        
	}//end of indexPath.row ==0;
    else if(indexPath.section == 1)
    {
        switch (indexPath.row) {
            case 0:
                if(stageNum > 0)
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                [cell addSubview:self.stageNumLabel];
                self.stageNumLabel.center = CGPointMake(160, 22);
                break;
            default:
                break;
        }
        
    }
    else if(indexPath.section == 2)
    {
        if(indexPath.row == Difficulty)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        
        cell.textLabel.text = [self.levelArray objectAtIndex:indexPath.row];
        
    }//end of indexPath.section ==1
    else if(indexPath.section == 3)
    {
        cell.textLabel.text = gameCenterStr;
        [cell addSubview:self.gameCenterSwitch];
        self.gameCenterSwitch.center = CGPointMake(240, 22);
        
    }//end of indexPath.section ==3
    else if(indexPath.section == 4)
    {
        if(indexPath.row == 0)
        {
            cell.textLabel.text = @"Auto reporting";
            [cell addSubview: self.autoReportingSwitch];
            self.autoReportingSwitch.center = CGPointMake(240, 22);
        }
        else
        {
            cell.textLabel.text = @"Name";
            [cell addSubview:self.reporterName];
            self.reporterName.center = CGPointMake(200, 22);
        }
        
    
    }

    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
  	//NSLog(@"titleForHeaderInSection method in  SettingTableViewController.m is properly called");     
    NSString *title = nil;
    switch (section) {
        case 0:
            title = NSLocalizedString(@"Number of Light Poles", @"Node Number");
            break;
        case 1:
			 title = NSLocalizedString(@"Stage", @"Stage");
        break;
            
        case 2:
             title = NSLocalizedString(@"Difficulty", @"Difficulty");
        break;
            
        case 3:
		title = NSLocalizedString(@"Game Center", @"Game Center");
        break;
            
		case 4:
		title = NSLocalizedString(@"Reporting", @"Reporting");
		break;
        
        default:
        break;
    }
    return title;
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;    
    if (indexPath.section == 1) {
        
        if(!self.stgController)
        {
            self.stgController = [[GameStageController alloc] initWithNibName:nil bundle:nil];
            self.stgController.delegate = self;
        }
        [self.navigationController pushViewController:self.stgController animated:YES];
                
    }//end of if 
    
    else if (indexPath.section == 2) {
        if(indexPath.row != Difficulty)
        {
        cell = [self.gameCfgTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:Difficulty inSection:2]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell = [self.gameCfgTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:2]];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        Difficulty = indexPath.row;
        }//end of if(indexPath.section != Difficulty)
        
    }//end of if
        
}//end of didSelect

#pragma mark GUI updates


-(void)updateStageNumLabel
{
    if(stageNum > -1)
    {   
        self.stageNumLabel.text = [NSString stringWithFormat:@"Stage No.%d", stageNum + 1];
    }
    else
    {    
        self.stageNumLabel.text = @"Random";

    }
}

-(void)updateNodesNum
{    
    NextTree_NoNodes = (int)self.noNodesSlider.value;
    self.nodeNumLabel.text = [NSString stringWithFormat:@"%d", NextTree_NoNodes];
}//end of updateNodesNum

-(void)moveNodeNum:(id)sender
{

    if(sender == self.upButton)
    {
        if(NextTree_NoNodes < MAX_Node_Num)
        NextTree_NoNodes++;        
    }
    else if(sender == self.downButton)
    {    
        if(NextTree_NoNodes> Min_Node_Num)
        NextTree_NoNodes--;
    }//end of else if
    
    self.noNodesSlider.value = NextTree_NoNodes;    
    nodeNumLabel.text = [NSString stringWithFormat:@"%d", NextTree_NoNodes];    
}//end of moveNodeNum

#pragma mark GUI update for other class

-(void)updateStageTable{
    if(self.stgController)
    {
        [self.stgController updateStageInfo_AND_ReloadTable];
    }
}//end of updateStageTable

#pragma mark GameStageControllerDelegate
-(void)stageChanged:(GameStageController*)stg
{

    [self updateStageNumLabel];
    if(stageNum> -1)
        [self randomStageEnabled:NO];
    else
        [self randomStageEnabled:YES];
    
    [self updateNodesNum_ForStage];
}

#pragma mark UIEvent

-(void)gameCenterSwichChanged{

    if(self.gameCenterSwitch.on)
    {
        gameCenterOff = NO;
    }
    else
    {
        gameCenterOff = YES;
    }
    
    [[self delegate] gameCenterConfigChanged:self];
    
}//end of gameCenterSwitchChanged


#pragma mark Utility methods
-(void)updateNodesNum_ForStage{

    if(stageNum >= 0)
    [GameConfigurationController setNodeNumForStage];
    self.noNodesSlider.value = NextTree_NoNodes;
    [self updateNodesNum];
}

-(void)autoReportSwitchChamged
{
    autoReporting = self.autoReportingSwitch.on;

    //No need to disable the text field
    /*
    if(!autoReporting)
    {
        self.reporterName.enabled = NO;
        self.reporterName.alpha = 0.5;
    }
    else
    {
        self.reporterName.enabled = YES;
        self.reporterName.alpha = 1.0;
    }
    */

}

+(void)setNodeNumForStage
{
    /********************
     Stage 1 cosists of 3 nodes. 
     Stage 2 to 5 consists 5 nodes.
     Stage 6 to 20 consists 10 nodes.
     Stage 21 to 50 consists 15 nodes.
     Stage 51 to 100 consists 20 nodes. 
     ********************/
    if(stageNum == 0)
    {
        NextTree_NoNodes = 3;
    }
    else if(stageNum > 0 && stageNum < 5)
    {
        NextTree_NoNodes = 5;
    }
    else if( stageNum < 10)
    {
        NextTree_NoNodes = stageNum + 1;
    }
    else if(stageNum < 20)
    {
        NextTree_NoNodes = 10;
    }
    else if (stageNum < 50)
    {
        NextTree_NoNodes = 15;
    }
    else if(stageNum < 85)
    {
        NextTree_NoNodes = 20;
    }
    else if(stageNum < 95)
    {
        NextTree_NoNodes = 25;
    }
    else if(stageNum < 100)
    {
        NextTree_NoNodes = 30;
    }    
}

-(void)randomStageEnabled:(BOOL)tag
{
    self.downButton.enabled = tag;
    self.upButton.enabled = tag;
    self.noNodesSlider.enabled = tag;
    if(!tag)
    {
    [self updateNodesNum];
    }
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
    registeredUserName = [textField text];
    [registeredUserName retain];
    
    NSLog(@"registered username is %@", registeredUserName);
    self.gameCfgTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 90.0)];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:4];
    [self.gameCfgTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
	return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	self.gameCfgTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 300.0)];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:4];
    [self.gameCfgTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];    
    return YES;
}

@end
