//
//  GameStageController.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 8/19/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameStageCell.h"
#import "SQLManager.h"

@protocol GameStageControllerDelegate;

enum MedalIndex{
    BronzMedal =0,
    SilberMedal,
    GoldenMedal,
    EmeraldBlueMedal
};

@interface GameStageController : UIViewController
<UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate > 
{
    UITableView *gameStgTableView;  
    int currentStageNum;
    id <GameStageControllerDelegate> delegate;
    UIImage /**checkMark,*/ *question, *currentStage;
    UIImage *medalArray[4];
    BOOL clearedStage[100];
    double clearedScore[100];
    SQLManager *mgr;
}
@property(retain, nonatomic)    SQLManager *mgr;
@property(retain,nonatomic)UITableView *gameStgTableView;  
@property (nonatomic, assign) id <GameStageControllerDelegate> delegate;
@property(retain, nonatomic)    UIImage /* *checkMark,*/ *question, *currentStage;
-(void)initStages;
-(void)releaseObjects;
//-(void)initRandomSeed:(int)seed;
-(void)copyStageInfo;
-(void)updateStageInfo_AND_ReloadTable;

@end

@protocol GameStageControllerDelegate <NSObject>
@optional
-(void)stageChanged:(GameStageController*)stg;
@end
