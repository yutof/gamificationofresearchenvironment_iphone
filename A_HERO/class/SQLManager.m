//
//  SQLManager.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/18/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "SQLManager.h"


@implementation SQLManager
@synthesize databaseName, databasePath;

-(id)init
{

	self.databaseName = @"DCT.sql";
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    self.databasePath = [documentsDir stringByAppendingPathComponent:self.databaseName];
    [self checkAndCreateDatabase];
                
    [self tableUpdate_v102];
    [self tableUpdate_v110];
    [self tableUpdate_v120];
	[self tableUpdate_v121];
    return self;
}

#pragma mark Table updates
-(void)tableUpdate_v102
{
    if(sqlVersionTag < 1)
    {
    NSString* statment = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS StepwiseLog(treeID INTEGER, stepNum INTEGER, comment TEXT);Create Table IF NOT EXISTS ReportList(id INTEGER, stageNum INTEGER, reportType INTEGER, reportedDate TEXT, name TEXT, treeID INTEGER, strategy TEXT);"];

    [self query:statment];
    sqlVersionTag = 1;
        
    }
}

-(void)tableUpdate_v110
{
    if(sqlVersionTag == 1)
    {
        
    NSString* statment = [NSString stringWithFormat:@"ALTER Table PendingReport ADD PR_StepNum Integer;ALTER Table PendingReport ADD PR_StageNum Integer;ALTER Table PendingReport ADD reportType Integer;"];
        
        [self query:statment];
        sqlVersionTag = 2;        
    }
}

-(void)tableUpdate_v120
{
    if(sqlVersionTag == 2)
    {
        
        NSString* statment = [NSString stringWithFormat:@"ALTER Table Tree ADD NodeNum INTEGER;Alter Table Tree ADD LoseCounter INTEGER;"];
        [self query:statment];
        sqlVersionTag = 3;
        
    }
//NSLog(@"version num %d", sqlVersionTag);
    
}
-(void)tableUpdate_v121
{

    if(sqlVersionTag == 3)
    {
        
        NSString* statment = @"CREATE TABLE IF NOT EXISTS PendingAnonymousReport(id INTEGER PRIMARY KEY, stageNum INTEGER, stepNum INTEGER, indicatorFlag INTEGER);";
        [self query:statment];
        sqlVersionTag = 4;
        
    }

}

#pragma mark Query methods

-(void) checkAndCreateDatabase{
    
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
        
    // Check if the database has already been created in the users filesystem
    success = [fileManager fileExistsAtPath:self.databasePath];
        
    // If the database already exists then return without doing anything
    if(success) return;
        
    // If not then proceed to copy the database from the application to the users filesystem
    // Get the path to the database in the application package
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseName];
        
    // Copy the database from the package to the users filesystem
    [fileManager copyItemAtPath:databasePathFromApp toPath:self.databasePath error:nil];
            
    NSString *initDB = @"CREATE TABLE Tree ( Tree_ID INTEGER PRIMARY KEY AUTOINCREMENT, Tree_StageNum INTEGER, Tree_ClearedScore DOUBLE); CREATE TABLE NodeList (NL_NodeID INTEGER, NL_X INTEGER, NL_Y INTEGER, NL_Tree_ID INTEGER, FOREIGN KEY (NL_Tree_ID) REFERENCES Tree(Tree_ID) ); CREATE TABLE UserNodeList (UNL_NodeID INTEGER, UNL_ParentID INTEGER, UNL_Power DOUBLE, UNL_Tree_ID INTEGER, FOREIGN KEY (UNL_Tree_ID) REFERENCES Tree(Tree_ID)); CREATE TABLE DCTNodeList (DCTNL_NodeID INTEGER, DCTNL_ParentID INTEGER, DCTNL_Power DOUBLE, DCTNL_Tree_ID INTEGER, FOREIGN KEY (DCTNL_Tree_ID) REFERENCES Tree(Tree_ID) ); CREATE TABLE Action (Action_ID INTEGER PRIMARY KEY AUTOINCREMENT,Action_Description VARCHAR(255));CREATE TABLE UserLog (UL_Step INTEGER, UL_ActualStep INTEGER, UL_RelyingNodeID INTEGER, UL_DestinationNodeID INTEGER, UL_Power DOUBLE, UL_Action_ID INTEGER, UL_Tree_ID INTEGER, FOREIGN KEY (UL_Action_ID) REFERENCES Action(Action_ID),  FOREIGN KEY (UL_Tree_ID) REFERENCES Tree(Tree_ID) );Insert INTO Action (Action_Description) Values ('Cover'); Insert INTO Action (Action_Description) Values ('Cancel');Insert INTO Action (Action_Description) Values ('Restore');CREATE TABLE Report(Report_ID INTEGER, Report_Strategy TEXT, Report_Username TEXT, Report_Filepath TEXT); CREATE TABLE PendingReport(PR_Tree_ID INTEGER, PR_Strategy TEXT, PR_Name TEXT, PR_Filepath TEXT, PR_Score DOUBLE);CREATE TABLE Stage(Stage_ID INTEGER, Stage_TreeID INTEGER, Stage_Cleared INTEGER, Stage_ClearedScore DOUBLE, secretHash TEXT);";
    NSString *stages =@"";
    for(int i =0; i< HighestStageNumber; i++)
    {
    stages=[NSString stringWithFormat:@"%@ INSERT INTO Stage (Stage_ID, Stage_Cleared, Stage_ClearedScore) VALUES(%d, 0, -1000);",stages,i];
    }
    
    
    NSString *initDB_state = [NSString stringWithFormat:@"BEGIN TRANSACTION; %@ %@ COMMIT;", initDB, stages];
    [self query:initDB_state];
    //NSLog(@"InitDB is done");
    
    
}

-(void)query:( NSString*)statement
{
        sqlite3 *database=nil;
        if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
            
            sqlite3_stmt *compiledStatement=nil;
            //NSLog(@"query statement : %@",statement);
            if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
                sqlite3_exec(database, [statement UTF8String], NULL, NULL, NULL);
            }
            else
                NSLog(@"\n\nERROR in QUERY %s ?",sqlite3_errmsg(database));
            
            sqlite3_finalize(compiledStatement);		
        }//end of if
        sqlite3_close(database);

}//end of query


-(BOOL)queryWithErrorcheck:(NSString*)statement
{
    BOOL succeeded = YES;
    sqlite3 *database=nil;
    if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
        sqlite3_stmt *compiledStatement=nil;
        //NSLog(@"query statement : %@",statement);
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
            sqlite3_exec(database, [statement UTF8String], NULL, NULL, NULL);
        }
        else
        {
            NSLog(@"\n\nERROR in QUERY %s ?",sqlite3_errmsg(database));
            succeeded = NO;
        }        
        sqlite3_finalize(compiledStatement);		
    }//end of if
    sqlite3_close(database);
    
    return succeeded;
    
}//end of query



-(int)createTree:(int)tree_stageNum
{
    
    int localtreeID =0;
        sqlite3 *database;
        if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
            NSString *statement = [NSString stringWithFormat:@"INSERT INTO Tree (Tree_StageNum, NodeNum, LoseCounter) VALUES (%d, %d, 0);", tree_stageNum, NoNodes];
            const char *sqlStatement = [statement UTF8String];
            sqlite3_stmt *compiledStatement;
            //NSLog(@"Create Tree %s statement?", [statement UTF8String]);
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                sqlite3_exec(database, sqlStatement, NULL, NULL, NULL);
                localtreeID = sqlite3_last_insert_rowid(database);
            }
            else
                NSLog(@"%s error?",sqlite3_errmsg(database));
            
            sqlite3_finalize(compiledStatement);		
        }//end of if
        
        
        sqlite3_close(database);
        
    return localtreeID;

}//end of createTree


-(void)copyResult:(struct nodeType*)nodeArray Step:(int)step Previous:(int)prev TreeID:(int)treeID RelyingNode:(int *)rel totalPower:(double *)totalPower
{
    double prevPower;
    int relyingNode=-1, destinationNode=-1, originalRelyingNode=-1, originalDestinationNode=-1;
    sqlite3 *database;
    
    
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) 
    {
        NSString *statement = [NSString stringWithFormat:@"SELECT UL_RelyingNodeID, UL_DestinationNodeID ,UL_Power FROM UserLog WHERE UL_Step='%d' AND UL_Tree_ID ='%d' ORDER BY UL_ActualStep DESC;", step, treeID];
        const char *sqlStatement = [statement UTF8String];//UL_Step DESC, 
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                relyingNode = sqlite3_column_int(compiledStatement, 0);
                destinationNode = sqlite3_column_int(compiledStatement, 1);
                prevPower = sqlite3_column_int(compiledStatement, 2);    
                
                originalRelyingNode = relyingNode;
                originalDestinationNode= destinationNode;
                for(int i=0; i< NoNodes; i++)
                {
                        if(nodeArray[i].parent_id == relyingNode)
                        {
                            nodeArray[i].parent_id = -2;
                        }//end of if
                    }//end of for
                    nodeArray[relyingNode].power = 0;
                
                break;
                
            }//end of while
            
            *rel = originalRelyingNode;
            if( step > 0)
                *totalPower = prevPower;
            else
                *totalPower = 0;

            }
        else
            NSLog(@"%s error?",sqlite3_errmsg(database));
        
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
    
    
	sqlite3_close(database);
    
    
}//end of copyResult





-(void)restoreStep:(int)step TreeID:(int)treeID relyingNode:(int *)rel destinationNode:(int*)dest totalPower:(double *)totalPower
{
    
    double prevPower, originalPower=0;
    int relyingNode=-1, destinationNode=-1, originalRelyingNode=-1, originalDest =-1;
    sqlite3 *database;
    
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) 
    {
        NSString *statement = [NSString stringWithFormat:@"SELECT UL_RelyingNodeID, UL_DestinationNodeID ,UL_Power, UL_ActualStep FROM UserLog WHERE UL_Step='%d' AND UL_Tree_ID ='%d' ORDER BY UL_ActualStep DESC;", step, treeID];
        const char *sqlStatement = [statement UTF8String];//UL_Step DESC, 
		sqlite3_stmt *compiledStatement;
        //NSLog(@"RESTORESTEP %s ", [statement UTF8String]);
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
           // NSLog(@"")
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                relyingNode = sqlite3_column_int(compiledStatement, 0);
                destinationNode = sqlite3_column_int(compiledStatement, 1);
                prevPower = sqlite3_column_int(compiledStatement, 2);    
                    originalRelyingNode = relyingNode;
                    originalPower = prevPower;
                    originalDest = destinationNode;
                    //firstRow = NO;
                break;
                
            }//end of while
            
            *rel = originalRelyingNode;
            
                *totalPower = originalPower;
        *dest = originalDest;
		}
        else
            NSLog(@"%s error?",sqlite3_errmsg(database));
        
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
    
    
	sqlite3_close(database);

}//end of restoreStep


-(void)copyTree:(int*)xArray y:(int*)yArray tree:(int)treeIDNum
{

    sqlite3 *database;
    int counter =0;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) 
    {
        NSString *statement = [NSString stringWithFormat:@"SELECT NL_X, NL_Y FROM NodeList WHERE NL_Tree_ID=%d", treeIDNum];
        const char *sqlStatement = [statement UTF8String];//UL_Step DESC, 
		sqlite3_stmt *compiledStatement;
        //NSLog(@"copyTree %s", [statement UTF8String]);
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                xArray[counter] = sqlite3_column_int(compiledStatement, 0);
                yArray[counter] = sqlite3_column_int(compiledStatement, 1);
                //NSLog(@"x:%d and y:%d", xArray[counter], yArray[counter]);
                counter++;
            }//end of while
		}
        else
            NSLog(@"%s error?",sqlite3_errmsg(database));
        
        //*relyingNode;
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
    
    
	sqlite3_close(database);

    
}//end of copyTree


-(void)copyStageResult:(BOOL*)cleared score:(double*)scr
{
    sqlite3 *database;
    int counter =0;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) 
    {
        NSString *statement = @"SELECT Stage_Cleared, Stage_ClearedScore FROM Stage WHERE STAGE_ID < 100 ORDER BY STAGE_ID ASC";
        //[NSString stringWithFormat:@"SELECT Stage_ID, Stage_TreeID, Stage_Cleared, Stage_ClearedScore, Stage_ClearedStep FROM NodeList WHERE NL_Tree_ID=%d", treeIDNum];
        const char *sqlStatement = [statement UTF8String];//UL_Step DESC, 
		sqlite3_stmt *compiledStatement;
        //NSLog(@"copyTree %s", [statement UTF8String]);
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                cleared[counter] = sqlite3_column_int(compiledStatement, 0);
                scr[counter] = sqlite3_column_double(compiledStatement, 1);
                
                //NSLog(@"x:%d and y:%d", xArray[counter], yArray[counter]);
                counter++;
            }//end of while
		}
        else
            NSLog(@"%s error?",sqlite3_errmsg(database));
       
        
        
        //*relyingNode;
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);
}

-(int)getInteger:(NSString*)statement
{
    int returnNum =-1;
    sqlite3 *database;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) 
    {
        const char *sqlStatement = [statement UTF8String];//UL_Step DESC, 
		sqlite3_stmt *compiledStatement;
        //NSLog(@"getText %s", [query UTF8String]);
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                returnNum = sqlite3_column_int(compiledStatement, 0);
                //returnStr = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement, 0)];                
                break;
            }//end of while
		}
        else
            NSLog(@"%s error?",sqlite3_errmsg(database));
        
        //*relyingNode;
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);

    return returnNum;
}



-(NSString*)getText:(NSString*)query
{
    sqlite3 *database;
    NSString* returnStr = @"";
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) 
    {
        const char *sqlStatement = [query UTF8String];//UL_Step DESC, 
		sqlite3_stmt *compiledStatement;
        //NSLog(@"getText %s", [query UTF8String]);
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                returnStr = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement, 0)];
             
                break;
                //NEVER DO THIS !!! CAUSE DEADLOCK
                //return returnStr;
            }//end of while
		}
        else
            NSLog(@"%s error?",sqlite3_errmsg(database));
        
        //*relyingNode;
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);

    return returnStr;
    
}


-(int)getRowCount:(NSString*)tableName
{
    int recordCount = -1;
    NSString *statement = [NSString stringWithFormat:@"SELECT Count(*) FROM %@", tableName];
    sqlite3 *database=nil;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
		sqlite3_stmt *compiledStatement=nil;
        //NSLog(@"getRowCount : %@",statement);
		if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                recordCount = sqlite3_column_int(compiledStatement, 0);                
            }//end of while            
            
		}//end of if
        else
            NSLog(@"ERROR: %s",sqlite3_errmsg(database));
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);
    
    return recordCount;
    
    
    
}


-(NSString*)convertDateFormat:(NSString*)datetime
{
    NSArray *dateTimeArr= [datetime componentsSeparatedByString:@" "];
    NSString *dateStr =[dateTimeArr objectAtIndex:0];
    NSArray *dateArr = [dateStr componentsSeparatedByString:@"-"];
    NSString *returnStr = [NSString stringWithFormat:@"%@/%@/%@", [dateArr objectAtIndex:0],[dateArr objectAtIndex:1],[dateArr objectAtIndex:2]];
    return returnStr;
}
-(NSMutableArray*)getReportList:(int)type
{
    NSMutableArray* reportListArray = [[NSMutableArray alloc] init];
     /*
     @property (retain, nonatomic)NSString* name;
     @property NSInteger reportID;
     @property NSInteger stageNum;
     @property (retain, nonatomic)NSString * reportDate;
     */
    
    //ReportList(id INTEGER, stageNum INTEGER, reportType INTEGER, reportedDate TEXT, name TEXT, treeID INTEGER);
    NSString *statement = [NSString stringWithFormat:@"SELECT id, stageNum, reportedDate, name, treeID, strategy FROM ReportList WHERE reportType=%d ORDER BY id DESC", type];
    sqlite3 *database=nil;

	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
		sqlite3_stmt *compiledStatement=nil;
        //NSLog(@"getRowCount : %@",statement);
		if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                ReportItem *report = [[ReportItem alloc] init];
                report.reportID = sqlite3_column_int(compiledStatement, 0);
                report.stageNum = sqlite3_column_int(compiledStatement, 1);
                report.reportDate = [self convertDateFormat:[NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement, 2)]];
                report.name = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement, 3)];                
                report.treeID = sqlite3_column_int(compiledStatement, 4);
                report.strategy = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement, 5)];
                //recordCount = sqlite3_column_int(compiledStatement, 0);
                NSLog(@"treeid %d", report.treeID);
                [reportListArray addObject:report];
            }//end of while
            
		}//end of if
        else
            NSLog(@"ERROR: %s",sqlite3_errmsg(database));
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);
    
    return reportListArray;
}


-(NSMutableArray*)getPendingReportList
{
    NSMutableArray *pendingList = [[NSMutableArray alloc] init];

    NSString *statement = @"SELECT PR_Tree_ID, PR_Strategy, PR_Name,PR_Score, PR_StepNum, PR_StageNum, reportType FROM PendingReport;";
    sqlite3 *database=nil;
    
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
		sqlite3_stmt *compiledStatement=nil;
        //NSLog(@"getRowCount : %@",statement);
		if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                //ReportItem *report = [[ReportItem alloc] init];
                PendingReportItem *report = [[PendingReportItem alloc] init];
                report.treeID = sqlite3_column_int(compiledStatement, 0);
                report.strategy = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement, 1)];
                report.name = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement, 2)];
                report.score = sqlite3_column_double(compiledStatement, 3);
                report.stepNum = sqlite3_column_int(compiledStatement, 4);
//                NSLog(@"step num? %d", report.stepNum);
                report.stageNum = sqlite3_column_int(compiledStatement, 5);
                report.reportType = sqlite3_column_int(compiledStatement, 6);
//                NSLog(@"treeid %d", report.treeID);
                [pendingList addObject:report];
            }//end of while
            
		}//end of if
        else
            NSLog(@"ERROR: %s",sqlite3_errmsg(database));
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);
    
    return pendingList;
}


-(NSMutableArray*)getPendingAnonymousReportList
{
    NSMutableArray * anonymousReports=[[NSMutableArray alloc] init] ;
    
    NSString *statement = @"SELECT StepNum, StageNum, indicatorFlag FROM PendingAnonymousreport;";
    sqlite3 *database=nil;
    
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
		sqlite3_stmt *compiledStatement=nil;
		if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {

                AnonymousReportItem *report = [[AnonymousReportItem alloc] init];
                report.stepNum = sqlite3_column_int(compiledStatement, 0);
                report.stageNum =sqlite3_column_int(compiledStatement, 1);
                report.indicatorFlag = sqlite3_column_int(compiledStatement, 2);
                [anonymousReports addObject:report];
            }//end of while
            
		}//end of if
        else
            NSLog(@"ERROR: %s",sqlite3_errmsg(database));
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);
    
    return anonymousReports;
}

-(void)returnPendingReport:(int*)pr_treeID type:(int*)rptTypeTag  stp:(int*)stepNumber stg:(int*)stageNumber  nm:(NSString**)pr_name strtgy:(NSString**)pr_strategy scr:(double*)pr_score file:(NSString**)filePath
{
    *pr_treeID = -10;
    NSString *statement = [NSString stringWithFormat:@"SELECT PR_Tree_ID, PR_Name, PR_Strategy, PR_score, PR_Filepath, ReportType, PR_StepNum, PR_StageNum FROM PendingReport"];
    sqlite3 *database=nil;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
		sqlite3_stmt *compiledStatement=nil;
        //NSLog(@"getRowCount : %@",statement);
		if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                *pr_treeID = sqlite3_column_int(compiledStatement, 0); 
                *pr_name = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement,1)];
                //*pr_name = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement,1)];
                *pr_strategy = [NSString stringWithFormat:@"%s", sqlite3_column_text(compiledStatement,2)];
                *pr_score = sqlite3_column_double(compiledStatement, 3);
                *filePath = [NSString stringWithFormat:@"%s",sqlite3_column_text(compiledStatement,4)];
                *rptTypeTag = sqlite3_column_int(compiledStatement, 5);
                *stepNumber = sqlite3_column_int(compiledStatement, 6);
                *stageNumber = sqlite3_column_int(compiledStatement, 7);
                break;
            }//end of while            
            
		}//end of if
        else
            NSLog(@"ERROR: %s",sqlite3_errmsg(database));
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);

    
}

-(BOOL)isPending:(int)del_tree_id
{
    int recordCount = -1;
    NSString *statement = 
        [NSString stringWithFormat:@"SELECT Count(*) FROM PendingReport WHERE PR_Tree_ID=%d",del_tree_id ];
    
    sqlite3 *database=nil;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
		sqlite3_stmt *compiledStatement=nil;
        //NSLog(@"getRowCount : %@",statement);
		if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) 
            {
                recordCount = sqlite3_column_int(compiledStatement, 0);                
            }//end of while            
            
		}//end of if
        else
            NSLog(@"ERROR: %s",sqlite3_errmsg(database));
        
		sqlite3_finalize(compiledStatement);		
	}//end of if
	sqlite3_close(database);
    
    //return recordCount;
    if(recordCount > 0)
        return YES;
    else
        return NO;
    
}



-(void)dealloc
{
    [databaseName release];
    [databasePath release];
    [super dealloc];
}

#pragma mark Report Detail 
-(id)initWithReportID:(int)reportID
{
    self.databaseName = [NSString stringWithFormat:@"/db/dct_%d.sql", reportID];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    self.databasePath = [documentsDir stringByAppendingPathComponent:self.databaseName];

    [self checkAndDownloadDatabase:reportID];

    return self;
}


-(void) checkAndDownloadDatabase:(int)reportID{
    
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Check if the database has already been created in the users filesystem
    success = [fileManager fileExistsAtPath:self.databasePath];
    
    // If the database already exists then return without doing anything
    if(success) return;
    else
        NSLog(@"failed??");
    
    [self downloadTheSQLFile:reportID];
        
}


-(void)downloadTheSQLFile:(int)reportIDNum{

    
    NSString *unexpandedPath = [NSHomeDirectory() stringByAppendingString:@"/Documents/db/"];
    NSString *folderPath = [NSString pathWithComponents:[NSArray arrayWithObjects:[NSString stringWithString:[unexpandedPath stringByExpandingTildeInPath]], nil]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath isDirectory:NULL]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/db_Location_Finder/veryStrong_security/hiddenFolder"];
	ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
	[request setPostValue:[NSNumber numberWithInt:reportIDNum] forKey:@"reportID"];
    request.tag = DBPathFinder;    
    request.delegate = self;
    
	[request startSynchronous];
    

    NSString *response =  [request responseString];
    
    NSLog(@"response is %@", response);
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL uploadFailed = ([response isEqualToString:[NSString stringWithFormat:@"not found"]]);
    

    if(!uploadFailed)
    {    
    NSString *srcURL = [request responseString];
    url = [NSURL URLWithString:srcURL];
    
    request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];    
    unexpandedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/db/dct_%d.sql",reportIDNum]];
    folderPath = [NSString pathWithComponents:[NSArray arrayWithObjects:[NSString stringWithString:[unexpandedPath stringByExpandingTildeInPath]], nil]];
    [request setDownloadDestinationPath:folderPath];
	[request startSynchronous];
        
	NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:folderPath];
    
    if(success)
        NSLog(@"worked!!!!");
    else
        NSLog(@"nooo!!");
    }
    else
    {
    
        NSString *srcURL = [NSString stringWithFormat:@"http://yourdomain.com/setsuden/sql_uploads/DCT_%d.sql", reportIDNum];
        url = [NSURL URLWithString:srcURL];
        
        request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];    
        unexpandedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/db/dct_%d.sql",reportIDNum]];
        folderPath = [NSString pathWithComponents:[NSArray arrayWithObjects:[NSString stringWithString:[unexpandedPath stringByExpandingTildeInPath]], nil]];
        [request setUsername:server_username];
        [request setPassword:server_password];
        
        [request setDownloadDestinationPath:folderPath];
        [request startSynchronous];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL success = [fileManager fileExistsAtPath:folderPath];
        
        if(success)
            NSLog(@"worked!!!!");
        else
            NSLog(@"nooo!!");
        
    }

}

@end
