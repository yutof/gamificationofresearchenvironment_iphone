//
//  PendingReportItem.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 1/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PendingReportItem.h"

@implementation PendingReportItem
@synthesize name, strategy;
@synthesize reportType, score, stepNum;
@synthesize stageNum;
@synthesize treeID;
@end
