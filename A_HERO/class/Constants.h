//
//  Constants.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/24/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
struct nodeType{
    NSInteger id_number,parent_id, cn_id;
    NSInteger x, y;
    double power;  
    BOOL inTree;
}nd;

int NoNodes/*Represent the number of nodes*/;
int NextTree_NoNodes;
int step/*Keeps track of the step the user can see.*/
    ,actualStep/*Keeps track of all steps*/
    ,treeID/*Tree's unique id stored in DB*/;

//int ghostID;
int starNum, dctBeatedNum;

enum ConfirmationChoice{
    CFM_EveryTime=0,
    CFM_OnlyForFinalTree,
    CFM_None
};

enum StageType{
    StageType_Regular=0,
    StageType_Random
};


enum{
    VeryHard =0,
//    Hard,
//    FairlyHard,
//    FairlyEasy,
//    Easy,
    Medium,
    VeryEasy
};

enum ViewControllerTags{
    Tag_MenuView=0
    ,Tag_GamView
    ,Tag_GameConfigurationView
    ,Tag_StrategyReportView
};

enum SubmissionMessage{
    Submission_Completed=0,
    Submission_Form_Completed,
    Submission_Form_Incomplete,
    Submission_Failed,
    Submission_Confirmed
};

enum ReportType{
    RegularSubmission=0,
    PendingSubmission,
    RegularStepSubmission,
    PendingStepSubmission,
    DBPathFinder,
    ReportListing
};

enum StrReportType{
    GeneralStrategy=0,
    StepwiseStrategy, 
    LogOnly
};

int Difficulty;
NSString* playerID;
BOOL serverAvailable;
BOOL pending;
BOOL sendingReport;


double winningPercentage;
BOOL showReport;
BOOL retrying;
BOOL gameCenterAvailable, gameCenterOff, askStepConfirmation;
BOOL firstTime;
BOOL reorganizedForReport;
int  actualTreeID;
int actualNoNodes;
BOOL showingReportDetail;

int viewTagNum;

//int confirmationSetting;

int stageNum, lastClearedStage, curStageNum;
int characterLevel, consecutiveWinCounter;
int consecutiveLoseCounter;

static NSString *server_username = @"secretName123";
static NSString *server_password = @"superStrongPassword";
static NSString *commonkey = @"commonKey";
NSString* registeredUserName, *registeredUserID;
int sqlVersionTag;
BOOL autoReporting;

#define MAX_Node_Num 30
#define Min_Node_Num 3
#define DefaultNodeNum 5
#define HighestStageNumber 100

//CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;}
//CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};
