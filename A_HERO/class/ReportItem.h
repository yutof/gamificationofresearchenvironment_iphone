//
//  ReportItem.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportItem : NSObject


@property NSInteger reportID;
@property NSInteger stageNum;
@property NSInteger treeID;
@property (retain, nonatomic)NSString *name;
@property (retain, nonatomic)NSString *reportDate;
@property (retain, nonatomic)NSString *strategy;
//Create Table IF NOT EXISTS ReportList(id INTEGER, stageNum INTEGER, reportType INTEGER, reportedDate TEXT, name TEXT, treeID INTEGER);
@end
