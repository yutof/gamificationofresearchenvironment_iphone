//
//  ReachabilityUpdator.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/20/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReachabilityUpdator class. 
 * Verify reachability of a particular server
 * with error handling so that we can detect
 * an error earlier if there's any before 
 * sending data to the server
 *******************************************/

#import "ReachabilityUpdator.h"
#import "ASIFormDataRequest.h"

@implementation ReachabilityUpdator

+(void)updateServerAvailability{
    
    Reachability* reachability = [Reachability reachabilityWithHostName:@"www.yourdomain.com"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    serverAvailable = NO;
    
    if(remoteHostStatus == NotReachable) { 
        //NSLog(@"not reachable");
        serverAvailable = NO;
    }
    else if (remoteHostStatus == ReachableViaWWAN) { 
        //NSLog(@"reachable via wan");
        serverAvailable = YES;
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        //NSLog(@"reachable via wifi");
        serverAvailable = YES;
    }    
    
    if(serverAvailable)
    {
        NSURL *responder = [NSURL URLWithString:@"http://yourdomain.com/setsuden/serverAvilablityCheck/veryStrong_security/hiddenFolder"];
        ASIHTTPRequest *request  = [ASIHTTPRequest requestWithURL:responder];
        [request startSynchronous];
        NSString *response =  [request responseString];
        response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        serverAvailable = ([response isEqualToString:[NSString stringWithFormat:@"Available"]]);
        
    }
}

@end
