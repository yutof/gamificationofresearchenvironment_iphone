//
//  SubmissionRequestor.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * SubmissionRequestor class. 
 * Takes care of almost all web requests using
 * ASIFormDataRequest class. 
 * This class also consists a procedure that 
 * stores necessary data into local database
 * so that the application will not lose the
 * data that was provided by the user
 *******************************************/



#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "Constants.h"
#import "EncryptorClass.h"
#import "SQLManager.h"
#import "ReachabilityUpdator.h"
#import "PendingReportItem.h"
#import "AnonymousReportItem.h"

@protocol SubmissionRequestorDelegate;
@interface SubmissionRequestor : NSObject
{
    int GV_reportedTreeID;
    NSString* GV_name, *GV_strtgy_txt;
    double GV_winningPercentage;
    int GV_stepNum ,GV_type, GV_stageNum;
    id<SubmissionRequestorDelegate>delegate;
    BOOL error_occured;
    enum StrReportType pendingTag;
    
}
@property int GV_reportedTreeID, GV_stepNum ,GV_type, GV_stageNum;
@property(retain, nonatomic)NSString* GV_name, *GV_strtgy_txt;
@property double GV_winningPercentage;
@property (nonatomic, assign) id<SubmissionRequestorDelegate> delegate;
@property BOOL error_occured;
@property enum StrReportType pendingTag;
-(BOOL)latestSubmitPendingRecord:(PendingReportItem*)pendingItem;
-(void)submitStrategy:(int)reportedTreeID strategy:(NSString*)strtgy_txt 
                score:(double)winningPercentage stepNumber:(int)stepNum stg:(int)stageNumber rptType:(enum ReportType)type;
-(void)storePendingRest:(int)pr_treeID stp:(int)stepNumber strtgy:(NSString*)pr_strategy scr:(double)pr_score file:(NSString*)filePath stage:(int)stageNumber;
-(void)submitPendingRecord;
-(void)submitRecord_Request:(int)rpt_treeID stp:(int)stepNumber stg:(int)stageNumber strtgy:(NSString*)rpt_strategy scr:(double)rpt_score file:(NSString*)filePath type:(enum ReportType)tag;
-(void)regularSubmissionSucceeded;
-(void)regularSubmissionFailed;
-(void)askConfirmation;
-(void)getReportList;
-(void)reuploadFile:(int)treeIDNumber;
-(void)updateNameAndStrategy:(NSString*)str;
-(void)updatePendingRequest:(NSString*)Name str:(NSString*)strategy rptType:(enum StrReportType)type;;
-(void)getRegisteredID;
-(void)submitAnonymousReport:(AnonymousReportItem*)item;
-(void)storeAnonymousReport:(AnonymousReportItem*)item;
@end


@protocol SubmissionRequestorDelegate <NSObject>
@optional
-(void)requestCompleted:(SubmissionRequestor*)req;
@end
