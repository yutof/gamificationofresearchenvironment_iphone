//
//  StepStrategyReport.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * StepStrategyReport class. 
 * Provide GUI for stepwise strategy report
 *******************************************/

#import "StepStrategyReport.h"

@implementation StepStrategyReport
@synthesize stepMemo, bottomBar, addANoteButton, submitButton, memoDoneButton;
@synthesize textShown;
@synthesize stepLabel;
@synthesize requestorObj;
@synthesize delegate;



-(void)dealloc{
    
    if(stepMemo)
        [stepMemo release];
    
    if(bottomBar)
        [bottomBar release];
    
    if(addANoteButton)
        [addANoteButton release];
    
    if(submitButton)
        [submitButton release];

    if(memoDoneButton)
        [memoDoneButton release];
    
    if(requestorObj)
        [requestorObj release];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setUpStepStrategyReportView];
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setUpStepStrategyReportView{


    self.view.frame = CGRectMake(0, 00, 480.0, 480.0);
    self.view.backgroundColor = [UIColor clearColor];
    self.bottomBar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 396, 320.0, 40.0)];
    [self.view addSubview:self.bottomBar];
    
    
    self.addANoteButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
    self.addANoteButton.frame = CGRectMake(220, 5.0, 86, 26);
    //    self.bottomBarButton = [[UIButton alloc] initWithFrame:CGRectMake(300.0, 0.0, 26.0, 26.0)];
    self.addANoteButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.addANoteButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.addANoteButton.backgroundColor = [UIColor clearColor];
    [self.addANoteButton setTitle:@"Add a Note" forState:UIControlStateNormal];
    //	[self.bottomBarButton setBackgroundImage:checked forState:UIControlStateNormal];
    //    self.bottomBarButton.tag = 1;
    [self.addANoteButton addTarget:self action:@selector(showText) forControlEvents:UIControlEventTouchDown];
    
    [self.bottomBar addSubview:self.addANoteButton];
    [self.addANoteButton release];

    
    
    self.submitButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
    self.submitButton.frame = CGRectMake(120, 5.0, 56, 26);
    //    self.bottomBarButton = [[UIButton alloc] initWithFrame:CGRectMake(300.0, 0.0, 26.0, 26.0)];
    self.submitButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.submitButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.submitButton.backgroundColor = [UIColor clearColor];
    [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    //	[self.bottomBarButton setBackgroundImage:checked forState:UIControlStateNormal];
    //    self.bottomBarButton.tag = 1;
    [self.submitButton addTarget:self action:@selector(askConfirmation) forControlEvents:UIControlEventTouchDown];
    
    [self.bottomBar addSubview:self.submitButton];
    [self.submitButton release];
    
    
    
    //UILabel *logLabel;
    self.stepLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100.0, 40.0)] autorelease];
    //self.cordinateLabel.font = [UIFont systemFontOfSize:12.0];
	self.stepLabel.textAlignment = UITextAlignmentLeft;
	self.stepLabel.textColor = [UIColor blackColor];
	self.stepLabel.backgroundColor = [UIColor clearColor];
	self.stepLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    self.stepLabel.text = [NSString stringWithFormat:@"Step 0/100"];    
    self.stepLabel.numberOfLines =0;
    [self.bottomBar addSubview:self.stepLabel];
    

    
    CGRect textframe = CGRectMake(0, 0.0, 320, 100);
    self.stepMemo = [[UITextView alloc] initWithFrame:textframe];
    self.stepMemo.scrollEnabled = YES;
    self.stepMemo.editable = YES;
    self.stepMemo.textColor = [UIColor blackColor];
    self.stepMemo.font = [UIFont systemFontOfSize:17.0];
    self.stepMemo.backgroundColor = [UIColor whiteColor];
    self.stepMemo.text =@"Why did you choose this route?";
    self.stepMemo.autocorrectionType = UITextAutocorrectionTypeNo;
    self.stepMemo.keyboardType = UIKeyboardTypeDefault;
    self.stepMemo.returnKeyType = UIReturnKeyDone;
    self.stepMemo.delegate = self;
    [self.stepMemo setAccessibilityLabel:NSLocalizedString(@"stepstrategy", @"")];
    [self.view addSubview:self.stepMemo];
    self.stepMemo.hidden = YES;
    self.stepMemo.center = CGPointMake(160, 40);
    
    
    self.memoDoneButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
    self.memoDoneButton.frame = CGRectMake(100, 115.0, 126, 26);
    //    self.bottomBarButton = [[UIButton alloc] initWithFrame:CGRectMake(300.0, 0.0, 26.0, 26.0)];
    self.memoDoneButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.memoDoneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.memoDoneButton.backgroundColor = [UIColor clearColor];
    [self.memoDoneButton setTitle:@"Done Writing" forState:UIControlStateNormal];
    //	[self.bottomBarButton setBackgroundImage:checked forState:UIControlStateNormal];
    //    self.bottomBarButton.tag = 1;
    [self.memoDoneButton addTarget:self action:@selector(doneEditting) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.memoDoneButton];
    //self.memoDoneButton.hidden = YES;
    [self.memoDoneButton release];
    self.memoDoneButton.hidden = YES;
    self.textShown = NO;
}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //Return YES for supported orientations
	return YES;
}

#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == Submission_Form_Completed)
    {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }//end of if Submission_Form_Completed
    else if(actionSheet.tag == Submission_Confirmed)
    {
        if(buttonIndex != 0)
        {
            [self submitAction];
        }
    }
}



#pragma mark Button action
-(void)askConfirmation
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Successful submission results in displaying your strategy on the website. Can we proceed to submit the strategy?"
                                                   delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    [alert show];
    alert.tag = Submission_Confirmed;
    [alert release];
    
    
}

-(void)submitAction
{
    if([self.stepMemo.text isEqualToString:[NSString stringWithFormat:@"Why did you choose this route?"]] == NO)    
        [[self delegate] stepStrategySubmissionRequested:self];
    
    if(!self.requestorObj)
        self.requestorObj = [[SubmissionRequestor alloc] init];
    
    
    [self.requestorObj reuploadFile:treeID];
    [[self delegate] stepStrategyRequestCompleted:self];
    
}

-(void)showText
{
    if([self.stepMemo.text isEqualToString:[NSString stringWithFormat:@"Why did you choose this route?"]])
        self.stepMemo.text = @"";
    
    //NSLog(@"show text");
    
    if(self.textShown)
    {
        self.stepMemo.hidden = YES;
        self.memoDoneButton.hidden = YES;
        self.textShown = NO;
    }
    else
    {
        self.stepMemo.hidden = NO;
        self.memoDoneButton.hidden = NO;
        self.textShown = YES;
    }
}

-(void)doneEditting
{
    //NSLog(@"show doneedasdajs;as");
    [self.stepMemo resignFirstResponder];
    self.stepMemo.hidden = YES;
    self.memoDoneButton.hidden=YES;
    self.textShown = NO;

}

-(void)updateLabel:(NSInteger)nstep output:(NSInteger)kstep
{
    self.stepLabel.text = [NSString stringWithFormat:@"Step %d/%d", nstep, kstep];
}

#pragma mark TextView delegates
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
	[textView resignFirstResponder];
	return YES;    
}

- (void)saveAction:(id)sender
{
	[self.stepMemo resignFirstResponder];
	self.navigationItem.rightBarButtonItem = nil;	
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //[self.scroller scrollRectToVisible:CGRectMake(300, 200, 300, 300) animated:YES];
    //self.strategy.alpha = 1.0;
    
    if([textView.text isEqualToString:[NSString stringWithFormat:@"Why did you choose this route?"]])
        textView.text = @"";
    /*
	// provide my own Save button to dismiss the keyboard
	UIBarButtonItem* saveItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                target:self action:@selector(saveAction:)];
	self.navigationItem.rightBarButtonItem = saveItem;
	[saveItem release];
    */
    
}

/*
-(NSString*)nextStatePushed:(NSString*)passedComment{
    NSString *current = self.stepMemo.text;
    self.stepMemo.text=passedComment;
    return current;
    
}
 */

-(NSString*)replaceAndReturn:(NSString*)passedComment{
//    self.stepMemo.hidden = YES;
//    self.memoDoneButton.hidden=YES;
    [self doneEditting];
//    self.textShown = NO;
    NSString *current = [ NSString stringWithFormat:@"%@",self.stepMemo.text];

        NSLog(@"passed comment? %@", passedComment);
    
    self.stepMemo.text=passedComment;
    return current;
}




#pragma mark Submission Delegate
-(void)requestCompleted:(SubmissionRequestor *)req
{
    NSLog(@"delegate called");
    [[self delegate]stepStrategyRequestCompleted:self];
    
}



@end
