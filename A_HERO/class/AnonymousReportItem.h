//
//  AnonymousReportItem.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 1/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnonymousReportItem : NSObject
@property NSInteger stageNum, stepNum, indicatorFlag;
@end
