//
//  InitialRegistrationViewController.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * InitialRegistrationViewController class. 
 * Provides GUI for initial registration view
 * It overwrite application variable once 
 * the registration is completed so that 
 * the application will not show the same 
 * screen after the registration is completed
 *******************************************/

#import <UIKit/UIKit.h>
@protocol InitialRegistrationDelegate;
@interface InitialRegistrationViewController : UIViewController<UIScrollViewDelegate>
{
    id<InitialRegistrationDelegate>delegate;
}
@property(assign, nonatomic)id<InitialRegistrationDelegate>delegate;
-(void)setUpInitalView;
-(void)completeReg;
@end


@protocol InitialRegistrationDelegate <NSObject>
@optional
-(void)registrationCompleted:(InitialRegistrationViewController*)registration;
@end
