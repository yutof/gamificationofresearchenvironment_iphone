//
//  InstructionAlertView.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "InstructionAlertView.h"

@implementation InstructionAlertView
- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle okButtonTitle:(NSString *)okayButtonTitle
{
    
    if (self = [super initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:okayButtonTitle, nil])
    {
        self.frame = CGRectMake(0, 0, 260, 200);
        UILabel* inst = [[[UILabel alloc] initWithFrame:CGRectMake(12, 45, 260, 25)] autorelease];
        inst.text = [NSString stringWithFormat:@"Please use                \nto see your next step"];
        inst.backgroundColor = [UIColor clearColor];
        [self addSubview:inst];
        UIImageView *next = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_blue.png"]] autorelease];
        [self addSubview:next];
        next.center = CGPointMake(100, 30);
        
        /*
        UITextField *theTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)]; 
        [theTextField setBackgroundColor:[UIColor whiteColor]]; 
        [self addSubview:theTextField];
        self.textField = theTextField;
        [theTextField release];
        */
        
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 130.0); 
        [self setTransform:translate];
        
    }
    return self;
}
- (void)dealloc
{
    [super dealloc];
}
@end;
