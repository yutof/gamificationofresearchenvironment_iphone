//
//  PendingReportItem.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 1/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PendingReportItem : NSObject

@property NSInteger treeID;
@property NSInteger reportType;
@property NSInteger stepNum;
@property NSInteger stageNum;
@property double score;
@property (retain, nonatomic)NSString *name;
@property (retain, nonatomic)NSString *strategy;
@end
