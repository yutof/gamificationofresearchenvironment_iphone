//
//  MenuViewController.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/4/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * MenuViewController class. 
 * Provides GUI for the main menu and control
 * the hierarchy of the view throughout the 
 * application.
 *******************************************/

#import "MenuViewController.h"
#import "GameConfigurationController.h"
#import "GameViewController.h"
#import "StrategyReport.h"

@implementation MenuViewController
@synthesize menuTableView, cfgViewController;
@synthesize gameViewController, leaderboardController, achievementsController, score;
@synthesize about;
@synthesize playGameLabelImage, gameConfigLabelImage, aboutLabelImage; 
@synthesize reportList, initialRegView;


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupMenu];
    }

    return self;
}


- (void) reportScore: (int64_t) score1 forCategory: (NSString*) category
{
    GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:category] autorelease];
    scoreReporter.value = score1;
    
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
        if (error != nil)
        {
            NSLog(@"%@", error);
        }
    }];
}


-(void)setupMenu
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;

    playerID = @"";
    gameCenterAvailable = [self isGameCenterAPIAvailable];
    
    if(gameCenterAvailable == YES && gameCenterOff == NO)
    {
        [self authenticateUser];    
        gameCenter_Shown = YES;
    }//end of if(isGameCenterAPIAvailable())
    else
    {
        gameCenter_Shown = NO;
    }
    
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    self.view.tag = Tag_MenuView;
    
    self.menuTableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStyleGrouped];	
	self.menuTableView.delegate = self;
	self.menuTableView.dataSource = self;
	self.menuTableView.autoresizesSubviews = YES;
	[self.view addSubview:menuTableView];
	self.menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 90.0)];
    
    if(firstTime)
    {
        self.initialRegView = [[InitialRegistrationViewController alloc] initWithNibName:nil bundle:nil];
     
        self.initialRegView.delegate=self;
        
        [self.view addSubview:initialRegView.view];

    }
}//end of setupMenu

- (void)dealloc
{
    
    if(menuTableView)
        [menuTableView release];
    
    if(cfgViewController)
        [cfgViewController release];
    
    if(gameViewController)
        [gameViewController release];
    
    if(leaderboardController)
        [leaderboardController release];

    if(achievementsController)
        [achievementsController release];
    
    if(about)
        [about release];
    
    if(playGameLabelImage)
        [playGameLabelImage release];
    
    if(gameConfigLabelImage)
        [gameConfigLabelImage release];

    if(aboutLabelImage)
        [aboutLabelImage release];
    
    if(reportList)
        [reportList release];
    
    if(initialRegView)
        [initialRegView release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark Game Center Support
-(void)authenticateUser{

    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];    
    [[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:^(NSError *error) {
        
        if (localPlayer.isAuthenticated) {
            
            
            if (! playerID || ! [playerID isEqualToString:localPlayer.playerID]) {
                
                playerID = localPlayer.playerID;
                
                GKLeaderboard *query = [[GKLeaderboard alloc] initWithPlayerIDs: [NSArray arrayWithObject: playerID]];
                
                [query loadScoresWithCompletionHandler: ^(NSArray *scores, NSError *error) {
                    if (error != nil) {
                        NSLog(@"ERROR: Friends Table Score not loaded");
                    }
                    if (scores != nil) {
                        self.score = [[GKScore alloc] init];
                        self.score = [scores objectAtIndex:0];
                        [self.score release];
                    }
                    if(starNum < [query.localPlayerScore.formattedValue intValue])
                        starNum = [query.localPlayerScore.formattedValue intValue]; 
                    
                }];
            }
        } else {
            gameCenterAvailable = NO;
            NSLog(@"error? %@", error);
        }
    }];
}

// Check for the availability of Game Center API. 
-(BOOL) isGameCenterAPIAvailable
{
    // Check for presence of GKLocalPlayer API.
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    // The device must be running running iOS 4.1 or later.
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
    return (gcClass && osVersionSupported); 
}

-(void)update_GameCenterRows{

    if(gameCenterAvailable ==  YES && gameCenterOff == NO)
    {
        gameCenter_Shown = YES;
        [self.menuTableView reloadData];
    } 
    else
    {
        gameCenter_Shown = NO;
        [self.menuTableView reloadData];
    }
}
#pragma GameCenter Delegate
- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark GameConfiguration Delegate

-(void)gameCenterConfigChanged:(GameConfigurationController *)gm_cfg
{
    
    if(gm_cfg.gameCenterSwitch.on)
    {
        if(gameCenter_Shown == NO)
            [self update_GameCenterRows];
        //Show Game Center Stuff
    }
    else
    {
        if(gameCenter_Shown == YES)
            [self update_GameCenterRows];
        //Hide Game Center Stuff
    }//end of else if
}//end of gameCenterConfigChanged

#pragma mark GameViewController Delegate
-(void)stageWillRenew:(GameViewController *)stg
{
    if(stageNum >= 0)
    [GameConfigurationController setNodeNumForStage];
}

-(void)stageIsUpdated:(GameViewController *)gameView
{
    [self.cfgViewController updateStageTable];
    
}//end of stageIsUpdated

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"Menu";
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(gameCenterOff)
    return 4;//was 3
    else
    return 6;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]autorelease];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    switch (indexPath.section) {
        case Row_Game:
            cell.textLabel.text = @"Play Game";
            break;
            
        case Row_Cfg:
            cell.textLabel.text = @"Configure Game Settings";
            break;
            
            
        case Row_Report:
            cell.textLabel.text = @"Report";
            //if(!showReport)
            //                cell.textLabel.alpha = 0.5;
            break;

            
        case Row_About:
            cell.textLabel.text = @"About";
            break;
        case Row_Score:
                cell.textLabel.text = @"User Score";
            
            break;
            
        case Row_Achievement:
            cell.textLabel.text = @"User Achievement";
            break;                
            
        
        default:
            break;
    }//end of switch
    
    
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = @"";

    
    switch (section) {
        case Row_Game:
            title = NSLocalizedString(@"Play Game", @"Game");
            break;
        case Row_Cfg:
            title = NSLocalizedString(@"Game Settings", @"Settings");
            break;
        case Row_Report:
            title = NSLocalizedString(@"Report", @"Report");
            break;

        case Row_About:
            title = NSLocalizedString(@"About", @"About");
            break;
            
		case Row_Score:
            title = NSLocalizedString(@"Score", @"Score");
            break;
        case Row_Achievement:
            title = NSLocalizedString(@"Achievements", @"Achievements");
            break;
        default:
            break;
    }
    
     return title;
	
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	if(indexPath.row == 0)
	{
		if(indexPath.section == Row_Game)
		{
            
            if(!self.gameViewController)
            {
                if(stageNum>= 0)
                [GameConfigurationController setNodeNumForStage];
                
                self.gameViewController = [GameViewController getInstance];
                
                [self.gameViewController initRandomSeed];
                self.gameViewController.delegate = self;
            }
            
            if(showingReportDetail)
            {
                showingReportDetail = NO;
                gameViewController.showStrategyButton.hidden = YES;
                gameViewController.reportedStrategy.hidden = YES;
                [gameViewController reorganizeForGeneralGame];    
            }
            
            [self callGaneViewController];
            if(gameViewController.stepwiseReportShown)
            {
                [gameViewController removeStepWiseReportView];
            }
            else if(([self.gameViewController isGameProgress] || (stageNum == -1 && curStageNum >= 0) ))
            {

                if(gameViewController.gameEnded)
                    {
                        [self renewTheGame];

                        [self.navigationController pushViewController:gameViewController animated:YES];
                        self.navigationController.navigationBarHidden = NO;                            
                        
                    }
                    else
                    {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil 
                                               message:@"Do you want to resume game? or start new game?" delegate:self 
                                               cancelButtonTitle:@"New Game" otherButtonTitles:@"Resume", nil];
                        
                        alert.tag = Alert_GameResume;
                        [alert show];
                        [alert release];
                    }
                }
                else
                {
                    if(stageNum != -1 || self.gameViewController.gameEnded)
                    [self renewTheGame];
                    
                    [self.navigationController pushViewController:self.gameViewController animated:YES];
                    self.navigationController.navigationBarHidden = NO;    
                    
                }
		}//end of if
		else if(indexPath.section == Row_Cfg)
		{
            if(!self.cfgViewController)
            {
                self.cfgViewController = [[GameConfigurationController alloc] initWithNibName:nil bundle:nil];
                self.cfgViewController.delegate = self;
                if(stageNum>=0)
                    [self.cfgViewController randomStageEnabled:NO];
                else
                    [self.cfgViewController randomStageEnabled:YES];
            }
            [self.cfgViewController updateStageNumLabel];
            [self.navigationController pushViewController:self.cfgViewController animated:YES];
            self.navigationController.navigationBarHidden = NO;    
		}//end of else if row==2
		else if(indexPath.section == Row_Score)
        {

            if (self.leaderboardController == nil)
            {
            self.leaderboardController = [[[GKLeaderboardViewController alloc] init] autorelease];
            }
                self.leaderboardController.leaderboardDelegate = self;
                self.leaderboardController.category = @"1001";
                [self presentModalViewController:leaderboardController animated: YES];
                [self.view addSubview:leaderboardController.view];
        }
        else if(indexPath.section == Row_Achievement)
        {  
            self.achievementsController = [[GKAchievementViewController alloc] init];
            if (self.achievementsController != nil)
            {
                self.achievementsController.achievementDelegate = self;
                [self presentModalViewController: self.achievementsController animated: YES];
            }
            [self.achievementsController release];
        }//end of if indexPath.section == 3
        else if(indexPath.section == Row_About)
        {
            if(!self.about)
            {
                self.about = [[AboutWebViewControllerClass alloc] initWithNibName:nil bundle:nil];
            }
            
            [self.navigationController pushViewController:self.about animated:YES];
            self.navigationController.navigationBarHidden = NO; 
        }
        else if(indexPath.section == Row_Report)
        {
            [ReachabilityUpdator updateServerAvailability];
            if(!serverAvailable)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil 
                                        message:@"We are sorry. Failed to get latest reports because network is not available. " delegate:nil 
                                                      cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                
                [alert show];
                [alert release];
            }
            SubmissionRequestor *t;
            t = [[[SubmissionRequestor alloc] init]autorelease];
            [t getReportList];
            
            if(!self.reportList)
            {
                self.reportList = [[ReportTypeView alloc] initWithNibName:nil bundle:nil];
            }
            [self.navigationController pushViewController:self.reportList animated:YES];
            self.navigationController.navigationBarHidden = NO; 
            
        
        }
    }//end of if row==0
}//end of didSelect


#pragma mark updator for other classes
-(void)callGaneViewController
{
    if(stageNum >= 0)
  [GameConfigurationController setNodeNumForStage];
    
    if(self.gameViewController)
  [self.gameViewController shownGhostCheck];
    
    if(curStageNum < 0)
        [self.gameViewController modifyViewFor:GameMode_Random];
    else
        [self.gameViewController modifyViewFor:GameMode_RegStage];
    
}

-(void)update_StrategyTreeID
{
    [self.gameViewController updateStrategyReportTreeID];
}


-(void)submitPendingRecords
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    
    dispatch_async(queue, ^(void) {

        SubmissionRequestor *rq = [[[SubmissionRequestor alloc] init] autorelease];
        [rq submitPendingRecord];
        
    });
}

-(void)showReportDoneAlert
{
    if(self.gameViewController)
        [self.gameViewController done_reporting_alert];
}

#pragma mark UIAlert Delegate

-(void)renewTheGame
{
    [self.gameViewController updateStageNumber];
    [self.gameViewController initRandomSeed];
    [self.gameViewController renewTheGame];
}


- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == Alert_GameResume)
    {
        if(buttonIndex == 0)
        {
            if(stageNum >= 0)
            [GameConfigurationController setNodeNumForStage];
            [self renewTheGame];
        }
        [self.navigationController pushViewController:gameViewController animated:YES];
        self.navigationController.navigationBarHidden = NO;
    }
}

#pragma mark Initial Registration Delegate
-(void)registrationCompleted:(InitialRegistrationViewController *)registration
{
    [self.initialRegView.view removeFromSuperview];
    [self.initialRegView release];
    self.initialRegView = nil;
    firstTime = NO;
    
    NSUserDefaults	*userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSNumber numberWithBool:firstTime] forKey:@"first"];
    
    if(!self.gameViewController)
    {
        if(stageNum>= 0)
            [GameConfigurationController setNodeNumForStage];
        
        self.gameViewController = [GameViewController getInstance];
        
        [self.gameViewController initRandomSeed];
        self.gameViewController.delegate = self;
    }
    
    if(showingReportDetail)
    {
        showingReportDetail = NO;
        gameViewController.showStrategyButton.hidden = YES;
        gameViewController.reportedStrategy.hidden = YES;
        [gameViewController reorganizeForGeneralGame];    
    }
    
    [self callGaneViewController];
    if(gameViewController.stepwiseReportShown)
    {
        [gameViewController removeStepWiseReportView];
    }
    else if(([self.gameViewController isGameProgress] || (stageNum == -1 && curStageNum >= 0) ))
    {
        
        if(gameViewController.gameEnded)
        {
            [self renewTheGame];
            
            [self.navigationController pushViewController:gameViewController animated:YES];
            self.navigationController.navigationBarHidden = NO;                            
            
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil 
                                                            message:@"Do you want to resume game? or start new game?" delegate:self 
                                                  cancelButtonTitle:@"New Game" otherButtonTitles:@"Resume", nil];
            
            alert.tag = Alert_GameResume;
            [alert show];
            [alert release];
        }
    }
    else
    {
        if(stageNum != -1 || self.gameViewController.gameEnded)
            [self renewTheGame];
        
        [self.navigationController pushViewController:self.gameViewController animated:YES];
        self.navigationController.navigationBarHidden = NO;    
        
    }

    
}
@end
