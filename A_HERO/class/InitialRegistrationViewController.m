//
//  InitialRegistrationViewController.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * InitialRegistrationViewController class. 
 * Provides GUI for initial registration view
 * It overwrite application variable once 
 * the registration is completed so that 
 * the application will not show the same 
 * screen after the registration is completed
 *******************************************/

#import "InitialRegistrationViewController.h"

@implementation InitialRegistrationViewController

@synthesize delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setUpInitalView];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];    
}

-(void)dealloc
{
    [super dealloc];
}
#pragma mark - View lifecycle
-(void)setUpInitalView
{
    self.view.backgroundColor = [UIColor whiteColor];
    CGRect frameSize= CGRectMake(0, 0, 320, 480);
	UIScrollView* scroller = [[[UIScrollView alloc] initWithFrame:frameSize]autorelease];
	scroller.scrollEnabled = YES;
	scroller.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	scroller.clipsToBounds = YES;		
	scroller.delegate = self;
    [scroller setContentSize:CGSizeMake(320, 600)];
	[self.view addSubview:scroller];
    
    UIImageView *attention = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"attention_mark.png"]]autorelease];
    [scroller addSubview:attention];
    attention.center = CGPointMake(160, 50);
 
    CGRect textframe = CGRectMake(0, 8.0, 310, 400);
    UITextView *initialDescription = [[UITextView alloc] initWithFrame:textframe];
    initialDescription.scrollEnabled = YES;
    initialDescription.editable = NO;
    initialDescription.textColor = [UIColor blackColor];
    initialDescription.font = [UIFont systemFontOfSize:17.0];
    initialDescription.backgroundColor = [UIColor whiteColor];
    [scroller addSubview:initialDescription];
    initialDescription.center = CGPointMake(160,320);
    initialDescription.text = @"Thank you very much for downloading Setsuden!!! Setsuden is developed for academic research purpose. Therefore it collects your game log and submits the log to our webserver. If you want to disable such feature, you can disable autosubmission by setting Autosubmission 'Off' in Game Configration Menu.\n The log that will be submitted consists of game logs only. Therefore we will not be able to know any other information other than how you solved the problem. Thank you very much for reading this!! We hope you enjoy the game!!";
    

    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(182.0, 5.0, 120.0, 30.0)];
    btn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btn setTitle:@"Confirm" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor clearColor];
    UIImage *newImage = [[UIImage imageNamed:@"whiteButton.png"] stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	[btn setBackgroundImage:newImage forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(completeReg) forControlEvents:UIControlEventTouchDown];
    [scroller addSubview:btn];
    btn.center = CGPointMake(160, 530);
    [btn release];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark Button Action
-(void)completeReg
{
    [[self delegate] registrationCompleted:self];
}
@end
