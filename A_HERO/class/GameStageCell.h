//
//  GameStageCell.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 11/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameStageCell : UITableViewCell
{
    UILabel *passwordLabel, *percentageLabel;
    
}
@property(retain, nonatomic)UILabel *passwordLabel, *percentageLabel;
- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier;
-(void)setData:(NSString*)password  percentage:(NSString*)str;

@end
