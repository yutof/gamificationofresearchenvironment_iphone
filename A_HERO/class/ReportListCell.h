//
//  ReportListCell.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 1/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReportListCell class. 
 * Provides GUI for each row for ReportList
 * table
 *******************************************/

#import <Foundation/Foundation.h>
#import "ReportItem.h"
@interface ReportListCell : UITableViewCell
{
    UILabel *stageNumLabel, *dateTimeLabel;
}
@property(retain, nonatomic)    UILabel *stageNumLabel, *dateTimeLabel;

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier;
-(void)setData:(ReportItem*)reportObj;
@end
