//
//  SQLManager.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/18/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h> // Import the SQLite database framework
#import "Constants.h"
#import "ReportItem.h"
#import "PendingReportItem.h"
#import "ASIFormDataRequest.h"
#import "AnonymousReportItem.h"


@interface SQLManager : NSObject {
    // Database variables
	NSString *databaseName;
	NSString *databasePath;
}
-(id)initWithReportID:(int)reportID;
-(void) checkAndDownloadDatabase:(int)reportID;
-(void)downloadTheSQLFile:(int)reportID;
-(void)checkAndCreateDatabase;

-(void)query:(NSString*)statement;
-(BOOL)queryWithErrorcheck:(NSString*)statement;

-(void)copyResult:(struct nodeType*)nodeArray Step:(int)step Previous:(int)prev TreeID:(int)treeID RelyingNode:(int *)rel totalPower:(double *)totalPower;
-(void)restoreStep:(int)step TreeID:(int)treeID relyingNode:(int *)rel destinationNode:(int*)dest totalPower:(double *)totalPower;
-(int)createTree:(int)tree_stageNum;

-(void)copyTree:(int*)xArray y:(int*)yArray tree:(int)treeIDNum;
-(int)getRowCount:(NSString*)tableName;

-(void)returnPendingReport:(int*)pr_treeID type:(int*)rptTypeTag stp:(int*)stepNumber stg:(int*)stageNumber nm:(NSString**)pr_name strtgy:(NSString**)pr_strategy scr:(double*)pr_score file:(NSString**)filePath;

-(BOOL)isPending:(int)del_tree_id;
-(void)copyStageResult:(BOOL*)cleared score:(double*)scr;
-(NSString*)getText:(NSString*)query;
-(int)getInteger:(NSString*)statement;
-(void)tableUpdate_v102;
-(void)tableUpdate_v110;
-(void)tableUpdate_v120;
-(void)tableUpdate_v121;
-(NSMutableArray*)getReportList:(int)type;
-(NSMutableArray*)getPendingReportList;
-(NSMutableArray*)getPendingAnonymousReportList;
-(NSString*)convertDateFormat:(NSString*)datetime;

@property(retain, nonatomic) NSString *databaseName;
@property(retain, nonatomic) NSString *databasePath;
@end
