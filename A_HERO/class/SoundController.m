//
//  SoundController.m
//
//  Created by Yuto Fujikawa on 7/9/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * SoundController class. 
 * Manages all sound effect in the application
 * Uses both AVFoundation and OpenAL
 *******************************************/


#import "SoundController.h"
#import "oalPlayback.h"
#import "MyOpenALSupport.h"


@implementation SoundController
@synthesize soundFileURLRef;
@synthesize win_1,win_2, lose_1, lose_2, refresh, cngRlyngNode, prev, next, killed, goHeaven, revive;
@synthesize coinArray, coinPath, applause;
@synthesize coin;


#pragma mark Object Init / Maintenance
void interruptionListener(	void *	inClientData,
                          UInt32	inInterruptionState);
void RouteChangeListener(	void *                  inClientData,
                         AudioSessionPropertyID	inID,
                         UInt32                  inDataSize,
                         const void *            inData);


void interruptionListener(	void *	inClientData,
                          UInt32	inInterruptionState)
{
	SoundController* THIS = (SoundController*)inClientData;
	if (inInterruptionState == kAudioSessionBeginInterruption)
	{
        alcMakeContextCurrent(NULL);		
	}
	else if (inInterruptionState == kAudioSessionEndInterruption)
	{
		OSStatus result = AudioSessionSetActive(true);
		if (result) NSLog(@"Error setting audio session active! %@\n", result);
        
		alcMakeContextCurrent(THIS->context);
        
	}
}

void RouteChangeListener(	void *                  inClientData,
                         AudioSessionPropertyID	inID,
                         UInt32                  inDataSize,
                         const void *            inData)
{
	CFDictionaryRef dict = (CFDictionaryRef)inData;
	
    CFDictionaryGetValue(dict, CFSTR(kAudioSession_AudioRouteChangeKey_OldRoute));
	
	UInt32 size = sizeof(CFStringRef);
	
	CFStringRef newRoute;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &size, &newRoute);
    
}


-(id)init{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^(void) { 

    
    OSStatus result = AudioSessionInitialize(NULL, NULL, interruptionListener, self);
    if (result) NSLog(@"Error initializing audio session! %ld\n", result);
    else {
        // if there is other audio playing, we don't want to play the background music
        UInt32 size = sizeof(iPodIsPlaying);
        result = AudioSessionGetProperty(kAudioSessionProperty_OtherAudioIsPlaying, &size, &iPodIsPlaying);
        if (result) NSLog(@"Error getting other audio playing property! %ld", result);
        
        // if the iPod is playing, use the ambient category to mix with it
        // otherwise, use solo ambient to get the hardware for playing the app background track
        UInt32 category = (iPodIsPlaying) ? kAudioSessionCategory_AmbientSound : kAudioSessionCategory_SoloAmbientSound;
        
        result = AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(category), &category);
        if (result) NSLog(@"Error setting audio session category! %ld\n", result);
        
        result = AudioSessionAddPropertyListener(kAudioSessionProperty_AudioRouteChange, RouteChangeListener, self);
        if (result) NSLog(@"Couldn't add listener: %ld", result);
        
        result = AudioSessionSetActive(true);
        if (result) NSLog(@"Error setting audio session active! %ld\n", result);
    }
	NSBundle* bundle = [NSBundle mainBundle];
	CFURLRef fileURL = (CFURLRef)[[NSURL fileURLWithPath:[bundle pathForResource:@"coin" ofType:@"wav"]] retain];
    self.coin = [[oalPlayback alloc] init:fileURL];
    
  
	NSString *Path;	
    Path = [bundle pathForResource:@"draw" ofType:@"mp3"];
    self.win_1 = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];
    Path = [bundle pathForResource:@"win" ofType:@"mp3"];
    self.win_2 = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];	
    Path = [bundle pathForResource:@"lose_1" ofType:@"mp3"];
    self.lose_1 = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];
    Path = [bundle pathForResource:@"lose_2" ofType:@"mp3"];
    self.lose_2 = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];
    Path = [bundle pathForResource:@"refresh" ofType:@"mp3"];
    self.refresh = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];
    
    Path = [bundle pathForResource:@"prev" ofType:@"mp3"];
    self.prev = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];
    Path = [bundle pathForResource:@"next" ofType:@"mp3"];
    self.next = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];
    
    Path = [bundle pathForResource:@"jump_1" ofType:@"mp3"];
    self.cngRlyngNode = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];

    Path = [bundle pathForResource:@"killed" ofType:@"mp3"];
    self.killed = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];    
    Path = [bundle pathForResource:@"death" ofType:@"mp3"];
    self.goHeaven = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];    
    
    Path = [bundle pathForResource:@"reverse" ofType:@"mp3"];
    self.revive = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];    
    
    Path = [bundle pathForResource:@"app-8" ofType:@"mp3"];
    self.applause = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:Path] error: nil];    
    
    });
    
    return self;
    
    

}

-(void)playSoundEffect:(enum SoundList)tag
{    
    switch (tag) {
        case CoinSound:
            [self.coin startSound];
            break;
        case PrevStepSound:
            self.prev.currentTime = 0;
            [self.prev prepareToPlay];
            [self.prev play];
            break;
        case NextStepSound:
            self.next.currentTime = 0;
            [self.next prepareToPlay];
            [self.next play];
            break;
        case ChangeRelyingNodeSound:
            self.cngRlyngNode.currentTime =0;
            [self.cngRlyngNode prepareToPlay];
            [self.cngRlyngNode play];
            break;
        case WinningSound:
            [self.win_1 prepareToPlay];
            [self.win_1 play];
            
            break;
        case LosingSound:
            if((int)(rand()%2) == 1)
            {
                self.lose_1.currentTime = 0;
                [self.lose_1 prepareToPlay];
                [self.lose_1 play];
            } 
            else
            {
                self.lose_2.currentTime =0;
                [self.lose_2 prepareToPlay];
                [self.lose_2 play];
            }
            
            break;
        case RefreshSound:
            self.refresh.currentTime =0;
            [self.refresh prepareToPlay];
            [self.refresh play];
            
            break;
        case AchievementSound:
            self.win_2.currentTime =0;
            [self.win_2 prepareToPlay];
            [self.win_2 play];
            break;
            
        case KilledSound:
            self.killed.currentTime =0;
            [self.killed prepareToPlay];
            [self.killed play];
            break;
        case GoHeavenSound:
            self.goHeaven.currentTime =0;
            [self.goHeaven prepareToPlay];
            [self.goHeaven play];
            break;
        case ReviveSound:
            self.revive.currentTime =0;
            [self.revive prepareToPlay];
            [self.revive play];
            break;
            
        case ApplauseSound:
            self.applause.currentTime =0;
            [self.applause prepareToPlay];
            [self.applause play];
            break;
            
        default:
            break;
    }
    
}//end of playSoundEffect


-(void)stopSoundEffect:(enum SoundList)tag
{    
    switch (tag) {
        case PrevStepSound:
            [self.prev stop];
            
            break;
        case NextStepSound:
            [self.next stop];
            
            break;
        case ChangeRelyingNodeSound:
            [self.cngRlyngNode stop];
            
            break;
        case WinningSound:
            [self.win_1 stop];
            
            break;
        case LosingSound:
                [self.lose_1 stop];
                [self.lose_2 stop];
            
            break;
        case RefreshSound:
            [self.refresh stop];
            
            break;
        case AchievementSound:
            [self.win_2 stop];
            break;
            
        case KilledSound:
            [self.killed stop];
            break;
        case GoHeavenSound:
            [self.goHeaven stop];
            break;
        case ReviveSound:
            [self.revive stop];
            break;
            
        case ApplauseSound:
            [self.applause stop];
            break;
            
        default:
            break;
    }
    
}//end of playSoundEffect



-(void)dealloc{    
    [prev release];
    [next release];
    [cngRlyngNode release];
    
    [win_1 release];
    [win_2 release];
    [lose_1 release];
    [lose_2 release];
    [refresh release];
    
    [killed release];
    [goHeaven release];
    [super dealloc];
    
}//end of dealloc


#pragma mark AVAudioPlayer delegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{ 
//    NSLog(@"releasing?");
}




@end
