//
//  GameStageCell.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 11/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GameStageCell.h"

@implementation GameStageCell
@synthesize passwordLabel, percentageLabel;


-(void)dealloc
{
    [passwordLabel dealloc];
    [percentageLabel dealloc];
    [super dealloc];
}
#pragma mark - View lifecycle

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier
{
    if(self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier])
    {
        
		UIView *myContentView = self.contentView;
		[myContentView setBackgroundColor:[UIColor clearColor]];
        self.passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.passwordLabel.font = [UIFont systemFontOfSize:15.0];
        self.passwordLabel.textAlignment = UITextAlignmentCenter;
        self.passwordLabel.textColor = [UIColor blackColor];
        self.passwordLabel.backgroundColor = [UIColor clearColor];
        self.passwordLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
//        self.passwordLabel.text = @"";
//        self.passwordLabel.center = CGPointMake(200, 30);
        [myContentView addSubview:self.passwordLabel];
        self.passwordLabel.center = CGPointMake(200, 30);
        
        self.percentageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.percentageLabel.font = [UIFont systemFontOfSize:15.0];
        self.percentageLabel.textAlignment = UITextAlignmentCenter;
        self.percentageLabel.textColor = [UIColor blackColor];
        self.percentageLabel.backgroundColor = [UIColor clearColor];
        self.percentageLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        //self.passwordLabel.text = @"";
        [myContentView addSubview:self.percentageLabel];
        self.passwordLabel.center = CGPointMake(200, 30);
    }

    return self;
}

-(void)setData:(NSString*)password  percentage:(NSString*)str
{
    if(!self.passwordLabel)
    {
        UIView *myContentView = self.contentView;
		[myContentView setBackgroundColor:[UIColor clearColor]];
        self.passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.passwordLabel.font = [UIFont systemFontOfSize:15.0];
        self.passwordLabel.textAlignment = UITextAlignmentCenter;
        self.passwordLabel.textColor = [UIColor blackColor];
        self.passwordLabel.backgroundColor = [UIColor clearColor];
        self.passwordLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        //self.passwordLabel.text = @"";
        //self.passwordLabel.center = CGPointMake(200, 30);
        [myContentView addSubview:self.passwordLabel];
        self.passwordLabel.center = CGPointMake(210, 15);
    
        self.percentageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.percentageLabel.font = [UIFont systemFontOfSize:15.0];
        self.percentageLabel.textAlignment = UITextAlignmentCenter;
        self.percentageLabel.textColor = [UIColor blackColor];
        self.percentageLabel.backgroundColor = [UIColor clearColor];
        self.percentageLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        //self.passwordLabel.text = @"";
        [myContentView addSubview:self.percentageLabel];
        self.percentageLabel.center = CGPointMake(210, 35);
    }
    self.passwordLabel.text = password; 
    self.percentageLabel.text = str;
}


@end
