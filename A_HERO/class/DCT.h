//
//  DCT.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/25/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface DCT : NSObject {
    struct nodeType nodeList[100];
    int NodeArea[100][100];
    double pMat[100][100];
    double o_pMat[100][100];
    double brdcstTree[100];
    double totalEn;
}
-(void)copyDctAnswer:(struct nodeType*)dctArray;
-(id)copyNodesOver:(struct nodeType*)originalNodes;

-(void)initializeTree;

-(void) constructAOMEGaTree:(int)size source:(int)sID;
//void constructAOMEGaTree(int size, int sID);

-(void)sortList:(struct nodeType*)list src:(int)srcidx size:(int)size;
//void sortList(struct nodeType* list, int srcidx, int size);

-(void)findClosestNeighbor:(struct nodeType*)nd size:(int)size;
//void findClosestNeighbor(struct nodeType* nd, int size);

-(double)calcDistance:(int)idx1x idx2:(int)idx2x idy1:(int)idx1y idy2:(int)idx2y;
//double calcDistance(int idx1x, int idx2x, int idx1y, int idx2y);

-(void)displayArea:(int)size;
//void displayArea(int size);

-(double)computeEnergyGain:(int) i node:(struct nodeType)cp eGainArray:(double*)eGain eGainReference:(double*)enGain minPowerReference:(double*)minPower size:(int)size;
//double computeEnergyGain(int i, struct nodeType* cp, double * eGain, double *enGain, double *minPower, int size);

-(double)constructDCT:(int)size;
//void constructDCT(int size);

@end
