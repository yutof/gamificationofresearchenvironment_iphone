//
//  DCT.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/25/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "DCT.h"


@implementation DCT

-(id)copyNodesOver:(struct nodeType *)originalNodes
{
    for(int i =0; i< NoNodes; i++)
    {
        nodeList[i].x = originalNodes[i].x;
        nodeList[i].y = originalNodes[i].y;
        nodeList[i].parent_id = originalNodes[i].parent_id;
        nodeList[i].power = originalNodes[i].power;
        nodeList[i].id_number = originalNodes[i].id_number;
        nodeList[i].inTree = originalNodes[i].inTree;
    }//end of for
    
    [self initializeTree];
    
    return self;
}//end of copyNodesOver

-(void)initializeTree
{
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^(void) { 
	int i, j;//, k, x, y;
	totalEn = 0;
    
	for(i =0; i<NoNodes; i++)
		for(j =0; j< NoNodes; j++)
		{
			pMat[i][j] = [self calcDistance:nodeList[i].x idx2:nodeList[j].x idy1:nodeList[i].y idy2:nodeList[j].y];
            //[self calcDistance:nodeList[i].x, :nodeList[j].x, :nodeList[i].y, :nodeList[j].y];
            o_pMat[i][j] = pMat[i][j];
		}//end of for
    
    [self findClosestNeighbor:nodeList size:NoNodes];
    	//findClosestNeighbor(nodeList, NoNodes);
    
//        });    
//    NSLog(@"Initialize the tree");
}//end of initialize tree;



-(double)calcDistance:(int)idx1x idx2:(int)idx2x idy1:(int)idx1y idy2:(int)idx2y
{
	return (double)(idx1x - idx2x)*(idx1x - idx2x) + (idx1y - idx2y)*(idx1y - idx2y);
}

-(void)findClosestNeighbor:(struct nodeType*)nd size:(int)size;
{
	double minDist, curDis;
	//minDist = DBL_MAX;//100000;
	int i, j;
	struct nodeType temp;
	//nd = head;
    
	for(i=0; i< size; i++)
	{
		minDist =DBL_MAX; 
 		for(j=0; j<size; j++)
		{
			temp = nd[j];
			if(temp.id_number != nd[i].id_number)
			{
				curDis = pMat[temp.id_number][nd[i].id_number];
				if(curDis < minDist)
				{
					nd[i].cn_id = temp.id_number;
					minDist = curDis;
				}//end of if
			}//end of if
			//temp= temp->next;
		}//end of for
		//nd = nd->next;
	}
}//end of findClosestNeighbor


-(void)sortList:(struct nodeType*)list src:(int)srcidx size:(int)size
{
	int i, j, minID=0; 
	double minDistance;
    
    
	struct nodeType temp;
	for(i =0; i<size -1 ; i++)
	{
		minDistance = DBL_MAX;//100000;
		for(j=i; j<size; j++)
		{
			if(nodeList[srcidx].id_number != list[j].id_number )
			{ 
				if(pMat[srcidx][list[j].id_number] < minDistance)
				{
					minID = j;
					minDistance = pMat[srcidx][list[j].id_number];
				}
			}//end of if
		} //end of for
        
		temp = list[i];
		list[i] = list[minID];
		list[minID] = temp;
        
	}//end of for
    
}//end of sortList


-(void)displayArea:(int)size
{
    /*
    printf("\nDisplay Area!!\n");

    for(int i=0; i< size; i++)
    {
        printf("\nID:%d ParentID:%d Power:%f",nodeList[i].id_number, nodeList[i].parent_id, nodeList[i].power);
    }*/
    
}//end of displayArea

-(double)computeEnergyGain:(int) i node:(struct nodeType)cp eGainArray:(double*)eGain eGainReference:(double*)enGain minPowerReference:(double*)minPower size:(int)size
{
	int qq=0 ;
    //int xk,
//	xk = 0;
    
	if(cp.inTree)
		return - DBL_MAX;
    
    
	double inclPower=0;
	for(qq=0; qq<size; qq++)
		inclPower += brdcstTree[qq];
    
	if (o_pMat[cp.cn_id][cp.id_number] > brdcstTree[cp.cn_id]) {
		*enGain = *enGain + (o_pMat[cp.cn_id][cp.id_number] - brdcstTree[cp.cn_id]);
		if (pMat[i][cp.id_number] > *minPower) {
			*enGain -= (pMat[i][cp.id_number] - *minPower);
		}
        
	} else {
		*enGain = *enGain + - (pMat[i][cp.id_number] - *minPower);
	}
    
	//////	if (brdcstTree[cp->cn_id] < pMat[cp->cn_id][cp->id])
	//////		brdcstTree[cp->cn_id] = pMat[cp->cn_id][cp->id];
	if (brdcstTree[cp.cn_id] < o_pMat[cp.cn_id][cp.id_number])
		brdcstTree[cp.cn_id] = o_pMat[cp.cn_id][cp.id_number];
    
	*minPower = pMat[i][cp.id_number];//pMat[cp->id - 1][i];
    
	return  *enGain;
    
}//end of computeEnergyGain

-(void) constructAOMEGaTree:(int)size source:(int)sID
{

    struct nodeType copy[size];//declare copy
	int i, j, minID, rsID;
	double curPower, eGain[size], maxPower=DBL_MAX, enGain, minPower;
	nodeList[sID].inTree = true;
	nodeList[sID].parent_id = -1;
	int prevTreeSize[5] ={0,0,0,0,0};
	int minIDHist[5] ={size,size,size,size,size};
	int treeSize = 1;
	int counter =0;
	int realRSID, realMINID;
    
	for(i=0; i<size; i++)
	{
		eGain[i]=0;
		copy[i].id_number = nodeList[i].id_number;
		copy[i].x = nodeList[i].x;
		copy[i].y = nodeList[i].y;
		copy[i].cn_id = nodeList[i].cn_id;
		copy[i].inTree = nodeList[i].inTree;
	}//end of for
    
	//	nodeList[0].inTree = true;
	//////	nodeList[sID].inTree = true;
	//////	copy[0].inTree = true;
	//////	copy[0].relyingNode = true;
    
	while(treeSize < size)
	{
//		allTrue = true;
        
        
        /*		for(i=0; i< 5; i++)
         if(minID != minIDHist[i])
         allTrue = false;
         //  if(treeSize != prevTreeSize[i])
         //    allTrue = false;
         
         if(allTrue)
         {
         printf("\n\n\nSomething went wrong\nNow:%d\n", treeSize);
         displayArea(size);
         
         for(i=0; i<5; i++)
         {printf("%d, Intree:",minIDHist[i]);
         printf("%s\n", (copy[minIDHist[i]].inTree) ? "Yes": "NO");
         }
         printf("\n");
         exit(1);
         }//end of if
         
         */
		minID = 0; 
		maxPower = - DBL_MAX;//-100000;
		rsID = 0;
		enGain =0;
		minPower = 0;// DBL_MAX;
                
		for(i =0; i < size; i++)
		{ 
            
			////// enGain and minPower seem to be initialized for every i
			////// Also brdcstTree should be initialized for every i
            
			enGain = 0;
			minPower =0;
			for (j = 0; j < size; j++) {
				//////				brdcstTree[j] = 0;
				brdcstTree[j] = nodeList[j].power;
			}
            
			for(j=0;j<size;j++)
				copy[j].inTree = nodeList[copy[j].id_number].inTree;
            
			if(nodeList[i].inTree == YES)
			{ 
                
                
                [self sortList:copy src:i size:size];
                
                
				for(j=0; j< NoNodes; j++)
				{
                    
					//if(i == j)
					//break;
                    
					//////	if i == copy[j].id, it means we have checked every neighbor of i, so stop.
					if (i == copy[j].id_number) break;
                    
					//  brdcstTree[j] = pMat[i][j];
					if (copy[j].inTree) {continue;}
                    
					curPower = [self computeEnergyGain:i node:copy[j] eGainArray:eGain eGainReference:&enGain minPowerReference:&minPower size:size];
                    //computeEnergyGain(i,&copy[j],eGain, &enGain, &minPower, size);
                    
                    
                    
					if(maxPower < curPower /*&& copy[j].inTree == false*/)
					{
						maxPower = curPower;
						minID = j;
						rsID = i;//nodeList[i].id;
						//if(cop)
						//////						copy[i].power = pMat[copy[j].id][nodeList[i].id];
						//////	Since we're using incremental power matrix, we need to use the source and destination idexes for the transmission carefully.
						copy[i].power = pMat[nodeList[i].id_number][copy[j].id_number];
                        
						copy[j].inTree = true;
                        
						//printf("\n%d update the minID!!\n", minID);
						realRSID = nodeList[i].id_number;// + 1;
						realMINID = copy[j].id_number;// + 1;
                        
						///printf("\nIs %d in tree?  %s\n", copy[j].id + 1, copy[j].inTree ? "YES" : "NO" );
                        
					}//end of if
					else if(maxPower == curPower)
					{
						//////						if(pMat[rsID][copy[minID].id] >= pMat[i][j])
                        
                        
						if(pMat[rsID][copy[minID].id_number] > pMat[i][copy[j].id_number])
						{
							minID = j;
							rsID = i;
							//	  copy[j].power = pMat[copy[j].id - 1][nodeList[i].id - 1];
							copy[j].inTree = true;
							maxPower = curPower;
							//	  realRSID = copy[i].id;
							realRSID = nodeList[i].id_number;
							realMINID = copy[j].id_number;                            
						}//end of if
						//else
						//	printf("\n\n\n\n\nTie break occured, but not updated!!\n\n\n\n\n");
                        
					}//end of else if
                    
					//					printf("\n max power %e ",maxPower);
					//printf("No, it is not\n");
                    
				}//end of for j	
                
				for(j =0; j<size; j++)
				{
					eGain[j]=0;
					//	   if(j>0)
					copy[j].inTree = false;
                    
					//	copy[i].id = nodeList[i].id;
                    
					//  copy[j].inTree = false;
				}//end of for
                
                
                
			}//end of if	
		}//end of for i
                

		//nodeList[rsID].power += pMat[rsID][copy[minID].id];
		//////		nodeList[rsID].power = o_pMat[rsID][copy[minID].id];
		//////	No sortList, so just use realMINID.
        
        
		nodeList[rsID].power = o_pMat[rsID][realMINID];
        
		//i=0;
        
		//	while(i <= minID) 
		for(i=0; i<size; i++)
		{
            
            
			//////			if(nodeList[copy[i].id].inTree == false && pMat[rsID][copy[i].id] <= pMat[rsID][copy[minID].id])
			//////	Make array index as simple as possible.
			if(nodeList[i].inTree == false && pMat[rsID][i] <= pMat[rsID][realMINID])
			{
				//////				nodeList[copy[i].id].inTree = true;
				//////				nodeList[copy[i].id].parent_id = nodeList[rsID].id;
				nodeList[i].inTree = true;
				nodeList[i].parent_id = realRSID;
                
                
				//Need to update the pMat here!!
                
                
                
				treeSize++;
				//printf("\n\nsize added!! %d\n%d is in tree now\n", treeSize, nodeList[copy[i].id].id +1);
			}//end of if
			//			else
			//	{
			//		printf("\nnope, %d is in tree", nodeList[copy[i].id].id + 1);
			//	}
			//		i++;
            
            
            
		}//end of while 
        
		//////		curPower = pMat[rsID][copy[minID].id];
		curPower = pMat[rsID][realMINID];
		//////printf("Tx[%d][%d] (%f) is chosen. EnGain is %f.\n", rsID, realMINID, curPower, maxPower);
		for(i =0; i < size; i++)
		{
			//////			if(i == 0 || nodeList[i].parent_id >= 0 || i == copy[minID].id)
			//////	set transmission power to either source node or a node in the tree as 0.
			if(i == sID || nodeList[i].parent_id >= 0)
			{
				pMat[rsID][i]  = 0;
				//pMat[i][rsID] =0;
			}
			else
			{
				pMat[rsID][i] -= curPower;
				//pMat[i][rsID] -= curPower;
			}
            
		}//end of while
        
		////// It seems that curPower should be added to the totalEn, not pMat which is 0.
		totalEn += curPower;
        
        
		prevTreeSize[counter++%5] = treeSize;
		minIDHist[counter++%5] = minID; //realMINID;
        
	}//end of while

}//end of constructAOMEGaTree

-(double)constructDCT:(int)size
{
    
    
    int t, WCnt, W[size];
    
	double power[size], mpower[size], powerSum, minPower = DBL_MAX;
	struct nodeType copy[size];//, tempCopy[size], minTree[size];//declare copy
	int i,j,k, parent[size], mparent[size], bestParent[size];
	double bestPower[size], bestSum = DBL_MAX;
    
    
    
	for(t =0; t< size ; t++)
	{
		for(i =0; i<size; i++)
		{
			nodeList[i].inTree = false;
			nodeList[i].parent_id = -2;
			nodeList[i].power = 0;
			for(j =0; j< size; j++)
			{
				pMat[i][j] = [self calcDistance:nodeList[i].x idx2:nodeList[j].x idy1:nodeList[i].y idy2:nodeList[j].y];
			}
		}
        
        [self constructAOMEGaTree:size source:t];
        
		for(i =0; i<size; i++)
			for(j =0; j< size; j++)
			{
                pMat[i][j] = [self calcDistance:nodeList[i].x idx2:nodeList[j].x idy1:nodeList[i].y idy2:nodeList[j].y];
				//pMat[i][j] = calcDistance(nodeList[i].x, nodeList[j].x, nodeList[i].y, nodeList[j].y);
			}//end of for
        
		/*********************Omega Part is done, SOR starts here*****************************/
        
		nodeList[t].parent_id = -2;
		nodeList[0].parent_id = -1;
        
		for(i=0; i<size; i++)
		{
			copy[i].id_number = nodeList[i].id_number;
			copy[i].x = nodeList[i].x;
			copy[i].y = nodeList[i].y;
			copy[i].cn_id = nodeList[i].cn_id;
			copy[i].inTree = nodeList[i].inTree;
			parent[i] = copy[i].parent_id = nodeList[i].parent_id;
			power[i] = copy[i].power = nodeList[i].power;
			mparent[i] = copy[i].parent_id;// -2;
		}
        
		mparent[0] = -1;
		for (j = 0; j < size; j++) {
			W[j] = 0;
		}
		W[0] = 1;
		WCnt = 1;
		minPower =power[0];
		for (k = 0; k < size; k++) {
			for (j = 0; j < size; j++) {
				if (W[j] == 0 && mparent[j] > -1 && W[mparent[j]]) {
					WCnt++;
					W[j] = 1;
					minPower += power[j];
				}
			}
		}
        
        
		while(WCnt < size)
		{  
			minPower = DBL_MAX;
			for(i =0; i< size; i++)
			{
                
				if(W[i] != 1)
				{
					continue;
				}
                
				for(j =0; j< size; j++)
				{
					if(i == j) 
					{
						continue;
					}
                    
					for(k=0; k< size; k++)
					{
						if(k != i && o_pMat[i][k] <= o_pMat[i][j] && W[k] != 1)
							break;
					}
                    
					if (k == size) {continue;}
					for (k = 0; k < size; k++) {
						parent[k] = copy[k].parent_id;
					}
                    
					for(k =0; k< size; k++)
					{
						if (k == i) continue;
                        
						if(o_pMat[i][k] <= o_pMat[i][j])
						{ 
							int temp = i;//k;
                            
							while( temp >= 0 && temp != k)
							{
								temp = parent[temp];// - 1;
							}
                            
							if (temp != k)
							{
								parent[k] = i;
							}
						}
                        
					}
                    
					int kk;
					for(kk =0; kk< size; kk++)
					{power[kk] = 0;}
                    
					for(k=0; k< size; k++)
					{ 
                        
						if(parent[k] >= 0 && power[parent[k]] < o_pMat[parent[k]][k] && parent[k] != k)
						{
							power[parent[k]] = o_pMat[parent[k]][k];
						}
					}
                    
					powerSum = 0;
					for(k =0; k< size; k++)
						powerSum += power[k];
                    
					if(powerSum < minPower)
					{
						for(k =0; k< size; k++)
						{
							mparent[k] = parent[k];
							mpower[k] = power[k];
						}
						minPower = powerSum;
					}
				}
			}
			if (minPower < DBL_MAX) {//why?
				for(k =0; k< size; k++)
				{
					copy[k].parent_id = mparent[k];
					copy[k].power = mpower[k];
					if(mparent[k] != -2)
						copy[k].inTree = true;
				}
                
				if(WCnt < size)
				{
					for (j = 0; j < size; j++) {
						W[j] = 0;
					}
					W[0] = 1;
					WCnt = 1;
					for (k = 0; k < size; k++) {
						for (j = 0; j < size; j++) {
							if (W[j] == 0 && mparent[j] >= -1 && W[mparent[j]]) {
								WCnt++;
								W[j] = 1;
							}
						}
					}
				}
			} else {
				WCnt = size;
			}
            
            //////			double tpsum = 0;
            //			printf("Intermediate Tree in SOR phase WCnt: %d:\n", WCnt);
            //			for (k = 0; k < size; k++) {
            //				printf("%d\t%f\t%d\n", k, mpower[k], mparent[k]);
            //				tpsum += mpower[k];
            //			}
            //			printf("Power Sum = %f.\n\n", tpsum);
            
		}
        
		/*********************Init for next loop*********************************************/
        
        
		if(WCnt == size)
			if(minPower < bestSum)
			{
				//printf("\n\n\nBest updated!!\n");
				for(k =0; k< size; k++)
				{
					//////				bestParent[k] = copy[k].parent_id;
					//////				bestParent[k] = copy[k].parent_id;
					bestParent[k] = copy[k].parent_id;//mparent[k];
					bestPower[k] = copy[k].power;//mpower[k];
					//printf("ID:%d  ParentID:%d  Power:%f\n", copy[k].id + 1, copy[k].parent_id + 1, copy[k].power);
					//printf("ID:%d  MParentID:%d  MPower:%f\n", copy[k].id + 1, mparent[k], mpower[k]);
                    
				}
				bestSum = minPower;
				/*if(bestSum < 1)
				{
					printf("\nbest power is somewhat weird %f\n", bestSum);
					exit(1);
				}*/
			}//end of if
        
		for(i=0; i< size; i++)
		{
			brdcstTree[i] =0;
			nodeList[i].inTree = false;
			nodeList[i].parent_id = -2;
			//nodeList[i].power = 0.0;		
		}//end of for
		totalEn =0;
		//		printf("\n\n\nEND OF %d LOOP\n\n\n", t);
        
	}//end of for loop t
    
	//printf("\n\nBest Power is %f\n", bestSum);
    
	for(k =0; k< size; k++)
	{
		//printf("\nid:%d and its parent:%d then power:%f", nodeList[k].id_number + 1, bestParent[k] + 1, bestPower[k]);
        nodeList[k].parent_id = bestParent[k];
        nodeList[k].power = bestPower[k];
	}//end of for
    
    

    
    return bestSum;
   

}//end of constructDCT

-(void)copyDctAnswer:(struct nodeType*)dctArray
{
    for(int i=0; i< NoNodes; i++)
    {
        dctArray[i].power = nodeList[i].power;
        dctArray[i].parent_id = nodeList[i].parent_id;
        dctArray[i].id_number = nodeList[i].id_number;
    
    }//end of for
}

@end
