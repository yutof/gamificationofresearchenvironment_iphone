//
//  ReachabilityUpdator.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/20/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReachabilityUpdator class. 
 * Verify reachability of a particular server
 * with error handling so that we can detect
 * an error earlier if there's any before 
 * sending data to the server
 *******************************************/

#import "Constants.h"
#import "Reachability.h"

@interface ReachabilityUpdator : NSObject {
    
}

+(void)updateServerAvailability;

@end

