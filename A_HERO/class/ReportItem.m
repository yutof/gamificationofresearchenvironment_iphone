//
//  ReportItem.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ReportItem.h"

@implementation ReportItem

@synthesize name, reportDate, strategy;
@synthesize reportID;
@synthesize stageNum;
@synthesize treeID;

@end
