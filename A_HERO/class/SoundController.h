//
//  SoundController.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/9/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * SoundController class. 
 * Manages all sound effect in the application
 * Uses both AVFoundation and OpenAL
 *******************************************/

#import <AVFoundation/AVAudioPlayer.h>
#import <OpenAL/al.h>
#import <OpenAL/alc.h>

enum SoundList{ CoinSound =0
,PrevStepSound
,NextStepSound
,ChangeRelyingNodeSound
,WinningSound
,LosingSound
,AchievementSound
,RefreshSound
,KilledSound
,GoHeavenSound
,ReviveSound
,ApplauseSound
};

@class oalPlayback;

@interface SoundController : NSObject<AVAudioPlayerDelegate> {
    AVAudioPlayer *cngRlyngNode, *prev, *next;
    AVAudioPlayer *win_1, *win_2, *lose_1, *lose_2, *refresh;
    AVAudioPlayer *killed, *goHeaven, *revive, *applause;
    
    CFURLRef		soundFileURLRef;
    UInt32		_coinSound;
    NSMutableArray *coinArray;
    NSString *coinPath;
    oalPlayback *coin;
    
    ALuint					source;
	ALuint					buffer;
	ALCcontext*				context;
	ALCdevice*				device;
	UInt32					iPodIsPlaying;
    
}

-(void)playSoundEffect:(enum SoundList)tag;
-(void)stopSoundEffect:(enum SoundList)tag;
@property (readwrite)CFURLRef soundFileURLRef;
@property (retain, nonatomic) AVAudioPlayer *win_1, *win_2, *lose_1, *lose_2, *refresh, *revive,
                                            *cngRlyngNode, *prev, *next, *killed, *goHeaven, *applause;
@property (retain, nonatomic)NSMutableArray *coinArray;
@property(retain, nonatomic) NSString *coinPath;
@property (retain, nonatomic)oalPlayback *coin;
@end
