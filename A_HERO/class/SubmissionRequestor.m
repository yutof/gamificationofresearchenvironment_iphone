//
//  SubmissionRequestor.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * SubmissionRequestor class. 
 * Takes care of almost all web requests using
 * ASIFormDataRequest class. 
 * This class also consists a procedure that 
 * stores necessary data into local database
 * so that the application will not lose the
 * data that was provided by the user
 *******************************************/


#import "SubmissionRequestor.h"

@implementation SubmissionRequestor

@synthesize GV_reportedTreeID, GV_stepNum ,GV_type,GV_stageNum;
@synthesize GV_name, GV_strtgy_txt;
@synthesize GV_winningPercentage;
@synthesize error_occured, delegate;
@synthesize pendingTag;

#pragma mark submission methods
-(void)askConfirmation
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Successful submission results in displaying your strategy on the website. Can we proceed to submit the strategy?"
        delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    [alert show];
    alert.tag = Submission_Confirmed;
    [alert release];
}

-(void)submitStrategy:(int)reportedTreeID strategy:(NSString*)strtgy_txt score:(double)winningPercentage stepNumber:(int)stepNum  stg:(int)stageNumber rptType:(enum ReportType)type
{
    GV_reportedTreeID = reportedTreeID;
    GV_strtgy_txt = strtgy_txt;
    GV_winningPercentage = winningPercentage;
    GV_stepNum = stepNum;
    GV_type = type;
    GV_stageNum = stageNumber;
    error_occured = NO;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        
    if(serverAvailable)
    {
    dispatch_async(queue, ^(void) {
            NSString *databasePath = @"";    

            NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDir = [documentPaths objectAtIndex:0];
            databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];        
            [ReachabilityUpdator updateServerAvailability];
            
        [self submitRecord_Request:reportedTreeID stp:stepNum stg:stageNumber 
                    strtgy:strtgy_txt scr:winningPercentage file:databasePath type:type];
                        
        });
        
    }
    else
    {
        NSString *databasePath = @"";    
        
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];        

        self.pendingTag = LogOnly;
        [self storePendingRest:reportedTreeID stp:stepNum strtgy:strtgy_txt scr:winningPercentage file:databasePath stage:stageNum];
        
    }//end of if serverAvailable == NO
        
    [[self delegate] requestCompleted:self];
    
    
}//end of submitStrategy


#pragma mark ASIHTTPRequest Delegae

- (void)requestFinished:(ASIHTTPRequest *)request
{    
        NSLog(@"finished :response text is %@", [request responseString]);            
    dispatch_queue_t queue_2 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH , 0ul);
    
    dispatch_async(queue_2, ^(void) {
        
    NSString *response =  [request responseString];
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL succeeded = ([response isEqualToString:[NSString stringWithFormat:@"everything succeeded"]]);
        
    if(succeeded)
    {
        error_occured = NO;
            NSString *updateStage_statement;
            SQLManager *sq = [[[SQLManager alloc] init] autorelease];    
            ASIFormDataRequest *hash = (ASIFormDataRequest*)request;
            updateStage_statement = [NSString stringWithFormat:@"UPDATE STAGE SET secretHash='%@' WHERE Stage_TreeID=%d;",hash.yfSentHash, hash.reportedID];
            [sq query:updateStage_statement];
            
            if(request.tag == RegularSubmission || request.tag == RegularStepSubmission)
            {
                [self performSelectorOnMainThread:@selector(regularSubmissionSucceeded) withObject:nil waitUntilDone:NO];
            }
            else
            {
                NSString *delete_statement;
                delete_statement = [NSString stringWithFormat:@"DELETE FROM PendingReport WHERE PR_Tree_ID=%d;DELETE FROM UserLog WHERE UL_Tree_ID=%d;DELETE FROM Tree WHERE Tree_ID=%d;DELETE FROM NodeList WHERE NL_Tree_ID=%d;DELETE FROM DCTNodeList WHERE DCTNL_Tree_ID=%d;DELETE FROM UserNodeList Where UL_Tree_ID=%d;", request.tag, request.tag, request.tag, request.tag, request.tag, request.tag];
                
                                
                [sq query:delete_statement];
                if([sq getRowCount:@"PendingReport"] > 0)
                    [self submitPendingRecord];
                else
                    pending = NO;
                
                //[sq release];
                
                
            }
        } else
        {
            error_occured = YES;
            if(request.tag == RegularSubmission)
            {
                [self performSelectorOnMainThread:@selector(regularSubmissionFailed) withObject:nil waitUntilDone:NO];
                
                NSString *databasePath = @"";    
                NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDir = [documentPaths objectAtIndex:0];
                databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];        
                [self storePendingRest:GV_reportedTreeID stp:GV_stepNum strtgy:GV_strtgy_txt scr:GV_winningPercentage file:databasePath stage:GV_stageNum];
                                
            }//if RegularSubmission            
        }

    });//end of block
 
}




-(void)regularSubmissionSucceeded
{    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:
    @"Thank you very much for submitting your strategy!! We truly appreciate your contribution!! Your record has been uploaded sucessfully."
        delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    alert.tag = Submission_Completed;
    [alert show];
    [alert release];
}

-(void)regularSubmissionFailed
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Thank you very much for submitting your strategy!! We truly appreciate your contribution!! Unfortunately, submission has failed. The record will be submitted once the server become available." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    alert.tag = Submission_Failed;
    [alert show];
    [alert release];
}




#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
}

#pragma mark Pending Report methods
-(void)storePendingRest:(int)pr_treeID stp:(int)stepNumber strtgy:(NSString*)pr_strategy scr:(double)pr_score file:(NSString*)filePath stage:(int )stageNumber
{
    
    pending = true;
    SQLManager *sq = [[[SQLManager alloc] init] autorelease];
    NSString *statement = @"INSERT INTO PendingReport(PR_Tree_ID, PR_Name, PR_Strategy, PR_FilePath, PR_Score, reportType, PR_StageNum, PR_StepNum)";
    statement = [NSString stringWithFormat:@"%@ Values(%d, '%@', '%@', '%@', %f, %d, %d, %d);",
                 statement,pr_treeID, registeredUserName, pr_strategy,filePath,pr_score, self.pendingTag, stageNumber, stepNumber];
    
    [sq query:statement];
    
}

-(void)submitPendingRecord
{
    SQLManager *sq = [[[SQLManager alloc] init] autorelease];    
    NSMutableArray *arr = [sq getPendingReportList];
    PendingReportItem *repo;
    NSString* deleteStatement = @"";
    for(int i =0; i< [arr count]; i++)
    {
        repo = [arr objectAtIndex:i];
        NSLog(@"treeid is %d", repo.treeID);
        if([self latestSubmitPendingRecord:repo])
        {
            NSLog(@"delete record!!");
            deleteStatement = [NSString stringWithFormat:@"DELETE FROM PendingReport WHERE PR_TREE_ID=%d;", repo.treeID];
            NSLog(@"statement:%@",deleteStatement);
            [sq query:deleteStatement];
        }
        else
        {
            NSLog(@"failed??");
        }
    }
    
    [arr release];
    arr = nil;
    arr = [sq getPendingAnonymousReportList];
    AnonymousReportItem *item;
    for(int i =0; i<[arr count]; i++)
    {
        item = [arr objectAtIndex:i];
        [self submitAnonymousReport:item];
    }
    
    deleteStatement = @"DELETE FROM PendingAnonymousReport;";
    [sq query:deleteStatement];
    [arr release];

}//end submitPendingRecord

-(BOOL)latestSubmitPendingRecord:(PendingReportItem*)pendingItem
{

    NSLog(@"report type is %d", pendingItem.reportType);
    
    sendingReport = YES;
    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/securityProcedure/veryStrong_security/hiddenFolder"];        
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request startSynchronous];
    
    NSString *seed;
    seed = [request responseString];
    seed = [seed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    seed = [NSString stringWithFormat:@"%@%@",seed,commonkey];
    seed = [EncryptorClass sha1:seed];
    url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/submitPendingReport_1/veryStrong_security/hiddenFolder"];                        
    request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];//[ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request setPostValue:[NSNumber numberWithInt:pendingItem.stageNum] forKey:@"stageNumber"];
    [request setPostValue:pendingItem.name forKey:@"uname"];
    [request setPostValue:pendingItem.strategy forKey:@"strategy"];
    [request setPostValue:[NSNumber numberWithDouble:pendingItem.score] forKey:@"score"];
    [request setPostValue:[NSNumber numberWithInt:pendingItem.treeID] forKey:@"treeID"];                
    [request setPostValue:[NSNumber numberWithInt:pendingItem.stepNum] forKey:@"step"];
    NSLog(@"step number %d", pendingItem.stepNum);
    [request setPostValue:[NSNumber numberWithInt:pendingItem.reportType] forKey:@"reportType"];
    request.yfSentHash = [EncryptorClass makeShortHash];
    request.reportedID = pendingItem.treeID;
    [request setPostValue:request.yfSentHash forKey:@"hashseed2"];
    [request setPostValue:seed forKey:@"hash"];
    NSString *databasePath = @"";
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];
    [request setFile:databasePath forKey:@"uploadedfile"];
    [request startSynchronous];
    NSLog(@"response is %@", request.responseString);
    
    
    NSString *returnedID = [request responseString];
    NSLog(@"returned ID? %@",returnedID);
    if(returnedID.intValue != 0)
    {
        url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/fileInsertion/veryStrong_security/hiddenFolder"];                            
        request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];//[ASIFormDataRequest requestWithURL:url];
        [request setUsername:server_username];
        [request setPassword:server_password];
        returnedID = [returnedID stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [request setPostValue:returnedID forKey:@"reportID"];
        [request setPostValue:[NSNumber numberWithInt:pendingItem.treeID] forKey:@"treeID"];                
        [request startSynchronous];
        NSLog(@"wowow response text %@", [request responseString]);        
        
        NSString *response =  [request responseString];
        response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        return  ([response isEqualToString:[NSString stringWithFormat:@"everything succeeded"]]);
                
    }
    
    return NO;
}


-(void)submitRecord_Request:(int)rpt_treeID stp:(int)stepNumber stg:(int)stageNumber strtgy:(NSString*)rpt_strategy scr:(double)rpt_score file:(NSString*)filePath type:(enum ReportType)tag
{
    
    NSLog(@"reached here. ");
    BOOL makeItPending = NO;
    sendingReport = YES;
    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/securityProcedure/veryStrong_security/hiddenFolder"];        
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request startSynchronous];
    
    NSString *seed;
    seed = [request responseString];
    seed = [seed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    seed = [NSString stringWithFormat:@"%@%@",seed,commonkey];
    seed = [EncryptorClass sha1:seed];
    url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/stp_submitReport_noStrategy_1/veryStrong_security/hiddenFolder"];                        
    request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];//[ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request setPostValue:[NSNumber numberWithInt:stageNumber] forKey:@"stageNumber"];
    [request setPostValue:registeredUserName forKey:@"uname"];
    [request setPostValue:[NSNumber numberWithDouble:rpt_score] forKey:@"score"];
    [request setPostValue:[NSNumber numberWithInt:rpt_treeID] forKey:@"treeID"];                
    [request setPostValue:[NSNumber numberWithInt:stepNumber] forKey:@"step"];
    request.yfSentHash = [EncryptorClass makeShortHash];
    request.reportedID = rpt_treeID;
    [request setPostValue:request.yfSentHash forKey:@"hashseed2"];
    [request setPostValue:seed forKey:@"hash"];
    [request setFile:filePath forKey:@"uploadedfile"];
    [request startSynchronous];
    NSString *returnedID = [request responseString];
    if(returnedID.intValue != 0)
    {
        url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/fileInsertion/veryStrong_security/hiddenFolder"];                            
        request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];//[ASIFormDataRequest requestWithURL:url];
        [request setUsername:server_username];
        [request setPassword:server_password];
        returnedID = [returnedID stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [request setPostValue:returnedID forKey:@"reportID"];
        [request setPostValue:[NSNumber numberWithInt:rpt_treeID] forKey:@"treeID"];                
        [request startSynchronous];
        
        NSString *response =  [request responseString];
        response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        BOOL succeeded = ([response isEqualToString:[NSString stringWithFormat:@"everything succeeded"]]);
        
        if(!succeeded)
            makeItPending = YES;
        
    }
    else
    {
        makeItPending = YES;
    }    
    
    if(makeItPending)
    {
        pendingTag = LogOnly;
        [self storePendingRest:rpt_treeID stp:stepNumber strtgy:rpt_strategy scr:rpt_score file:filePath stage:stageNumber];
    }
    
    
}

-(void)reuploadFile:(int)treeIDNumber
{
    if(!serverAvailable)
    {
        [self updatePendingRequest:@"" str:@"" rptType:StepwiseStrategy];
        return;
    }
    BOOL succeeded= NO;
    self.pendingTag = StepwiseStrategy;
    
    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/getMyReportID/veryStrong_security/hiddenFolder"];        
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request startSynchronous];
    NSString *reportID;
    reportID = [request responseString];
    
    url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/reuploadFile/veryStrong_security/hiddenFolder"];                        
    request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];//[ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];

    NSString *databasePath = @"";    
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];        
 
    [request setFile:databasePath forKey:@"uploadedfile"];
    [request setPostValue:reportID forKey:@"reportID"];
    [request setPostValue:[NSNumber numberWithInt:treeIDNumber] forKey:@"treeID"];    
    [request startSynchronous];

    NSString *response =  [request responseString];
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    succeeded = ([response isEqualToString:[NSString stringWithFormat:@"everything succeeded"]]);
        
    if(!succeeded)
        [self updatePendingRequest:registeredUserName str:@"" rptType:StepwiseStrategy];
    
[[self delegate] requestCompleted:self];
    
}


-(void)updateNameAndStrategy:(NSString*)str
{

    if(!serverAvailable)
    {
            [self updatePendingRequest:registeredUserName str:str rptType:GeneralStrategy];
        return;
    }
    
    self.pendingTag = GeneralStrategy;

    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/getMyReportID/veryStrong_security/hiddenFolder"];        
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request startSynchronous];
    NSString *reportID;
    reportID = [request responseString];
    
    url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/updateNameAndStrategy/veryStrong_security/hiddenFolder"];                        
    request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];//[ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request setPostValue:registeredUserName forKey:@"uname"];
    [request setPostValue:str forKey:@"strategy"];
    [request setPostValue:reportID forKey:@"reportID"];
    [request startSynchronous];
  
    NSString *response =  [request responseString];
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL succeeded = ([response isEqualToString:[NSString stringWithFormat:@"everything succeeded"]]);
    
    if(!succeeded)
    [self updatePendingRequest:registeredUserName str:str rptType:GeneralStrategy];
    
}

-(void)getReportList//:(int)lastReportID
{
    [ReachabilityUpdator updateServerAvailability];
    if(!serverAvailable)
        return;
    
    int lastReportID;
    SQLManager *sq = [[[SQLManager alloc] init] autorelease];
    lastReportID = [sq getInteger:@"select ID from ReportList ORDER BY id DESC LIMIT 1;"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://yourdomain.com/veryStrong_security/hiddenFolder/reportListing/veryStrong_security/hiddenFolder?lastReceivedID=%d", lastReportID]];
	ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    request.tag = ReportListing;
    request.delegate = self;
	[request startSynchronous];

    if([request responseString].length > 0)
    {
        [sq query:[request responseString]];
    }
}

-(void)updatePendingRequest:(NSString*)Name str:(NSString*)strategy rptType:(enum StrReportType)type
{
    NSString *statement = @"SELECT PR_Tree_ID INTEGER from PendingReport ORDER BY PR_Tree_ID DESC LIMIT 1;";
    SQLManager *sq = [[[SQLManager alloc] init] autorelease];
    int tr_id = [sq getInteger:statement];
    if(tr_id >= 0)
    {
        statement = [NSString stringWithFormat:@"UPDATE PendingReport SET PR_Strategy='%@', PR_Name='%@' , ReportType=%d WHERE PR_TREE_ID=", strategy, Name, type, tr_id];
    }
    else
    {
        NSString *databasePath = @"";    
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];  
        self.pendingTag = type;
        [self storePendingRest:treeID stp:step strtgy:strategy scr:winningPercentage file:databasePath stage:stageNum];
    }
}


-(void)getRegisteredID
{

    [ReachabilityUpdator updateServerAvailability];
    if(!serverAvailable)
        return;
    
    if([registeredUserID length] > 0)
        return;
    
    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/anonymousRegistration/veryStrong_security/hiddenFolder"];
	ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
	[request startSynchronous];
    if([request responseString].length > 0)
    {
        registeredUserID = [request responseString];
        [registeredUserID retain];
    }
}

-(void)submitAnonymousReport:(AnonymousReportItem*)item
{    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH , 0ul);
    
    dispatch_async(queue, ^(void) {
    
    [ReachabilityUpdator updateServerAvailability];
    if(!serverAvailable)
    {
        [self storeAnonymousReport:item];
        return;
    }
    
    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/anonymousReport/veryStrong_security/hiddenFolder"];
	ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
        
    [request setPostValue:[NSNumber numberWithInt:item.stepNum] forKey:@"stepNum"];
    [request setPostValue:[NSNumber numberWithInt:item.stageNum ] forKey:@"stageNum"];
    [request setPostValue:[NSNumber numberWithInt:item.indicatorFlag ] forKey:@"flag"];
    [request setPostValue:registeredUserID forKey:@"anID"];
	[request startSynchronous];

    NSString *response =  [request responseString];

    NSLog(@"response is %@", response);
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL succeeded = ([response isEqualToString:[NSString stringWithFormat:@"everything succeeded"]]);

        
    if(!succeeded)
            [self storeAnonymousReport:item];
        
    });
}

-(void)storeAnonymousReport:(AnonymousReportItem*)item
{
    SQLManager *sq = [[[SQLManager alloc] init] autorelease];
    NSString* statement = [NSString stringWithFormat:@"INSERT INTO PendingAnonymousReport(StepNum, StageNum, IndicatorFlag) VALUES(%d, %d, %d);", item.stepNum, item.stageNum, item.indicatorFlag];
    [sq query:statement];
}

@end
