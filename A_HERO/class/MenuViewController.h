//
//  MenuViewController.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/4/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * MenuViewController class. 
 * Provides GUI for the main menu and control
 * the hierarchy of the view throughout the 
 * application.
 *******************************************/

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "GameConfigurationController.h"
#import "GameViewController.h"
#import "AboutWebViewControllerClass.h"
#import "ReportTypeView.h"
#import "InitialRegistrationViewController.h"
@class GameConfigurationController;
@class GameViewController;

enum RowNames {
    Row_Game=0
    ,Row_Cfg
    ,Row_Report
    ,Row_About
    ,Row_Score
    ,Row_Achievement
};

BOOL isGameCenterAPIAvailable();

@interface MenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate ,
                                GKLeaderboardViewControllerDelegate, GKAchievementViewControllerDelegate, 
                                GameConfigurationDelegate, UIAlertViewDelegate, GameViewDelegate, InitialRegistrationDelegate> 
{
	UITableView *menuTableView;  
    GameConfigurationController * cfgViewController;
    GameViewController *gameViewController;
    AboutWebViewControllerClass *about;
    ReportTypeView *reportList;
    GKLeaderboardViewController *leaderboardController;   
    GKAchievementViewController *achievementsController;
    GKScore *score;
    BOOL gameCenter_Shown;
    UIImageView *playGameLabelImage, *gameConfigLabelImage, *aboutLabelImage; 
    InitialRegistrationViewController *initialRegView;

}

@property(retain, nonatomic) UITableView *menuTableView;
@property(retain, nonatomic) GameConfigurationController *cfgViewController;
@property(retain, nonatomic) GKLeaderboardViewController *leaderboardController;
@property(retain, nonatomic) GKAchievementViewController *achievementsController;
@property(retain, nonatomic) GameViewController *gameViewController;
@property(retain, nonatomic) AboutWebViewControllerClass *about;
@property(retain, nonatomic) GKScore *score ;
@property(retain, nonatomic) UIImageView *playGameLabelImage, *gameConfigLabelImage, *aboutLabelImage; 
@property(retain, nonatomic) ReportTypeView *reportList;
@property(retain, nonatomic) InitialRegistrationViewController *initialRegView;

-(void)setupMenu;
-(void)callGaneViewController;
-(void)authenticateUser;
-(void)update_GameCenterRows;
-(void)update_StrategyTreeID;
-(void)showReportDoneAlert;
-(void)submitPendingRecords;
-(void)renewTheGame;
-(BOOL) isGameCenterAPIAvailable;

@end
