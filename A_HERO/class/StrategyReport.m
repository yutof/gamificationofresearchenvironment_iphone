//
//  StrategyReport.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/19/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * StrategyReport class. 
 * Manages strategy report web request 
 * as well as providing GUI for general
 * report. 
 *******************************************/

#import "StrategyReport.h"
#import "ASIFormDataRequest.h"
#import "Constants.h"
#import "EncryptorClass.h"
#import "SQLManager.h"
#import "GameViewController.h"
#import "ReachabilityUpdator.h"

@implementation StrategyReport
@synthesize strategy, submit;
@synthesize reportedTreeID;
@synthesize delegate;
@synthesize reqObj;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setUpStrategyReporter];
    }
    return self;
}

-(id)init
{
    return self;
}

- (void)dealloc
{
    if(strategy)
        [strategy release];
    
    if(submit)
        [submit release];
        

    if(reqObj)
        [reqObj release];
    
        [super dealloc];
}

-(void)viewDidUnload
{
    if(self.strategy)
        [self.strategy release];
    self.strategy = nil;
    
    if(self.submit)
        [self.submit release];
    self.submit = nil;
    
    [super viewDidUnload];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle

-(void)setUpStrategyReporter
{
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.tag =Tag_StrategyReportView;
    self.reportedTreeID = treeID;
    reportStep = actualStep;
    sending = false;    
    
    CGRect textframe = CGRectMake(0, 8.0, 300, 170);
    self.strategy = [[UITextView alloc] initWithFrame:textframe];
    self.strategy.scrollEnabled = YES;
    self.strategy.editable = YES;
    self.strategy.textColor = [UIColor blackColor];
    self.strategy.font = [UIFont systemFontOfSize:17.0];
    self.strategy.backgroundColor = [UIColor whiteColor];
    self.strategy.text =@"Please describe your strategy";
    self.strategy.layer.borderColor = [[UIColor grayColor] CGColor];
    self.strategy.layer.borderWidth = 1;    
    self.strategy.autocorrectionType = UITextAutocorrectionTypeNo;
    self.strategy.keyboardType = UIKeyboardTypeDefault;
    self.strategy.returnKeyType = UIReturnKeyDone;
    self.strategy.delegate = self;
    [self.strategy setAccessibilityLabel:NSLocalizedString(@"strategy", @"")];
    [self.view addSubview:self.strategy];

    self.strategy.center = CGPointMake(160, 100);
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(182.0, 5.0, 120.0, 30.0)];
    btn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btn setTitle:@"Submit" forState:UIControlStateNormal];
    
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    btn.backgroundColor = [UIColor clearColor];
    UIImage *newImage = [[UIImage imageNamed:@"whiteButton.png"] stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	[btn setBackgroundImage:newImage forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(askConfirmation) forControlEvents:UIControlEventTouchDown];
    self.submit = btn;
    [btn release];
    [self.view addSubview:self.submit];
    self.submit.center = CGPointMake(160, 330);
    
}//end of setUpStrategyReporter


-(void)initializeForm
{
    self.strategy.text =@"Please describe your strategy";
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark TextView delegates
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
	[textView resignFirstResponder];
	return YES;
}

- (void)saveAction:(id)sender
{
	[self.strategy resignFirstResponder];
	self.navigationItem.rightBarButtonItem = nil;	
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.strategy.alpha = 1.0;
    if([textView.text isEqualToString:[NSString stringWithFormat:@"Please describe your strategy"]])
        textView.text = @"";
    
	UIBarButtonItem* saveItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                        target:self action:@selector(saveAction:)];
	self.navigationItem.rightBarButtonItem = saveItem;
	[saveItem release];
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}


#pragma mark Utility methods
-(void)updateReportedTreeID
{
    if(!sending)
        reportedTreeID = treeID;
    
    reportStageID = stageNum;
    reportStep = actualStep;
}

#pragma mark submission methods
-(void)askConfirmation
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Successful submission results in displaying your strategy on the website. Can we proceed to submit the strategy?"
                                                   delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    [alert show];
    alert.tag = Submission_Confirmed;
    [alert release];


}

-(void)submitStrategy
{
    sending = true;
    if([self.strategy.text isEqualToString:[NSString stringWithFormat:@"Please describe your strategy"]] || [self.strategy.text length] == 0 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please describe your strategy."
                                                       delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        alert.tag = Submission_Form_Incomplete;
        [alert release];
    }
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Thank you very much for submitting your strategy!! We truly appreciate your contribution!!." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag = Submission_Form_Completed;
        [alert show];
        [alert release];

        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                
        dispatch_async(queue, ^(void) {
            NSString *databasePath = @"";    
            NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDir = [documentPaths objectAtIndex:0];
            databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];        

            [ReachabilityUpdator updateServerAvailability];
            
            if(!self.reqObj)
                self.reqObj = [[SubmissionRequestor alloc] init];
            
            if(serverAvailable)
            {
                self.reqObj.delegate = self;
                [self.reqObj updateNameAndStrategy:self.strategy.text];
            }//end of if serverAvailable
            else
            {

                self.reqObj.pendingTag = GeneralStrategy;
                [self.reqObj storePendingRest:reportedTreeID stp:step strtgy:self.strategy.text scr:winningPercentage file:databasePath stage:reportStageID];
                [self performSelectorOnMainThread:@selector(initializeForm) withObject:NULL waitUntilDone:NO];
            }//end of if serverAvailable == NO
            
            
            });
            

    }//end of else if

    
}//end of submitStrategy

#pragma mark ASIHTTPRequest Delegae

- (void)requestFinished:(ASIHTTPRequest *)request
{    
    dispatch_queue_t queue_2 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH , 0ul);
    
    dispatch_async(queue_2, ^(void) {

    NSString *response =  [request responseString];
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL succeeded = ([response isEqualToString:[NSString stringWithFormat:@"everything succeeded"]]);
    
    if(succeeded)
    {
        NSString *updateStage_statement;
        SQLManager *sq = [[[SQLManager alloc] init] autorelease];    
        ASIFormDataRequest *hash = (ASIFormDataRequest*)request;
        updateStage_statement = [NSString stringWithFormat:@"UPDATE STAGE SET secretHash='%@' WHERE Stage_TreeID=%d;",hash.yfSentHash, hash.reportedID];
        [sq query:updateStage_statement];
        
            if(request.tag == RegularSubmission)
            {
            [self performSelectorOnMainThread:@selector(regularSubmissionSucceeded) withObject:nil waitUntilDone:NO];
            }
            else
            {
                NSString *delete_statement;
                
                delete_statement = [NSString stringWithFormat:@"DELETE FROM PendingReport WHERE PR_Tree_ID=%d;DELETE FROM UserLog WHERE UL_Tree_ID=%d;DELETE FROM Tree WHERE Tree_ID=%d;DELETE FROM NodeList WHERE NL_Tree_ID=%d;DELETE FROM DCTNodeList WHERE DCTNL_Tree_ID=%d;DELETE FROM UserNodeList Where UL_Tree_ID=%d;", request.tag, request.tag, request.tag, request.tag, request.tag, request.tag];               
                
                [sq query:delete_statement];
            if([sq getRowCount:@"PendingReport"] > 0)
                [self.reqObj submitPendingRecord];
            else
                pending = NO;
            
            
            }
        } else
        {
            if(request.tag == RegularSubmission)
            {
                [self performSelectorOnMainThread:@selector(regularSubmissionFailed) withObject:nil waitUntilDone:NO];
            
                NSString *databasePath = @"";    
                NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDir = [documentPaths objectAtIndex:0];
                databasePath = [documentsDir stringByAppendingPathComponent:@"DCT.sql"];        
            
                [self storePendingRest:reportedTreeID strtgy:self.strategy.text scr:winningPercentage file:databasePath];   
            }//if RegularSubmission
        
        }
    [self performSelectorOnMainThread:@selector(initializeForm) withObject:NULL waitUntilDone:NO];
        
    NSLog(@"finished :response text is %@", [request responseString]);
        
    sending = false;
    reportedTreeID = treeID;
        
    });//end of block
    
}

-(void)regularSubmissionSucceeded
{
    [[self delegate] reportSubmittedSucessfully:self];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Thank you very much for submitting your strategy!! We truly appreciate your contribution!! Your record has been uploaded sucessfully."
        delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];

    alert.tag = Submission_Completed;
    [alert show];
    [alert release];

}


-(void)regularSubmissionFailed
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Thank you very much for submitting your strategy!! We truly appreciate your contribution!! Unfortunately, submission has failed. The record will be submitted once the server become available."
        delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];

    alert.tag = Submission_Failed;
    [alert show];
    [alert release];

}



#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == Submission_Form_Completed)
    {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }//end of if Submission_Form_Completed
    else if(actionSheet.tag == Submission_Confirmed)
    {
        if(buttonIndex != 0)
        {
            [self submitStrategy];
        }
    }
}

#pragma mark Pending Report methods
-(void)storePendingRest:(int)pr_treeID strtgy:(NSString*)pr_strategy scr:(double)pr_score file:(NSString*)filePath
{
    pending = true;
    SQLManager *sq = [[[SQLManager alloc] init] autorelease];
    NSString *statement = @"INSERT INTO PendingReport(PR_Tree_ID, PR_Name, PR_Strategy, PR_FilePath, PR_Score)";
    statement = [NSString stringWithFormat:@"%@ Values(%d, '%@', '%@', '%@', %f);",
                 statement,pr_treeID, registeredUserName, pr_strategy,filePath,pr_score];
    
    [sq query:statement];

}


-(void)submitRecord_Request:(int)rpt_treeID nm:(NSString*)rpt_name strtgy:(NSString*)rpt_strategy scr:(double)rpt_score file:(NSString*)filePath type:(int)tag;
{
    
    sendingReport = YES;
    NSURL *url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/securityProcedure.php"];        
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    [request startSynchronous];
    NSString *seed;
    seed = [request responseString];
    seed = [seed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    seed = [NSString stringWithFormat:@"%@%@",seed,commonkey];
    seed = [EncryptorClass sha1:seed];
    url = [NSURL URLWithString:@"http://yourdomain.com/veryStrong_security/hiddenFolder/submitReport.php"];                        
    request = [[[ASIFormDataRequest alloc] initWithURL:url] autorelease];//[ASIFormDataRequest requestWithURL:url];
    [request setUsername:server_username];
    [request setPassword:server_password];
    request.tag = tag;
    request.delegate = self;
    [request setPostValue:[NSNumber numberWithInt:reportStageID] forKey:@"stageNumber"];
    [request setPostValue:rpt_name forKey:@"uname"];
    [request setPostValue:rpt_strategy forKey:@"strategy"];
    [request setPostValue:[NSNumber numberWithDouble:rpt_score] forKey:@"score"];
    [request setPostValue:[NSNumber numberWithInt:rpt_treeID] forKey:@"treeID"];                
    [request setPostValue:[NSNumber numberWithInt:reportStep] forKey:@"step"];
    
    request.yfSentHash = [EncryptorClass makeShortHash];
    request.reportedID = rpt_treeID;
    [request setPostValue:request.yfSentHash forKey:@"hashseed2"];
    [request setPostValue:seed forKey:@"hash"];
    
    [request setFile:filePath forKey:@"uploadedfile"];
    [request startSynchronous];
    
}


#pragma mark Submission Delegate
-(void)requestCompleted:(SubmissionRequestor *)req
{
    [self initializeForm];
}


@end
