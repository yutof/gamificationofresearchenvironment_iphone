//
//  StrategyReport.h
//
//  Created by Yuto Fujikawa on 7/19/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * StrategyReport class. 
 * Manages strategy report web request 
 * as well as providing GUI for general
 * report. 
 *******************************************/

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SubmissionRequestor.h"

@protocol StrategyReportDelegate;
@interface StrategyReport : UIViewController 
    <UITextViewDelegate, UIAlertViewDelegate, UIScrollViewDelegate, UITextFieldDelegate, SubmissionRequestorDelegate> 
{
    UITextView *strategy;
    UIButton *submit;
    SubmissionRequestor *reqObj;
    int reportedTreeID, reportStageID, reportStep;
    bool sending;
    id<StrategyReportDelegate>delegate;
}

@property (nonatomic, assign) id<StrategyReportDelegate>delegate;
@property(retain, nonatomic)SubmissionRequestor *reqObj;
@property(retain, nonatomic)UITextView *strategy;
@property(retain, nonatomic)UIButton *submit;
@property int reportedTreeID;

-(void)setUpStrategyReporter;
-(void)initializeForm;
-(void)submitStrategy;
- (void)saveAction:(id)sender;
-(void)updateReportedTreeID;
-(void)storePendingRest:(int)pr_treeID strtgy:(NSString*)pr_strategy scr:(double)pr_score file:(NSString*)filePath;

-(void)submitRecord_Request:(int)pr_treeID nm:(NSString*)pr_name strtgy:(NSString*)pr_strategy scr:(double)pr_score file:(NSString*)filePath type:(int)tag;
-(void)regularSubmissionSucceeded;
-(void)regularSubmissionFailed;
-(void)askConfirmation;

@end


@protocol StrategyReportDelegate <NSObject>
@optional
-(void)reportSubmittedSucessfully:(StrategyReport*)orderInstance;
@end



