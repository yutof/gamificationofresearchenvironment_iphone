//
//  TransmissionPower.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/14/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "TransmissionPower.h"


@implementation TransmissionPower

-(id)init
{
    UIImage *img =[UIImage imageNamed:@"light_beam_500.png"];
    CGRect frame = CGRectMake(0, 0, img.size.width, img.size.height);
    
    if ([self initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
		power = img;
    }
    return self;
}

-(void)setUpPower{

}

- (void)drawRect:(CGRect)rect {	
	[power drawAtPoint:(CGPointMake(0.0, 0.0))];
}

- (void)dealloc
{
    power= nil;
    [super dealloc];
}

#pragma mark - View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
