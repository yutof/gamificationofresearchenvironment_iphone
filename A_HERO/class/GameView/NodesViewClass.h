//
//  NodesViewClass.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/13/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GameViewController.h"


@interface NodesViewClass : UIView {
 UIImageView *nodeImage;   
 UIImageView *light;

}

-(void)updateImage:(int)parent_id;
-(void)updateFrame:(CGRect)frame;

@property(retain, nonatomic)UIImageView *nodeImage, *light;
@end
