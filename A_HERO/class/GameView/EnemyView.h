//
//  EnemyView.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/23/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

enum Positions{
DefaultPosition=0,
ApplausePosition
};

enum AnimationTag{
    FirstPart =0,
    SecondPart
};

@protocol EnemyViewDelegate;

@interface EnemyView : UIView {
    CALayer *ghostBody, *ghostLH, *ghostRH, *app_ghostLH, *app_ghostRH;
    //     NSTimer *_ghostTimer;
    //    int myID;
    BOOL animating;
    BOOL applause_animating;
    id <EnemyViewDelegate> delegate;
//    enum AnimationTag animTag;
    
}

//@property(retain, nonatomic)NSTimer *_ghostTimer;
@property(retain,nonatomic)CALayer *ghostBody, *ghostLH, *ghostRH, *app_ghostLH, *app_ghostRH;
@property BOOL animating, applause_animating;
//@property enum AnimationTag animTag;
@property (nonatomic, assign) id <EnemyViewDelegate> delegate;

//-(void)moveGhost2;


-(void)moveGhost;
-(void)moveGhost_part2;
//-(void)startGhostTimer;
//-(void)stopGhostTimer;
-(void)hideGhost:(BOOL)flag;
//-(void)animationCheck;

-(void)applause;
-(void)applause_part2;
//-(void)applauseAnimationCheck;
//-(void)stopApplauseAnimation;

-(void)setPosition:(enum Positions)tag;

//-(void)updateImage:(int)parent_id;



@end


@protocol EnemyViewDelegate <NSObject>
@optional
- (void)moveDidFinish:(EnemyView *)ghost;
- (void)applauseDidFinish:(EnemyView *)ghost;
@end

