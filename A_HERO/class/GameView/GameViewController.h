//
//  GameViewController.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/10/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * GameViewController class. 
 * Provide GUI for the game
 * manage all movements in the game
 * manage game system (stage and such)
 * calculate transmission power of user
 * and DCT.
 *******************************************/

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import <QuartzCore/QuartzCore.h>

#import "CharacterView.h"
#import "Constants.h"
#import "DCT.h"
#import "EnemyView.h"
#import "HorseViewClass.h"
#import "NodesViewClass.h"
#import "SoundController.h"
#import "SQLManager.h"
#import "StrategyReport.h"
#import "TransmissionPower.h"

#import "StepStrategyReport.h"
#import "InstructionAlertView.h"
#import "ReportItem.h"

#import "SubmissionRequestor.h"
#import "AnonymousReportItem.h"


@protocol GameViewDelegate;

enum GameResult{
    GameWin=0,
    GameDraw,
    GameLose
};

enum AlertTag{
    Alert_UnimportantMessage=0,
    Alert_ApplyChangesConfirmation,
    Alert_FinalTreeConfirmation,
    Alert_GameEndedConfirmation,
    Alert_TreeRefreshConfirmation,
    Alert_WonDCTConfirmation,
    Alert_DoneReporting
    ,Alert_GameResume
    ,Alert_GameEndedConfirmation_Lost
    ,Alert_DblTp_Instrctn
    ,Alert_Mv_Instrctn
    ,Alert_ReportType
};

enum ReportAsked
{
    RA_Cancelled=0,
    RA_Overall,
    RA_Stepwise

};

enum WonDCTConfirmation{
    WD_Quit=0
    ,WD_Renew
    ,WD_Log_Overall
    ,WD_Log_Stepwise
    ,WD_Report
};

enum GameEndedAction{
    GameEnded_Quit=0
    ,GameEnded_NewGame
    ,GameEnded_Retry
    
};

enum GameMode{
    GameMode_RegStage=0,
    GameMode_Random
};

enum GameInstruction{
    Dbl_Tap=0,
    Drag_Character,
    Move_Character
};

enum CK_Button{
    CK_Up=0,
    CK_Right,
    CK_Down,
    CK_Left
};

enum AR_Button{
    AR_Add=0,
    AR_Remove
};

@class SoundController;
@class CharacterView;
@class NodesViewClass;
@class TransmissionPower;
@class SQLManager;
@class StrategyReport;
@class EnemyView;


UIImage *lamp_light, *street_lamp;
UIImage *gb, *glh, *grh;
UIImage *coveredNode, *uncoveredNode;

@interface GameViewController : UIViewController <UIAlertViewDelegate, EnemyViewDelegate, UINavigationControllerDelegate, StrategyReportDelegate, StepStrategyReportDelegate, SubmissionRequestorDelegate>{
    
    StepStrategyReport *stepStrategyReport;
    CGPoint touchLocation;
    CGPoint startPoint;
    
    SoundController *sndControler;
    float prevScale;
    BOOL powerShown, refreshConfirmed;
    
    CharacterView *character;
    TransmissionPower *powerLayer;
    NodesViewClass *nodeImages[MAX_Node_Num];
    EnemyView *ghostImages[MAX_Node_Num];
    CGPoint ghostEndLocation[MAX_Node_Num];
    
    struct nodeType nodeList[MAX_Node_Num], copy[MAX_Node_Num];
    double pMat[MAX_Node_Num][MAX_Node_Num], o_pMat[MAX_Node_Num][MAX_Node_Num];
    //    double brdcstTree[50];
    double totalPower, dctTotalPower, o_dctTotalPower;
    
    int relyingNode, 
    mostAdcancedStep/*Keeps track of the most advanced step*/,    
    prevRelyingNode,/*keeps track of previous relying nodes*/
    prevDestNode;/*keeps track of previous destination nodes*/
    
    NSMutableArray *tPowerArray;
    SQLManager *sql, *report_sql;
    //    Texture2D *_textures[2];
    UIImageView *fuel[40];//[20];
    UILabel *powerPercentage, *stageLabel;
    StrategyReport *report;
    NSTimer *_gameTimer;
    
    UIImageView *angelView;
    BOOL drawAnimating, wonAnimating;
    BOOL gameEnded;
    UIBezierPath *winPath;
    int win_ghostIndex;
    
    UIButton *refresh, *rightArrow, *leftArrow;
    //    HorseViewClass *uma;
    id <GameViewDelegate> delegate;

    BOOL makingStepwiseReport, initializingTree, stepwiseReportShown;
//    BOOL showingReportDetail;

    //Cross Key Buttons
    UIButton *ck_UpButton, *ck_RightButton, *ck_DownButton, *ck_leftButton;
    UIButton *addCircleButton, *removeCircleButton;
    UIButton *showStrategyButton;
    UITextView *reportedStrategy;
    ReportItem *currentReport;
    SubmissionRequestor *reqObj;
}

@property(retain, nonatomic) StepStrategyReport *stepStrategyReport;
@property(retain, nonatomic) SoundController *sndControler;
@property(retain, nonatomic) CharacterView *character;
@property(retain, nonatomic) TransmissionPower *powerLayer;
@property(retain, nonatomic) UILabel *powerPercentage, *stageLabel;
@property(retain, nonatomic) NSMutableArray *tPowerArray;
@property(retain, nonatomic)StrategyReport *report;;

@property(retain, nonatomic)UIImageView *angelView;
@property(retain, nonatomic)UIBezierPath *winPath;
@property(retain, nonatomic)UIButton *refresh, *rightArrow, *leftArrow;
@property (nonatomic, assign) id <GameViewDelegate> delegate;
@property (nonatomic, retain)UIButton *ck_UpButton, *ck_RightButton, *ck_DownButton, *ck_leftButton;
@property (nonatomic, retain)UIButton *addCircleButton, *removeCircleButton;


@property BOOL gameEnded;
@property BOOL makingStepwiseReport, initializingTree, stepwiseReportShown;
@property (retain, nonatomic)SQLManager *sql, *report_sql;;

@property(retain, nonatomic)UIButton *showStrategyButton;
@property(retain, nonatomic)UITextView *reportedStrategy;
@property(retain, nonatomic)ReportItem *currentReport;

@property(retain, nonatomic)    SubmissionRequestor *reqObj;

+(GameViewController*)getInstance;
- (void)setupView;
-(void)shownGhostCheck;
-(double)calcScale:(float)inX :(float)outX :(float)inY :(float)outY;
-(double)calcDistance:(float)inX :(float)outX :(float)inY :(float)outY;
-(double)getScaleFromPower:(double)power;
-(void)updateNodes;
-(void)updateNodeAndGhost:(int)selfid parent:(int)parent_id;
-(void)stopApplause;
-(void)applyChangesConfirmed;
-(void)finalTreeConfirmed;
-(void)restorePreviousTree;
-(void)restoreNextTree;
-(void)updateFuelBar:(double)currentPower;
-(void)generateTree;
-(void)deleteTree;
-(void)refreshTree;

//-(void)submitSQL;

-(void)moveCharacterTo:(CGPoint)location time:(float)duration;
-(void)add_A_PowerLayer:(double)power;
-(void)removePowerLayers;

- (void) reportScore: (int64_t) score forCategory: (NSString*) category;
- (void) reportAchievementIdentifier: (NSString*) identifier percentComplete: (float) percent;

//-(void)loadStrategyReport;

-(void)game_lost;
-(void)game_draw;
-(void)game_draw_animation;

-(void)game_won;
-(void)game_won_animation;
-(void)game_won_ghostAnimation;
-(void)game_won_alert;

-(void)game_draw_alert;
-(void)game_lost_alert;

-(void)turnOffLights;
-(void)turnOnLights;
-(void)moveGhost;
-(void)moveGhostPt2;//:(CGPoint*)points;
-(void)moveAngel;
-(void)moveAngelPt2;


-(void)gameEndedAlert:(NSString*) message TAG:(enum GameResult)tag;
-(void)gameEnded:(enum GameResult)tag;

-(void)renewTheGame;
-(void)restartTheGame;


-(void)updateStrategyReportTreeID;

-(void)done_reporting_alert;


-(BOOL)isGameProgress;
-(void)initRandomSeed;

-(void)modifyViewFor:(enum GameMode)tag;
-(void)updateStageNumber;

-(void)stageCleared;

-(void)characterLevelControl;

-(void)instruction;

-(void)computeScoreAndReportIt;
-(int)getGhostNumberForStage;

-(void)show_StepwiseReport;
-(void)removeStepWiseReportView;

-(void)showHideCrossKeys;
-(int)findTheClosestInTheDirection:(int)direction;
-(void)moveOneToTheDirection:(UIButton*)btn;
-(void)addOrRemove_A_Circle:(UIButton*)btn;
-(void)add_A_Circle_withButton;
-(void)remove_A_Circle_withButton;

-(BOOL)reorganizeForReportID:(ReportItem*)report;
-(void)reorganizeForGeneralGame;
-(void)showHideReportedStrategy;
-(void)updateStepwiseReport_forReportDetail;
-(BOOL)isCurrentCommentDefaultText:(NSString*)comment;
-(void)anonymousReportProcedure:(enum GameResult)tag;


-(CGPoint)generateRandomPoint;
-(BOOL)duplicatedPointCheck:(CGPoint)point;

@end

@protocol GameViewDelegate <NSObject>
@optional
-(void)stageWillRenew:(GameViewController*)stg;
-(void)stageIsUpdated:(GameViewController*)gameView;
@end
