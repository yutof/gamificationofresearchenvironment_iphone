//
//  EnemyView.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/23/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "EnemyView.h"
#import "GameViewController.h"

@implementation EnemyView
//_ghostTimer,
@synthesize  animating, applause_animating;
@synthesize ghostBody, ghostLH, ghostRH,app_ghostLH, app_ghostRH;
@synthesize delegate;//, animTag;

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}*/

CGFloat Degrees_To_Radians(CGFloat degrees);
CGFloat Degrees_To_Radians(CGFloat degrees) {return degrees * M_PI / 180;}

-(id)init
{
    CGRect frame = CGRectMake(0, 0, 40.0, 40.0);
    //    self.uncoveredNode = img;
    //    self.coveredNode = [UIImage imageNamed:@"node2.png"];
    
    if ([self initWithFrame:frame]) {
        // Custom initialization
        self.opaque = NO;        
        self.applause_animating = NO;
        //self.layer.shadowOpacity = 0.3;
        
        //Improves performance. 
        //UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.bounds];
        //self.layer.shadowPath = path.CGPath;        
        CGRect smallFrame = CGRectMake(0, 0, 15.0, 15.0);
        self.ghostLH = [CALayer layer];
        self.ghostLH.bounds = smallFrame;
        self.ghostLH.contents = (id)glh.CGImage;
        [self.layer addSublayer:self.ghostLH];
//        self.ghostLH.position = CGPointMake(25.0, 10.0f);
        self.ghostLH.position = CGPointMake(25.0, 25.0f);        
        self.ghostLH.anchorPoint = CGPointMake(0.0f, 0.0f);
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:smallFrame];
        self.ghostLH.shadowPath = path.CGPath;
        self.ghostLH.shadowOpacity = 0.5;
        
        self.ghostRH = [CALayer layer];
        self.ghostRH.bounds = smallFrame;
        self.ghostRH.contents = (id)grh.CGImage;
        [self.layer addSublayer:self.ghostRH];
//        self.ghostRH.position = CGPointMake(17.0, 15.0f);
//        self.ghostRH.position = CGPointMake(17.0, 25.0f);        
        self.ghostRH.anchorPoint = CGPointMake(1.0f, 0.0f);
        self.ghostRH.shadowPath = path.CGPath;
        self.ghostRH.shadowOpacity = 0.5;

        self.ghostLH.position = CGPointMake(25.0, 10.0f);    
        self.ghostRH.position = CGPointMake(17.0, 15.0f);
        
        
        self.app_ghostLH = [CALayer layer];
        self.app_ghostLH.bounds = smallFrame;
        self.app_ghostLH.contents = (id)glh.CGImage;
        [self.layer addSublayer:self.app_ghostLH];
        //        self.ghostLH.position = CGPointMake(25.0, 10.0f);
        self.app_ghostLH.position = CGPointMake(25.0, 25.0f);        
        self.app_ghostLH.anchorPoint = CGPointMake(0.0f, 0.0f);
        self.app_ghostLH.shadowPath = path.CGPath;
        self.app_ghostLH.shadowOpacity = 0.5;
        
        self.app_ghostRH = [CALayer layer];
        self.app_ghostRH.bounds = smallFrame;
        self.app_ghostRH.contents = (id)grh.CGImage;
        [self.layer addSublayer:self.app_ghostRH];
        //        self.ghostRH.position = CGPointMake(17.0, 15.0f);
        //        self.ghostRH.position = CGPointMake(17.0, 25.0f);        
        self.app_ghostRH.anchorPoint = CGPointMake(1.0f, 0.0f);
        self.app_ghostRH.shadowPath = path.CGPath;
        self.app_ghostRH.shadowOpacity = 0.5;
        self.app_ghostLH.position = CGPointMake(25.0, 25.0f);        
        self.app_ghostRH.position = CGPointMake(17.0, 25.0f);                
        
        
        self.ghostBody = [CALayer layer];
        self.ghostBody.bounds = frame;
        self.ghostBody.contents =(id)gb.CGImage;
        [self.layer addSublayer:self.ghostBody];
        self.ghostBody.position = self.layer.position;
        
        [self setPosition:DefaultPosition];
//UIBezierPath *bodypath = [UIBezierPath bezierPathWithRect:frame];
//        self.ghostBody.shadowPath = bodypath.CGPath;
//        self.ghostBody.shadowOpacity = 0.5;
        //        CGPoint center =CGPointMake(18.0, 15.0f); // CGPointMake(30.0f, 30.0f);
        
        
        
        //        self.ghost = [[[UIImageView alloc] initWithImage:[ghost_array objectAtIndex:0]] autorelease];
        //        [self addSubview:ghost];
        //        self.ghost.center = CGPointMake(30, 40);
        //        self.ghost.hidden = YES;
        //       myID = ghostID++;
        //nodeImage.frame = CGRectMake(0, 0, 20.0, 20.0);
		//node = uncoveredNode;//img;
		// Load the display strings
    }
    return self;

}//end of init


- (void)dealloc
{
    if(ghostBody)
        [ghostBody release];
    if(ghostLH)
        [ghostLH release];
    if(ghostRH)
        [ghostRH release];
    if(app_ghostLH)
        [app_ghostLH release];
    if(app_ghostRH)
        [app_ghostRH release];
        
    [super dealloc];
}

/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)hideGhost:(BOOL)flag
{
    
    ghostBody.hidden = flag;
    ghostLH.hidden = flag;
    ghostRH.hidden = flag;
    //    animating = flag;
    /*
    if(flag)
    {
        animating = NO;
        applause_animating = NO;
    }
    else
    {
        animating = YES;
        applause_animating = YES;
    }*/
}//end of flag

-(void)setPosition:(enum Positions)tag
{
    if(tag == DefaultPosition)
    {
        animating = YES;
        applause_animating = NO;
        self.ghostLH.hidden = NO;
        self.ghostRH.hidden = NO;
        self.app_ghostLH.hidden = YES;
        self.app_ghostRH.hidden = YES;
//        self.ghostLH.position = CGPointMake(25.0, 10.0f);    
//        self.ghostRH.position = CGPointMake(17.0, 15.0f);
    }
    else if(tag ==ApplausePosition)
    {
        applause_animating = YES;
        self.ghostLH.hidden = YES;
        self.ghostRH.hidden = YES;
        self.app_ghostLH.hidden = NO;
        self.app_ghostRH.hidden = NO;

    //   self.ghostLH.position = CGPointMake(25.0, 25.0f);        
    //   self.ghostRH.position = CGPointMake(17.0, 25.0f);                
    }
}

/*
-(void)updateImage:(int)parent_id
{
    if(parent_id > -2)
    {
        //light.hidden = NO;
                [self hideGhost:YES];
    }
    else
    {
        //light.hidden = YES;
                [self hideGhost:NO];
                [self moveGhost];
        
        
        
    }//end of else if  
}//end of updateImage
*/

#pragma mark Ghost animation methods
/*
-(void)startGhostTimer
{
}
-(void)stopGhostTimer
{
    animating = NO;
    applause_animating = NO;
}*/

/*
-(void)moveGhost
{
//    NSLog(@"moving");
    self._ghostTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(animationCheck) userInfo:nil repeats:NO];
//    NSLog(@"move");
    CABasicAnimation *topToBottom = [CABasicAnimation animation];
//    topToBottom.keyPath = @"transform.rotation.y";
    topToBottom.keyPath = @"transform.rotation.z";

    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];    
    topToBottom.duration = 1.5;//(self.duration);
    topToBottom.removedOnCompletion = NO;
    topToBottom.delegate = self;
//    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
//    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-120)];
    
    // topToBottom presentation layer in final state; preventing snap-back to original state
    topToBottom.fillMode = kCAFillModeBoth; 
    //    topToBottom.repeatCount = 10;
    //stopAnimation.beginTime = 1.5;
    topToBottom.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [ghostLH addAnimation:topToBottom forKey:@"topToBottom"];
//    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(150)];
//    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(00)];
//    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
//    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(120)];
    
    
    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    [ghostRH addAnimation:topToBottom forKey:@"topToBottom"];    
    
}
*/


-(void)moveGhost
{
    //    NSLog(@"moving");
    /*
     static int prev=0;
     
     //    ghost.image = [ghost_array objectAtIndex:prev];
     if(prev< 4)
     prev++;
     else
     prev =0;
     
     */
    CABasicAnimation *topToBottom = [CABasicAnimation animation];
    //    topToBottom.keyPath = @"transform.rotation.y";
    topToBottom.keyPath = @"transform.rotation.z";
    
    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];    
    topToBottom.duration = 1.5;//(self.duration);
    topToBottom.removedOnCompletion = NO;
    topToBottom.delegate = self;
    //    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
    //    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-120)];
    
    // topToBottom presentation layer in final state; preventing snap-back to original state
    topToBottom.fillMode = kCAFillModeBoth; 
    //    topToBottom.repeatCount = 10;
    //stopAnimation.beginTime = 1.5;
    topToBottom.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [ghostLH addAnimation:topToBottom forKey:@"topToBottomWithDelegate"];
    //    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(150)];
    //    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(00)];
    //    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    //    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(120)];
    
    
    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    [ghostRH addAnimation:topToBottom forKey:@"topToBottomWithDelegate"];    

//    NSLog(@"move ghost");
}

-(void)moveGhost_part2
{
    if(animating)
    {
//        NSLog(@"second part animating");
        //    NSLog(@"finished");        
        CABasicAnimation *bottomToTop = [CABasicAnimation animation];
        //        bottomToTop.keyPath = @"transform.rotation.y";
        bottomToTop.keyPath = @"transform.rotation.z";        
        //        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-120)];
        //        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];        
        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
        
        bottomToTop.duration = 1.5;//(self.duration);
        bottomToTop.removedOnCompletion = NO;
        // leaves presentation layer in final state; preventing snap-back to original state
        bottomToTop.fillMode = kCAFillModeBoth; 
        bottomToTop.delegate = self;
        //        bottomToTop.repeatCount = 10;
        //stopAnimation.beginTime = 1.5;
        bottomToTop.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [ghostLH addAnimation:bottomToTop forKey:@"seconPartEnded"];
        //        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(0)];
        //        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(150)];
        //        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(120)];
        //        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];        
        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
        [ghostRH addAnimation:bottomToTop forKey:@"secondPartEnded"];  
        
        //[self moveGhost];
    }//end of if animating
    else
    {
//        NSLog(@"second part not animating");
        CABasicAnimation *backToNoral= [CABasicAnimation animation];
        backToNoral.keyPath = @"transform.rotation.z";
        backToNoral.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
        backToNoral.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(0)];
        backToNoral.duration = 1.5;//(self.duration);
        backToNoral.removedOnCompletion = NO;
        // leaves presentation layer in final state; preventing snap-back to original state
        backToNoral.fillMode = kCAFillModeBoth; 
        backToNoral.repeatCount = 1;
        //stopAnimation.beginTime = 1.5;
        backToNoral.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [ghostLH addAnimation:backToNoral forKey:@"stopAnimation"];
        
        backToNoral.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
        backToNoral.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(00)];
        [ghostRH addAnimation:backToNoral forKey:@"stopAnimation"];    
        
    }

    
    
}//end of moveGhost_part2

-(void)applause
{

//    NSLog(@"applausing");
//    self._ghostTimer = [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(applauseAnimationCheck) userInfo:nil repeats:NO];
    //    NSLog(@"move");
    CABasicAnimation *topToBottom = [CABasicAnimation animation];
    topToBottom.keyPath = @"transform.rotation.y";
    //    topToBottom.keyPath = @"transform.rotation.z";
    
    //    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    //    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];    
    topToBottom.duration = .3;//(self.duration);
    topToBottom.removedOnCompletion = NO;
    topToBottom.delegate = self;
    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-20)];
    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-120)];
    
    // topToBottom presentation layer in final state; preventing snap-back to original state
    topToBottom.fillMode = kCAFillModeBoth; 
    //    topToBottom.repeatCount = 10;
    //stopAnimation.beginTime = 1.5;
    topToBottom.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [app_ghostLH addAnimation:topToBottom forKey:@"applause_part1"];
    //    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(150)];
    //    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(00)];
    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(20)];
    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(120)];
    
    
    //    topToBottom.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
    //    topToBottom.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    [app_ghostRH addAnimation:topToBottom forKey:@"applause_part1"];    

}

-(void)applause_part2
{
    //NSLog(@"applause part 1 finished");
    CABasicAnimation *bottomToTop = [CABasicAnimation animation];
    bottomToTop.keyPath = @"transform.rotation.y";
    //        bottomToTop.keyPath = @"transform.rotation.z";        
    bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-120)];
    bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-20)];        
    //        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
    //        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    
    bottomToTop.duration = .3;//(self.duration);
    bottomToTop.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    bottomToTop.fillMode = kCAFillModeBoth; 
    bottomToTop.delegate = self;
    //        bottomToTop.repeatCount = 10;
    //stopAnimation.beginTime = 1.5;
    bottomToTop.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [app_ghostLH addAnimation:bottomToTop forKey:@"applause_part_2"];
    //        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(0)];
    //        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(150)];
    bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(120)];
    bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(20)];        
    //        bottomToTop.fromValue = [NSNumber numberWithFloat:Degrees_To_Radians(60)];
    //        bottomToTop.toValue = [NSNumber numberWithFloat:Degrees_To_Radians(-60)];
    [app_ghostRH addAnimation:bottomToTop forKey:@"applause_part2"];    
    //[self moveGhost];

}//end of applause_part2


- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    if(flag && (theAnimation == [app_ghostLH animationForKey:@"applause_part1"] 
                          || theAnimation == [app_ghostRH animationForKey:@"applause_part1"] )   )
    {
        //NSLog(@"applause part is called");
        [self applause_part2];
    
    }
    else if(flag && (theAnimation == [app_ghostLH animationForKey:@"applause_part_2"] 
                     || theAnimation == [app_ghostLH animationForKey:@"applause_part_2"] )   )
    {
//        NSLog(@"applause 3rd part");
        if(applause_animating)
        {
            [[self delegate]applauseDidFinish:self];
        }//end of if animating
    }

    
    
    else if(flag && (theAnimation == [ghostLH animationForKey:@"topToBottomWithDelegate"] 
                || theAnimation == [ghostRH animationForKey:@"topToBottomWithDelegate"] )   )
    {
//        NSLog(@"move ghost second part?");
        [self moveGhost_part2];
        
    }
    else if(flag && (theAnimation == [ghostLH animationForKey:@"seconPartEnded"] 
                     || theAnimation == [ghostRH animationForKey:@"seconPartEnded"] )   )
    {
        //NSLog(@"moveGhost 3rd part");
        if(animating)
        {
            //NSLog(@"called moveDidFinish protocol");
            [[self delegate]moveDidFinish:self];
        }//end of if animating
    }
    
//    static int q =0;
    
//    NSLog(@"q is %d", q++);
    
}//end of animationDidStop
/*
-(void)animationCheck
{
//    if(self._ghostTimer)
//        [self._ghostTimer invalidate];
//    self._ghostTimer = nil;
    
//    if(animating)
//        [self moveGhost];
    
}//end of animationCheck

-(void)applauseAnimationCheck
{
//    if(self._ghostTimer)
//        [self._ghostTimer invalidate];
//    self._ghostTimer = nil;
    
//    if(applause_animating)
//        [self applause];
    
}//end of animationCheck
*/
@end
