//
//  HorseViewClass.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 10/1/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface HorseViewClass : UIView{
  UIImage *character;
    CALayer *rootLayer;
    CALayer *head, *body, *forefoot1, *forefoot2, *hindfoot1, *hindfoot2, *tail;
    BOOL killed;
}

-(void)setUpHourse;
-(void)moveAnimation:(float)duration;
-(void)makeMeTaller;

@end
