//
//  NodesViewClass.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/13/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "NodesViewClass.h"



@implementation NodesViewClass

@synthesize nodeImage, light;//, ghost;

- (id)init
{
    CGRect frame = CGRectMake(0, 0, 60.0, 60.0);
    
    if ([self initWithFrame:frame]) {
        // Custom initialization
        self.opaque = NO;
        self.nodeImage = [[[UIImageView alloc] initWithImage:street_lamp] autorelease];
        self.light = [[[UIImageView alloc] initWithImage:lamp_light] autorelease];
        [self addSubview:nodeImage];
        [self addSubview:light];
        light.hidden = YES;
        light.center = CGPointMake(30, 10);
    }
    return self;
}



-(void)updateFrame:(CGRect)frame
{self.frame = frame;
 self.nodeImage.frame = frame;}//end of updateFrame:(CGRect)frame 

- (void)dealloc
{
    if(nodeImage)
    [nodeImage release];
    
    if(light)
        [light release];
    
    [super dealloc];
}

- (void)drawRect:(CGRect)rect {}
#pragma mark - View lifecycle

-(void)viewDidLoad
{}//end of viewDidLoad


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)updateImage:(int)parent_id
{
    if(parent_id > -2)
    {
        light.hidden = NO;
    }
    else
    {
        light.hidden = YES;
    }//end of else if  
}//end of -(void)updateImage:(int)parent_id;
@end
