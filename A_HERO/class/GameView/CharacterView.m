//
//  CharacterView.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/13/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "CharacterView.h"
#import "Constants.h"

@implementation CharacterView
@synthesize /*character,*/ rootLayer, uma;
@synthesize body, leftHand, rightHand, leftFoot, rightFoot;
@synthesize killed, helmet;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
#pragma mark Animation methods
CGFloat DegreesToRadians(CGFloat degrees) ;
CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;}


-(void)moveAnimation:(float)duration;
{
    if(self.uma)
    {[self.uma moveAnimation:duration];}
    
    //[CATransition begin];
    [rightFoot removeAllAnimations];
    [leftFoot removeAllAnimations];
    [rightHand removeAllAnimations];
    [leftHand removeAllAnimations];
    
    //int signValue = -1;
    CABasicAnimation *lrAnimation = [CABasicAnimation animation];
    lrAnimation.keyPath = @"transform.rotation.z";
    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(60)];
    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-60)];
    lrAnimation.duration = .1;//(self.duration);
    lrAnimation.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    lrAnimation.fillMode = kCAFillModeBoth; 
    lrAnimation.repeatCount = (int)(10 * duration);
    lrAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		


    if(!onHorse)
    {
        lrAnimation.delegate = self;    
        [rightFoot addAnimation:lrAnimation forKey:@"lrAnimation"];
        lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-60)];
        lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(60)];
        [leftFoot addAnimation:lrAnimation forKey:@"walkingAnimation"];
    }
    else
    {
        lrAnimation.duration = .5;//(self.duration);
        lrAnimation.repeatCount = (int)(2 * duration);
        lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(0)];
        lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(20)];
        [rightFoot addAnimation:lrAnimation forKey:@"lrAnimation"];
        
        lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(00)];
        lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-10)];
        
        [leftFoot addAnimation:lrAnimation forKey:@"walkingAnimation"];

        lrAnimation.duration = .1;//(self.duration);
        lrAnimation.repeatCount = (int)(10 * duration);
    }
    lrAnimation.delegate = self;        
    lrAnimation.duration = .25;//(self.duration);
    lrAnimation.repeatCount = (int)(4 * duration);    

    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(120)];
    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(80)];
    [leftHand addAnimation:lrAnimation forKey:@"rotateAnimation"];
    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-120)];
    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-80)];
    lrAnimation.delegate = self;
    [rightHand addAnimation:lrAnimation forKey:@"rotateAnimation"];	
}//end of moveAnimation

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
  //  if([theAnimation valueForKey:@"rotateAnimation"] == @"rotateAnimation")
    //if (theAnimation == [[hearingAidHalo layer] animationForKey:@"Throb"])
//    NSLog(@"Animation interrupted: %@", (!flag)?@"Yes" : @"No");
    if(flag && (theAnimation == [leftFoot animationForKey:@"walkingAnimation"] || theAnimation == [rightFoot animationForKey:@"walkingAnimation"] || theAnimation == [rightFoot animationForKey:@"reviveAnimation"] )   )
    {
//    NSLog(@"walking animation ended");
        
    CABasicAnimation *stopAnimation = [CABasicAnimation animation];
    stopAnimation.keyPath = @"transform.rotation.z";
    stopAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-60)];
    stopAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(20)];
    stopAnimation.duration = 0.2;//(self.duration);
    stopAnimation.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    stopAnimation.fillMode = kCAFillModeBoth; 
    stopAnimation.repeatCount = 1;
    //stopAnimation.beginTime = 1.5;
    stopAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [leftFoot addAnimation:stopAnimation forKey:@"stopAnimation"];

    stopAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(60)];
    stopAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-20)];
    [rightFoot addAnimation:stopAnimation forKey:@"stopAnimation"];    
    }//end of if animation is walkingAnimation
    
//    else
//        NSLog(@"it wasn't the walking animation");
    
}//end of animationDidStop

-(void)reviveAnimation
{
    
    killed = NO;
    [rightFoot removeAllAnimations];
    [leftFoot removeAllAnimations];
    [rightHand removeAllAnimations];
    [leftHand removeAllAnimations];
    
    //    int signValue = -1;
    CABasicAnimation *rightFoot1 = [CABasicAnimation animation];
    rightFoot1.keyPath = @"transform.rotation.z";
    rightFoot1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-80)];
    rightFoot1.toValue = [NSNumber numberWithFloat:DegreesToRadians(-20)];
    
    rightFoot1.duration = 1.5;//(self.duration);
    rightFoot1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    rightFoot1.fillMode = kCAFillModeBoth; 
    rightFoot1.repeatCount = 1;
    rightFoot1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    rightFoot1.delegate = self;    
    
    CABasicAnimation *rightFoot2 = [CABasicAnimation animationWithKeyPath:@"position"];
    rightFoot2.delegate = self; //to get the animationDidStop:finished: message
    rightFoot2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    rightFoot2.toValue =[NSValue valueWithCGPoint:CGPointMake(28.0, 45.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    rightFoot2.removedOnCompletion = NO;
    
    //    anim.toValue = [NSValue valueWithPoint:NSPointFromCGPoint()];
    //    [selection addAnimation:anim forKey:@"positionAnimation"];
    
    // Add the animation to the selection layer. This causes it to begin animating. 
    
    //    [rightFoot addAnimation:lrAnimation forKey:@"killed"];
    //    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-60)];
    //    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(60)];
    //    [leftFoot addAnimation:lrAnimation forKey:@"lrAnimation"];
    
    
    CAAnimationGroup *group = [CAAnimationGroup animation]; 
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    [group setAnimations:[NSArray arrayWithObjects:rightFoot1, rightFoot2, nil]];
    group.duration = .5;
    [rightFoot addAnimation:group forKey:@"reviveAnimation"];    
    
    
    CABasicAnimation *leftFoot1 = [CABasicAnimation animation];
    leftFoot1.keyPath = @"transform.rotation.z";
    leftFoot1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(80)];
    leftFoot1.toValue = [NSNumber numberWithFloat:DegreesToRadians(20)];
    
    leftFoot1.duration = .5;//(self.duration);
    leftFoot1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    leftFoot1.fillMode = kCAFillModeBoth; 
    leftFoot1.repeatCount = 1;
    leftFoot1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    leftFoot1.delegate = self;    
    
    CABasicAnimation *leftFoot2 = [CABasicAnimation animationWithKeyPath:@"position"];
    leftFoot2.delegate = self; //to get the animationDidStop:finished: message
    leftFoot2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    leftFoot2.toValue =[NSValue valueWithCGPoint:CGPointMake(28.0, 45.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    leftFoot2.removedOnCompletion = NO;
    
    [group setAnimations:[NSArray arrayWithObjects:leftFoot1, leftFoot2, nil]];    
    [leftFoot addAnimation:group forKey:@"reviveAnimation"];
    
    //    lrAnimation.duration = .25;//(self.duration);
    //    lrAnimation.repeatCount = 4;    
    
    CABasicAnimation *leftHand1 = [CABasicAnimation animation];
    leftHand1.keyPath = @"transform.rotation.z";
    leftHand1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(140)];
    leftHand1.toValue = [NSNumber numberWithFloat:DegreesToRadians(120)];
    
    leftHand1.duration = .5;//(self.duration);
    leftHand1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    leftHand1.fillMode = kCAFillModeBoth; 
    leftHand1.repeatCount = 1;
    leftHand1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    leftHand1.delegate = self;    
    
    CABasicAnimation *leftHand2 = [CABasicAnimation animationWithKeyPath:@"position"];
    leftHand2.delegate = self; //to get the animationDidStop:finished: message
    leftHand2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    leftHand2.toValue =[NSValue valueWithCGPoint:CGPointMake(29.0, 36.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    leftHand2.removedOnCompletion = NO;
    
    [group setAnimations:[NSArray arrayWithObjects:leftHand1, leftHand2, nil]];    
    
    [leftHand addAnimation:group forKey:@"reviveAnimation"];
    
    
    CABasicAnimation *rightHand1 = [CABasicAnimation animation];
    rightHand1.keyPath = @"transform.rotation.z";
    rightHand1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-140)];
    rightHand1.toValue = [NSNumber numberWithFloat:DegreesToRadians(-120)];
    
    rightHand1.duration = .5;//(self.duration);
    rightHand1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    rightHand1.fillMode = kCAFillModeBoth; 
    rightHand1.repeatCount = 1;
    rightHand1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    rightHand1.delegate = self;    
    
    CABasicAnimation *rightHand2 = [CABasicAnimation animationWithKeyPath:@"position"];
    rightHand2.delegate = self; //to get the animationDidStop:finished: message
    rightHand2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    rightHand2.toValue =[NSValue valueWithCGPoint:CGPointMake(29.0, 36.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    rightHand2.removedOnCompletion = NO;
    [group setAnimations:[NSArray arrayWithObjects:rightHand1, rightHand2, nil]];        
    [rightHand addAnimation:group forKey:@"reviveAnimation"];	
    
    
    CABasicAnimation *body1 = [CABasicAnimation animation];
    body1.keyPath = @"transform.rotation.z";
    body1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(30)];
    body1.toValue = [NSNumber numberWithFloat:DegreesToRadians(00)];
    
    body1.duration = .5;//(self.duration);
    body1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    body1.fillMode = kCAFillModeBoth; 
    body1.repeatCount = 1;
    body1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    body1.delegate = self;    
    
    CABasicAnimation *body2 = [CABasicAnimation animationWithKeyPath:@"position"];
    body2.delegate = self; //to get the animationDidStop:finished: message
    body2.fromValue =[NSValue valueWithCGPoint:body.position];
    body2.toValue =[NSValue valueWithCGPoint:CGPointMake(body.position.x, body.position.y - 10.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    body2.removedOnCompletion = NO;
    [group setAnimations:[NSArray arrayWithObjects:body1, body2, nil]];        
    [body addAnimation:group forKey:@"reviveAnimation"];	
    
    [self moveAnimation:0.5];
    
}//end of setInitialPosition


-(void)killedAnimation
{
//    body
//    leftHand
//    rightHand
//    leftFoot
//    rightFoot;
    self.killed = YES;
    
    [rightFoot removeAllAnimations];
    [leftFoot removeAllAnimations];
    [rightHand removeAllAnimations];
    [leftHand removeAllAnimations];
    
    //    int signValue = -1;
    CABasicAnimation *rightFoot1 = [CABasicAnimation animation];
    rightFoot1.keyPath = @"transform.rotation.z";
    rightFoot1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(60)];
    rightFoot1.toValue = [NSNumber numberWithFloat:DegreesToRadians(80)];
    
    rightFoot1.duration = .5;//(self.duration);
    rightFoot1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    rightFoot1.fillMode = kCAFillModeBoth; 
    rightFoot1.repeatCount = 1;
    rightFoot1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    rightFoot1.delegate = self;    
    
    CABasicAnimation *rightFoot2 = [CABasicAnimation animationWithKeyPath:@"position"];
    rightFoot2.delegate = self; //to get the animationDidStop:finished: message
    rightFoot2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    rightFoot2.toValue =[NSValue valueWithCGPoint:CGPointMake(18.0, 55.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    rightFoot2.removedOnCompletion = NO;
//    anim.toValue = [NSValue valueWithPoint:NSPointFromCGPoint()];
//    [selection addAnimation:anim forKey:@"positionAnimation"];
    
    // Add the animation to the selection layer. This causes it to begin animating. 

    //[rightFoot addAnimation:lrAnimation forKey:@"killed"];
    //        lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-60)];
    //    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(60)];
    //    [leftFoot addAnimation:lrAnimation forKey:@"lrAnimation"];
    
    
    CAAnimationGroup *group = [CAAnimationGroup animation]; 
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    [group setAnimations:[NSArray arrayWithObjects:rightFoot1, rightFoot2, nil]];
    group.duration = .5;
    [rightFoot addAnimation:group forKey:@"killedAnimation"];    
    
    
    

    CABasicAnimation *leftFoot1 = [CABasicAnimation animation];
    leftFoot1.keyPath = @"transform.rotation.z";
    leftFoot1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-60)];
    leftFoot1.toValue = [NSNumber numberWithFloat:DegreesToRadians(-80)];
    
    leftFoot1.duration = .5;//(self.duration);
    leftFoot1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    leftFoot1.fillMode = kCAFillModeBoth; 
    leftFoot1.repeatCount = 1;
    leftFoot1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    leftFoot1.delegate = self;    
    
    CABasicAnimation *leftFoot2 = [CABasicAnimation animationWithKeyPath:@"position"];
    leftFoot2.delegate = self; //to get the animationDidStop:finished: message
    leftFoot2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    leftFoot2.toValue =[NSValue valueWithCGPoint:CGPointMake(38.0, 55.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    leftFoot2.removedOnCompletion = NO;

    [group setAnimations:[NSArray arrayWithObjects:leftFoot1, leftFoot2, nil]];    
    [leftFoot addAnimation:group forKey:@"killedAnimation"];
    
//    lrAnimation.duration = .25;//(self.duration);
//    lrAnimation.repeatCount = 4;    

    CABasicAnimation *leftHand1 = [CABasicAnimation animation];
    leftHand1.keyPath = @"transform.rotation.z";
    leftHand1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(-120)];
    leftHand1.toValue = [NSNumber numberWithFloat:DegreesToRadians(-140)];
    
    leftHand1.duration = .5;//(self.duration);
    leftHand1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    leftHand1.fillMode = kCAFillModeBoth; 
    leftHand1.repeatCount = 1;
    leftHand1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    leftHand1.delegate = self;    
    
    CABasicAnimation *leftHand2 = [CABasicAnimation animationWithKeyPath:@"position"];
    leftHand2.delegate = self; //to get the animationDidStop:finished: message
    leftHand2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    leftHand2.toValue =[NSValue valueWithCGPoint:CGPointMake(49.0, 26.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    leftHand2.removedOnCompletion = NO;
    
    [group setAnimations:[NSArray arrayWithObjects:leftHand1, leftHand2, nil]];    
    
    [leftHand addAnimation:group forKey:@"killedAnimation"];
    
    CABasicAnimation *rightHand1 = [CABasicAnimation animation];
    rightHand1.keyPath = @"transform.rotation.z";
    rightHand1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(120)];
    rightHand1.toValue = [NSNumber numberWithFloat:DegreesToRadians(140)];
    
    rightHand1.duration = .5;//(self.duration);
    rightHand1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    rightHand1.fillMode = kCAFillModeBoth; 
    rightHand1.repeatCount = 1;
    rightHand1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    rightHand1.delegate = self;    
    
    CABasicAnimation *rightHand2 = [CABasicAnimation animationWithKeyPath:@"position"];
    rightHand2.delegate = self; //to get the animationDidStop:finished: message
    rightHand2.fromValue =[NSValue valueWithCGPoint:rightFoot.position];
    rightHand2.toValue =[NSValue valueWithCGPoint:CGPointMake(19.0, 26.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    rightHand2.removedOnCompletion = NO;
    [group setAnimations:[NSArray arrayWithObjects:rightHand1, rightHand2, nil]];        
    [rightHand addAnimation:group forKey:@"killedAnimation"];	

    CABasicAnimation *body1 = [CABasicAnimation animation];
    body1.keyPath = @"transform.rotation.z";
    body1.fromValue = [NSNumber numberWithFloat:DegreesToRadians(0)];
    body1.toValue = [NSNumber numberWithFloat:DegreesToRadians(30)];
    
    body1.duration = .5;//(self.duration);
    body1.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    body1.fillMode = kCAFillModeBoth; 
    body1.repeatCount = 1;
    body1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    body1.delegate = self;    
    
    CABasicAnimation *body2 = [CABasicAnimation animationWithKeyPath:@"position"];
    body2.delegate = self; //to get the animationDidStop:finished: message
    body2.fromValue =[NSValue valueWithCGPoint:body.position];
    body2.toValue =[NSValue valueWithCGPoint:CGPointMake(body.position.x, body.position.y + 10.0f)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    body2.removedOnCompletion = NO;
    [group setAnimations:[NSArray arrayWithObjects:body1, body2, nil]];        
    [body addAnimation:group forKey:@"killedAnimation"];	

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame]; 

    if (self) { 
		[self setUpCharacter];
    }//end of if self	
    return self;
}//end of if initWithFrame

/*
- (id)init {
        //self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    UIImage *img =[UIImage imageNamed:@"character.png"];
    CGRect frame = CGRectMake(0, 0, 60.0, 60.0);
    
    body = [[CALayer alloc] init];
    body = self.layer;
//    body.anchorPoint = CGPointMake(30, 15.0);
		character = img;    
    body.contents =(id)character.CGImage;
    
    if ([self initWithFrame:frame]) {
        // Custom initialization
        self.opaque = NO;
		character = img;
		
		// Load the display strings
    }
    return self;
}*/



#define STRING_INDENT 20
/*
- (void)drawRect:(CGRect)rect {
	
	// Draw the placard at 0, 0
	//[character drawAtPoint:(CGPointMake(3.0, 3.0))];
		
	// Find point at which to draw the string so it will be in the center of the view
	// place of text y x=30.30 == 100.100 
}*/


-(void)setUpCharacter
{
    self.killed = NO;
    UIImage *img =[UIImage imageNamed:@"main_Body.png"];
    CGRect frame = CGRectMake(0, 0, 60.0, 60.0);

    rootLayer = self.layer;
    body = [CALayer layer];
    body.bounds = frame;
    //character = img;
    
    body.contents =(id)img.CGImage;
    [rootLayer addSublayer:body];
    CGRect smallFrame = CGRectMake(0, 0, 3, 15);//was     CGRect smallFrame = CGRectMake(0, 0, 3, 25);
    img = [UIImage imageNamed:@"man_foot.png"];
    leftFoot = [CALayer layer];
    leftFoot.bounds = smallFrame;
    leftFoot.contents = (id)img.CGImage;
    [body addSublayer:leftFoot];
    leftFoot.anchorPoint = CGPointMake(.0f, 0.0f);

    rightFoot = [CALayer layer];
    rightFoot.bounds = smallFrame;
    rightFoot.contents = (id)img.CGImage;
    //    [footimg retain];
    [body addSublayer:rightFoot];
    rightFoot.anchorPoint = CGPointMake(0, 0);

    smallFrame = CGRectMake(0, 0, 3, 10.0f);//was     smallFrame = CGRectMake(0, 0, 3, 20.0f)
    //img = [UIImage imageNamed:@"main_rightHand.png"];
    rightHand = [CALayer layer];
    rightHand.bounds = smallFrame;
    rightHand.contents = (id)img.CGImage;
    [body addSublayer:rightHand];
    rightHand.anchorPoint = CGPointMake(0.0, 0.0);
    
    leftHand = [CALayer layer];
    leftHand.bounds = smallFrame;
    leftHand.contents = (id)img.CGImage;
    [body addSublayer:leftHand];
    leftHand.anchorPoint = CGPointMake(0.0, 0.0);

    
    //[self setInitialPosition];
    body.position = rootLayer.position;
    CGPoint center =CGPointMake(28.0, 45.0f); // CGPointMake(30.0f, 30.0f);
    leftFoot.position = CGPointMake(28.0, 45.0f);
    
    //img = [UIImage imageNamed:@"foot2.png"];
    rightFoot.position = center;
    center = CGPointMake(29.0, 36.0f);    
    rightHand.position = center;
    
    //img = [UIImage imageNamed:@"main_leftHand.png"];
    leftHand.position = center;
    
	CABasicAnimation *rotateAnimation = [CABasicAnimation animation];
    rotateAnimation.keyPath = @"transform.rotation.z";
    rotateAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(0)];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-20)];
    rotateAnimation.duration = 0;//(self.duration);
    rotateAnimation.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    rotateAnimation.fillMode = kCAFillModeBoth; 
    rotateAnimation.repeatCount = 0;
    rotateAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];    
    // Add the animation to the selection layer. This causes it to begin animating. 
    [leftFoot addAnimation:rotateAnimation forKey:@"rotateAnimation"];
    
	rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(20)];
	[rightFoot addAnimation:rotateAnimation forKey:@"rotateAnimation"];	
    
    rotateAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(0)];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(120)];    
    [leftHand addAnimation:rotateAnimation forKey:@"rotateAnimation"];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-120)];    
    [rightHand addAnimation:rotateAnimation forKey:@"rotateAnimation"];	
    
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(0)];    
    [body addAnimation:rotateAnimation forKey:@"rotateAnimation"];
    
    //UIImageView *mainBody = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"main_Body.png"]];
    //[self addSubview:mainBody];
    
    if(characterLevel >= 2)
        onHorse = YES;
    else
        onHorse = NO;
    
}//end of setUpCharacter

- (void)dealloc
{
   // CALayer *rootLayer;
    if(body)
        [body release];
    if(leftHand)
        [leftHand release];
    if(rightHand)
        [rightHand release];
    if(leftFoot)
        [leftFoot release];
    if(rightFoot)
        [rightFoot release];
    if(helmet)
        [helmet release];

    if(self.uma)
        [self.uma release];
    
    [super dealloc];
}

#pragma mark Character Modification Methods

-(void)changeDirectionTo:(enum CharacterDirection)direction
{
    if(!uma)
    {
        self.uma =[[HorseViewClass alloc] initWithFrame:CGRectMake(0, 0, 120.0, 120.0)];
        [self.layer insertSublayer:self.uma.layer atIndex:0];
        uma.center = CGPointMake(110, 100);
    } 
    if(direction == CD_Left)
        uma.hidden = YES;
    else
        uma.hidden = NO;
    

}
-(void)makeMeTaller
{
    /*
    if(!uma)
    {
        self.uma =[[HorseViewClass alloc] initWithFrame:CGRectMake(0, 0, 120.0, 120.0)];
        [self.layer insertSublayer:self.uma.layer atIndex:0];
        uma.center = CGPointMake(110, 100);
    } 


    [self rideOnHorse];
    */
    if(characterLevel == 0)
    {
    CGRect smallFrame = CGRectMake(0, 0, 3, 15.0f+consecutiveWinCounter);//was     CGRect smallFrame = CGRectMake(0, 0, 3, 25);
    leftFoot.bounds = smallFrame;
    rightFoot.bounds = smallFrame;    
    smallFrame = CGRectMake(0, 0, 3, 10.0f+consecutiveWinCounter);//was     smallFrame = CGRectMake(0, 0, 3, 20.0f)
    rightHand.bounds = smallFrame;
    leftHand.bounds = smallFrame;
    }
    else if(characterLevel == 1)
    {
        if(!uma)
        {
            self.uma =[[HorseViewClass alloc] initWithFrame:CGRectMake(0, 0, 120.0, 120.0)];
            [self insertSubview:self.uma atIndex:0];

            //            [self.layer insertSublayer:self.uma.layer atIndex:0];
            uma.center = CGPointMake(110, 100);
            [self.uma makeMeTaller];
        } 
        else
        {
            self.uma.alpha = 1.0f;
            [self.uma makeMeTaller];
        }
    }
    else if(characterLevel == 2 /*&& onHorse == NO*/)
    {

        if(!uma)
        {
            self.uma =[[HorseViewClass alloc] initWithFrame:CGRectMake(0, 0, 120.0, 120.0)];
            [self insertSubview:self.uma atIndex:0];
            //[self.layer insertSublayer:self.uma.layer atIndex:0];
            uma.center = CGPointMake(110, 100);
            [self.uma makeMeTaller];
        } 

        self.uma.alpha = 1.0f;
        //[self.uma makeMeTaller];
        [self rideOnHorse];
        
    }
    else if(characterLevel == 2 && consecutiveWinCounter >= 20)
    {
        if(helmet == NULL)
        {
            UIImage *img = [UIImage imageNamed:@"helmet.png"];
            helmet = [CALayer layer];
            helmet.bounds = CGRectMake(0, 0, 60, 30);
            helmet.contents = (id)img.CGImage;
            [body addSublayer:helmet];
            helmet.anchorPoint = CGPointMake(0.0, 0.0);
            helmet.position = CGPointMake(0, -10.0f);
            
        }

    }
    
 

    
//    NSLog(@"level %d, consec %d",characterLevel, consecutiveWinCounter);
    
}
-(void)rideOnHorse
{
    onHorse = YES;
    self.uma.center = CGPointMake(self.body.position.x+28, self.body.position.y+ 60);
        


	CABasicAnimation *rotateAnimation = [CABasicAnimation animation];
    rotateAnimation.keyPath = @"transform.rotation.z";
    rotateAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(0)];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(20)];
    rotateAnimation.duration = 0;//(self.duration);
    rotateAnimation.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    rotateAnimation.fillMode = kCAFillModeBoth; 
    rotateAnimation.repeatCount = 0;
    rotateAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];    
    [leftFoot addAnimation:rotateAnimation forKey:@"rotateAnimation"];    
	rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-10)];
	[rightFoot addAnimation:rotateAnimation forKey:@"rotateAnimation"];	
    
}

-(void)horseFadeout
{
    if(!uma)
        return;
    
    onHorse = NO;
    
        /*
    {
        self.uma =[[HorseViewClass alloc] initWithFrame:CGRectMake(0, 0, 120.0, 120.0)];
        [self.layer insertSublayer:self.uma.layer atIndex:0];
        uma.center = CGPointMake(110, 100);
    }   */
    
    [self.uma moveAnimation:1.0];
    CABasicAnimation *body2 = [CABasicAnimation animationWithKeyPath:@"position"];
    //    body2.delegate = self; //to get the animationDidStop:finished: message
    body2.fromValue =[NSValue valueWithCGPoint:self.uma.center];
    body2.toValue =[NSValue valueWithCGPoint:CGPointMake(-120, self.center.y - 10)];
    //rightFoot.position = CGPointMake(18.0, 55.0f);
    //  body2.removedOnCompletion = NO;
    body2.duration=1.5f;
    
    CABasicAnimation *fadeOutAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeOutAnimation.duration = 1.0f;
    fadeOutAnimation.removedOnCompletion = NO;
    fadeOutAnimation.fillMode = kCAFillModeForwards;
    fadeOutAnimation.toValue = [NSNumber numberWithFloat:0.0f];
    CAAnimationGroup *group = [CAAnimationGroup animation];     
    [group setAnimations:[NSArray arrayWithObjects:body2, fadeOutAnimation, nil]];        
//    [uma.layer addAnimation:fadeOutAnimation forKey:@"animateOpacity"];    

    [uma.layer addAnimation:body2 forKey:@"position"];	
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    uma.alpha = 0.0f;
    [UIView commitAnimations];
}
#pragma mark - View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
