//
//  HorseViewClass.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 10/1/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import "HorseViewClass.h"
#import "Constants.h"
//#import "CharacterView.h"
@implementation HorseViewClass

CGFloat DegreesToRadians_2(CGFloat degrees);

CGFloat DegreesToRadians_2(CGFloat degrees) {return degrees * M_PI / 180;}

-(void)dealloc
{
    
    if(head)
        [head release];
    if(body)
        [body release];
    if(forefoot1)
        [forefoot1 release];
    if(forefoot2)
        [forefoot2 release];
    if(hindfoot1)
        [hindfoot1 release];
    if(hindfoot2)
        [hindfoot2 release];
    if(tail)
        [tail release];

    if(rootLayer)
        [rootLayer release];

    
    [super dealloc];
}
#pragma mark - View lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame]; 
    
    if (self) { 
		[self setUpHourse];
    }//end of if self	
    return self;
}//end of if initWithFrame

-(void)moveAnimation:(float)duration
{
    [forefoot1 removeAllAnimations];
    [forefoot2 removeAllAnimations];
    [hindfoot1 removeAllAnimations];
    [hindfoot2 removeAllAnimations];
    CABasicAnimation *lrAnimation = [CABasicAnimation animation];
    lrAnimation.keyPath = @"transform.rotation.z";
    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians_2(0)];
    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians_2(-40)];
    lrAnimation.duration = .1;//(self.duration);
    //lrAnimation.removedOnCompletion = NO;
    //leaves presentation layer in final state; preventing snap-back to original state
    lrAnimation.fillMode = kCAFillModeBoth; 
    lrAnimation.repeatCount = (int)(10 * duration);
    lrAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];		
    lrAnimation.delegate = self;    
    [forefoot1 addAnimation:lrAnimation forKey:@"lrAnimation"];
    
    
    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians_2(0)];
    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians_2(40)];
    //    stopAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(60)];
    //    stopAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-30)];
    //    [group setAnimations:[NSArray arrayWithObjects:lrAnimation, stopAnimation, nil]];
    //    lrAnimation.beginTime = 1.0;
    [forefoot2 addAnimation:lrAnimation forKey:@"walkingAnimation"];
    lrAnimation.duration = .25;//(self.duration);
    lrAnimation.repeatCount = (int)(4 * duration);    
    
    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians_2(0)];
    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians_2(40)];
    //rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(120)];    
    [hindfoot1 addAnimation:lrAnimation forKey:@"rotateAnimation"];
    //rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-120)];    
    lrAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians_2(0)];
    lrAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians_2(-40)];
    lrAnimation.delegate = self;
    [hindfoot2 addAnimation:lrAnimation forKey:@"rotateAnimation"];	
    

}

-(void)setUpHourse
{
    UIImage *img =[UIImage imageNamed:@"horse_head.png"];
    CGRect frame = CGRectMake(0, 0, 25.0, 25.0);
    
    rootLayer = self.layer;
    head = [CALayer layer];
    head.bounds = frame;
    //    character = img;    
    head.contents =(id)img.CGImage;
    [rootLayer addSublayer:head];
    CGRect smallFrame = CGRectMake(0, 0, 30, 3);//was     CGRect smallFrame = CGRectMake(0, 0, 3, 25);
    img = [UIImage imageNamed:@"horse_body.png"];
    body = [CALayer layer];
    body.bounds = smallFrame;
    body.contents = (id)img.CGImage;
    [head addSublayer:body];
    body.anchorPoint = CGPointMake(0.0f, 0.0f);
    body.position = CGPointMake(24.0, 24.0f);
    
    
    smallFrame = CGRectMake(0, 0, 2, 15);//was     CGRect smallFrame = CGRectMake(0, 0, 3, 25);
    img = [UIImage imageNamed:@"horse_leg.png"];
    forefoot1 = [CALayer layer];
    forefoot1.bounds = smallFrame;
    forefoot1.contents = (id)img.CGImage;
    [body addSublayer:forefoot1];
    forefoot1.anchorPoint = CGPointMake(.0f, 0.0f);
    
    
    forefoot2.position = CGPointMake(00.0, 00.0f);
    forefoot2 = [CALayer layer];
    forefoot2.bounds = smallFrame;
    forefoot2.contents = (id)img.CGImage;
    [body addSublayer:forefoot2];
    forefoot2.anchorPoint = CGPointMake(.0f, 0.0f);
    forefoot2.position = CGPointMake(5.0f, 00.0f);

    

     hindfoot1 = [CALayer layer];
    hindfoot1.bounds = smallFrame;
    hindfoot1.contents = (id)img.CGImage;
    [body addSublayer:hindfoot1];
    hindfoot1.anchorPoint = CGPointMake(.0f, 0.0f);
    hindfoot1.position = CGPointMake(30.0f, 00.0f);

    hindfoot2 = [CALayer layer];
    hindfoot2.bounds = smallFrame;
    hindfoot2.contents = (id)img.CGImage;
    [body addSublayer:hindfoot2];
    hindfoot2.anchorPoint = CGPointMake(.0f, 0.0f);
    hindfoot2.position = CGPointMake(25.0f, 00.0f);
    
    smallFrame = CGRectMake(0, 0, 10, 10);
    UIImage *tailImage = [UIImage imageNamed:@"horse_tail.png"];
    tail = [CALayer layer];
    tail.bounds = smallFrame;
    tail.contents = (id)tailImage.CGImage;
    [body addSublayer:tail];
    tail.anchorPoint = CGPointMake(.0f, 0.0f);
    tail.position = CGPointMake(25.0f, -10.0f);
    

    /*
    leftFoot = [CALayer layer];
    leftFoot.bounds = smallFrame;
    leftFoot.contents = (id)img.CGImage;
    [body addSublayer:leftFoot];
    leftFoot.anchorPoint = CGPointMake(.0f, 0.0f);
    
    rightFoot = [CALayer layer];
    rightFoot.bounds = smallFrame;
    rightFoot.contents = (id)img.CGImage;
    //    [footimg retain];
    [body addSublayer:rightFoot];
    rightFoot.anchorPoint = CGPointMake(0, 0);
    
    smallFrame = CGRectMake(0, 0, 3, 10.0f);//was     smallFrame = CGRectMake(0, 0, 3, 20.0f)
    //img = [UIImage imageNamed:@"main_rightHand.png"];
    rightHand = [CALayer layer];
    rightHand.bounds = smallFrame;
    rightHand.contents = (id)img.CGImage;
    [body addSublayer:rightHand];
    rightHand.anchorPoint = CGPointMake(0.0, 0.0);
    
    leftHand = [CALayer layer];
    leftHand.bounds = smallFrame;
    leftHand.contents = (id)img.CGImage;
    [body addSublayer:leftHand];
    leftHand.anchorPoint = CGPointMake(0.0, 0.0);
    
    
    //    [self setInitialPosition];
    body.position = rootLayer.position;
    
    CGPoint center =CGPointMake(28.0, 45.0f); // CGPointMake(30.0f, 30.0f);
    leftFoot.position = CGPointMake(28.0, 45.0f);
    
    
    //img = [UIImage imageNamed:@"foot2.png"];
    rightFoot.position = center;
    
    center = CGPointMake(29.0, 36.0f);    
    rightHand.position = center;
    
    //img = [UIImage imageNamed:@"main_leftHand.png"];
    leftHand.position = center;
    
    
	CABasicAnimation *rotateAnimation = [CABasicAnimation animation];
    rotateAnimation.keyPath = @"transform.rotation.z";
    rotateAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(0)];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-20)];
    rotateAnimation.duration = 0;//(self.duration);
    rotateAnimation.removedOnCompletion = NO;
    // leaves presentation layer in final state; preventing snap-back to original state
    rotateAnimation.fillMode = kCAFillModeBoth; 
    rotateAnimation.repeatCount = 0;
    rotateAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];    
    // Add the animation to the selection layer. This causes it to begin animating. 
    [leftFoot addAnimation:rotateAnimation forKey:@"rotateAnimation"];
    
	rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(20)];
	[rightFoot addAnimation:rotateAnimation forKey:@"rotateAnimation"];	
    
    rotateAnimation.fromValue = [NSNumber numberWithFloat:DegreesToRadians(0)];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(120)];    
    [leftHand addAnimation:rotateAnimation forKey:@"rotateAnimation"];
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(-120)];    
    [rightHand addAnimation:rotateAnimation forKey:@"rotateAnimation"];	
    
    rotateAnimation.toValue = [NSNumber numberWithFloat:DegreesToRadians(0)];    
    [body addAnimation:rotateAnimation forKey:@"rotateAnimation"];
    */
    
    
    //UIImageView *mainBody = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"main_Body.png"]];
    //[self addSubview:mainBody];
    
}//end of setUpCharacter


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

-(void)makeMeTaller
{
    CGRect frame = CGRectMake(0, 0, 25.0f + consecutiveWinCounter, 25.0f+consecutiveWinCounter);
    head.bounds = frame;
    body.position = CGPointMake(24.0 + consecutiveWinCounter, 24.0f+ consecutiveWinCounter);
    CGRect smallFrame = CGRectMake(0, 0, 2, 15.0f+consecutiveWinCounter);//was     CGRect smallFrame = CGRectMake(0, 0, 3, 25);
    forefoot1.bounds = smallFrame;
    forefoot2.bounds = smallFrame;    
    hindfoot1.bounds = smallFrame;
    hindfoot2.bounds = smallFrame;

    smallFrame = CGRectMake(0, 0, 10.0f + consecutiveWinCounter , 10.0f + consecutiveWinCounter);
    tail.bounds = smallFrame;
    
    /*
    if(characterLevel == 1){
        if(!uma)
        {
            self.uma =[[HorseViewClass alloc] initWithFrame:CGRectMake(0, 0, 120.0, 120.0)];
            [self.layer insertSublayer:self.uma.layer atIndex:0];
            uma.center = CGPointMake(110, 100);
            // [self insertSubview:self.uma atIndex:0];
        }
    }*/
    
    

}


@end
