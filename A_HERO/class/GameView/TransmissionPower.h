//
//  TransmissionPower.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/14/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TransmissionPower : UIView {
    UIImage *power;   
}

-(void)setUpPower;


@end
