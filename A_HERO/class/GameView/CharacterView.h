//
//  CharacterView.h
//  Setsuden
//
//  Created by Yuto Fujikawa on 6/13/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "HorseViewClass.h"

enum CharacterDirection{
    CD_Left=0,
    CD_Right
};

//#import "Constants.h"
@interface CharacterView : UIView {
//    UIImage *character;
    CALayer *rootLayer;
    CALayer *body, *leftHand, *rightHand, *leftFoot, *rightFoot;
    CALayer *helmet;
    BOOL killed;
    BOOL onHorse;
    HorseViewClass *uma;
}

-(void)reviveAnimation;
-(void)moveAnimation:(float)duration;
-(void)killedAnimation; 
-(void)setUpCharacter;
-(void)makeMeTaller;
-(void)rideOnHorse;
-(void)horseFadeout;
-(void)changeDirectionTo:(enum CharacterDirection)direction;

//@property(retain, nonatomic)UIImage *character;
@property(retain, nonatomic)CALayer *rootLayer;
@property(retain, nonatomic)CALayer *body, *leftHand, *rightHand, *leftFoot, *rightFoot;
@property BOOL killed;
@property(retain,  nonatomic) CALayer *helmet;
@property(retain, nonatomic)HorseViewClass *uma;
@end