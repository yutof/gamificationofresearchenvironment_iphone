//
//  GameViewController.m
//  Setsuden
//
//  Created by Yuto Fujikawa on 7/10/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * GameViewController class. 
 * Provide GUI for the game
 * manage all movements in the game
 * manage game system (stage and such)
 * calculate transmission power of user
 * and DCT.
 *******************************************/

#import "GameViewController.h"

@implementation GameViewController

#define P(x,y) CGPointMake(x, y)

@synthesize sndControler, character, powerLayer, powerPercentage, stageLabel;
@synthesize tPowerArray, report, angelView, winPath;
@synthesize refresh, rightArrow, leftArrow, delegate;
@synthesize gameEnded;
@synthesize stepStrategyReport;
@synthesize makingStepwiseReport, initializingTree, stepwiseReportShown;;
@synthesize ck_UpButton, ck_RightButton, ck_DownButton, ck_leftButton;
@synthesize sql, report_sql;
@synthesize currentReport;
@synthesize addCircleButton, removeCircleButton;


@synthesize showStrategyButton;
@synthesize reportedStrategy;
@synthesize reqObj;


#pragma mark - Live methods


static GameViewController *instance;
+(GameViewController*)getInstance
{
    if(!instance)
        instance = [[GameViewController alloc] initWithNibName:nil bundle:nil];
    return instance;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Custom initialization
        [self setupView];
    }
    return self;
}

- (void)dealloc {

    

    
    if(lamp_light)
        lamp_light =nil;

    if(street_lamp)
        street_lamp = nil;

    if(gb)
        gb =nil;
    if(glh)
        glh = nil;
    if(grh)
        grh = nil;
    
//    if(ghost_array)
//        [ghost_array release];
    
    

    
    if(tPowerArray)
        [tPowerArray release];
    
    if(powerLayer)
        [powerLayer release];
    
    
    if(report)
        [report release];
    

    if(angelView)
        [angelView release];
    
    if(winPath)
        winPath = nil;
    
    if(sndControler)
        [sndControler release];
        sndControler = nil;
    
    for(int i=0; i< MAX_Node_Num; i++)
        {
            if(nodeImages[i])
                [nodeImages[i] release];
                nodeImages[i] = nil;
            
            if(ghostImages[i])
                [ghostImages[i] release];
                ghostImages[i] = nil;
            
        }
    for(int i =0; i< 40; i++)
    {
        if(fuel[i])
        {
            [fuel[i] release];
        }
    
    }
    if(character)
        [character release];
    
    if(refresh)
        [refresh release];
    
    if(rightArrow)
        [rightArrow release];
    
    if(leftArrow)
       [leftArrow release];
    
    if(sql)
        [sql release];
    
    if(report_sql)
        [report_sql release];
    
    if(reportedStrategy)
        [reportedStrategy release];
    
    if(currentReport)
        [currentReport release];
    
    if(ck_UpButton)
        [ck_UpButton release];
    
    if(ck_RightButton)
        [ck_RightButton release];
    if(ck_DownButton)
        [ck_DownButton release];
    if(ck_leftButton)
        [ck_leftButton release];

    if(addCircleButton)
        [addCircleButton release];
    
    if(removeCircleButton)
        [removeCircleButton release];
    
    
    if(stepStrategyReport) 
        [stepStrategyReport release];
    
    if(powerPercentage)
        [powerPercentage release];
        
    if(stageLabel)
        [stageLabel release];
    
    if(_gameTimer)
        _gameTimer = nil;
    
    if(showStrategyButton)
        [showStrategyButton release];
    
    if(reqObj)
        [reqObj release];
    

    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark View lifecycle

- (void)setupView {
    
    @try {
        showingReportDetail = NO;
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 335.0, 480.0)];
        self.view.tag = 1;
        UIColor *bg = [[UIColor alloc] initWithRed:0.2 green:0.2 blue:0.15 alpha:0.9];
        self.view.backgroundColor = bg;//[UIColor whiteColor];        

        [self.view setMultipleTouchEnabled:NO]; // enable multi touch
        self.view.multipleTouchEnabled = NO;
        self.sndControler = [[[SoundController alloc] init] autorelease];
        winningPercentage = 100.00;
        drawAnimating= NO;
        wonAnimating = NO;
        makingStepwiseReport = NO;
        stepwiseReportShown = NO;
        BOOL allCoverd = true;
        prevScale = -1;
        for(int i=0; i< MAX_Node_Num; i++){nodeImages[i] = NULL;ghostImages[i] = NULL;}        
        powerShown = NO;
        BOOL restoring = NO;
        sql = [[SQLManager alloc] init];
        int xarray[NoNodes];
        int yarray[NoNodes];
        self.powerLayer =[[TransmissionPower alloc] init];
        [self.view insertSubview:powerLayer atIndex:0];
        powerLayer.hidden = YES;
        tPowerArray = [[NSMutableArray alloc] init];

        if(treeID == 0)
        {
            actualStep = step =0;
            totalPower =0;
        } else {
            mostAdcancedStep = step;
            restoring = YES;
            [sql copyTree:xarray y:yarray tree:treeID];
        }
        
        street_lamp = [UIImage imageNamed:@"street_lamp.png"];
        lamp_light = [UIImage imageNamed:@"street_lamp_light.png"];
        
        [coveredNode retain];
        [uncoveredNode retain];
        
        gb = [UIImage imageNamed:@"ghost_body.png"];
        glh = [UIImage imageNamed:@"ghost_lh.png"];
        grh = [UIImage imageNamed:@"ghost_rh.png"];
        
       for(int i=0; i<MAX_Node_Num; i++)
        {
            nodeImages[i] = [[[NodesViewClass alloc] init] autorelease];
            ghostImages[i] = [[[EnemyView alloc] init] autorelease];
            ghostImages[i].delegate = self;
            [self.view addSubview:nodeImages[i]];
        }//end of for
        
        relyingNode = 0;//nodeList[0].id_number;//<= same         
        for(int i =0; i< MAX_Node_Num; i++)
        {
            [self.view addSubview:ghostImages[i]];
            if(i >= NoNodes)
            {
                nodeImages[i].hidden = YES;
                ghostImages[i].hidden = YES;
            }
        }
        
        self.character = [[CharacterView alloc] initWithFrame:CGRectMake(0, 0, 60.0, 60.0)];
        [self.view addSubview:character];
        [self.character makeMeTaller];
        
        
        if(restoring == NO)
        {    
            [self generateTree];
        }
        else
        {
            for(int i=0; i < NoNodes; i++)
            {
                
                ghostImages[i].center = nodeImages[i].center = CGPointMake((float)xarray[i], (float)yarray[i]);
                copy[i].id_number = nodeList[i].id_number = i;
                copy[i].parent_id =  nodeList[i].parent_id = -2;
                copy[i].power = nodeList[i].power =0;
                copy[i].x = nodeList[i].x = nodeImages[i].center.x;
                copy[i].y = nodeList[i].y = nodeImages[i].center.y;
                copy[i].inTree = nodeList[i].inTree = NO;
            }//end of for
            
            for(int i =0; i< NoNodes; i++)
            {
                for(int j=0; j<NoNodes; j++)
                {
                o_pMat[i][j] = pMat[i][j] = [self calcDistance:nodeList[i].x :nodeList[j].x :nodeList[i].y :nodeList[j].y];
                }//end of for j
            }//end of for i
            
            DCT *dctest = [[[DCT alloc] copyNodesOver:nodeList] autorelease];
            [dctest displayArea:NoNodes];
            dctTotalPower = [dctest constructDCT:NoNodes];

            o_dctTotalPower = dctTotalPower;
            if(stageNum != 0)
            dctTotalPower = (dctTotalPower * 0.15 * Difficulty) + dctTotalPower;

            struct nodeType dctAnswer[NoNodes];
            [dctest copyDctAnswer:dctAnswer];
            
            
            int rel, dest;
            for(int i =0; i< step; i++)
            {
                [sql restoreStep:i TreeID:treeID relyingNode:&rel destinationNode:&dest totalPower:&totalPower];
                
                double currentPower = o_pMat[rel][dest];

                self.powerLayer.center = nodeImages[rel].center;
                [self add_A_PowerLayer:currentPower];
                copy[rel].power = currentPower;
                for(int i =1; i< NoNodes; i++)
                {
                    if(o_pMat[rel][i] <= currentPower && copy[i].parent_id == -2)
                        copy[i].parent_id = rel;
                }//end of for
                
            }//end of for

            for(int i=0; i < NoNodes; i++)
            {
                nodeList[i].parent_id = copy[i].parent_id;
                nodeList[i].power = copy[i].power;
                
                if(i != 0 && nodeList[i].parent_id == -2)
                {
                 allCoverd = NO;
                }
                
                [self updateNodeAndGhost:i parent:nodeList[i].parent_id];
            }//end of for
            
            [self updateFuelBar:totalPower];
        }//end of if else
        
        copy[0].parent_id = nodeList[0].parent_id = -1;
        relyingNode = 0;
        if(nodeList[0].y < 240)
            character.center = CGPointMake(nodeList[0].x, nodeList[0].y + 20);//[self generateRandomPoint];
        else
            character.center = CGPointMake(nodeList[0].x, nodeList[0].y - 20);//[self generateRandomPoint];
        
        [self updateNodes];
        
        
        CGRect openButtonFrame = CGRectMake(0.0, 0.0, 40, 20);
        
        UIImage *leftImg = [UIImage imageNamed:@"left_gray.png"];
        
        self.leftArrow =  [[UIButton alloc] initWithFrame:openButtonFrame];
        self.leftArrow.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.leftArrow.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.leftArrow setBackgroundImage:leftImg forState:UIControlStateNormal];
        [self.leftArrow addTarget:self action:@selector(restorePreviousTree) forControlEvents:UIControlEventTouchDown];
        self.leftArrow.alpha = 0.8;
        
        
        UIImage *rightImg = [UIImage imageNamed:@"right_blue.png"];
        
        self.rightArrow =  [[UIButton alloc] initWithFrame:openButtonFrame];
        self.rightArrow.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.rightArrow.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.rightArrow setBackgroundImage:rightImg forState:UIControlStateNormal];
        [self.rightArrow addTarget:self action:@selector(restoreNextTree) forControlEvents:UIControlEventTouchDown];
        self.rightArrow.alpha = 0.8;

        
        
        UIImage *refreshImg = [UIImage imageNamed:@"Refresh_smaller.png"];	
        self.refresh =  [[UIButton alloc] initWithFrame:CGRectMake(0, 0, refreshImg.size.width-15, refreshImg.size.height-15)];
        self.refresh.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.refresh.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.refresh setBackgroundImage:refreshImg forState:UIControlStateNormal];
        [self.refresh addTarget:self action:@selector(refreshTree) forControlEvents:UIControlEventTouchDown];
        self.refresh.alpha = 0.8;

        
        self.powerPercentage = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)] autorelease];
        self.powerPercentage.font = [UIFont systemFontOfSize:15.0];
        self.powerPercentage.textAlignment = UITextAlignmentCenter;
        self.powerPercentage.textColor = [UIColor whiteColor];
        self.powerPercentage.backgroundColor = [UIColor clearColor];
        self.powerPercentage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        self.powerPercentage.text = [NSString stringWithFormat:@"%02.2f%%",((dctTotalPower-totalPower)/dctTotalPower)*100];
        
        
        self.stageLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)] autorelease];
        self.stageLabel.font = [UIFont systemFontOfSize:14.0];
        self.stageLabel.textAlignment = UITextAlignmentLeft;
        self.stageLabel.textColor = [UIColor whiteColor];
        self.stageLabel.backgroundColor = [UIColor clearColor];
        self.stageLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        
        [self updateStageNumber];

        
        
        self.showStrategyButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
        self.showStrategyButton.frame = CGRectMake(80, 5.0, 46, 26);
        self.showStrategyButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.showStrategyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.showStrategyButton.backgroundColor = [UIColor clearColor];
        [self.showStrategyButton setTitle:@"Strtgy" forState:UIControlStateNormal];
        [self.showStrategyButton addTarget:self action:@selector(showHideReportedStrategy) forControlEvents:UIControlEventTouchDown];
        [self.showStrategyButton release];
        
        
        UIView *barLayer = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 215, 30)] autorelease];
        [barLayer addSubview:self.stageLabel];
        stageLabel.center = CGPointMake(00, 15);
        [barLayer addSubview:self.powerPercentage];
        self.powerPercentage.center = CGPointMake(60, 15);

        [barLayer addSubview:leftArrow];
        leftArrow.center = CGPointMake(110, 15);
        [barLayer addSubview:rightArrow];
        rightArrow.center = CGPointMake(150, 15);
        [barLayer addSubview:refresh];
        refresh.center = CGPointMake(190, 15);
        [barLayer addSubview:self.showStrategyButton];
        showStrategyButton.center = CGPointMake(190, 15);    
        showStrategyButton.hidden = YES;
        
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:barLayer];
        [self navigationItem].rightBarButtonItem = barButton;
        [leftArrow release];//release local variable
        [rightArrow release];//release local variable        
        [refresh release];//release local variable
        
        
        UIImage *fl = [UIImage imageNamed:@"FuelBar.png"];
        UIImage *nfl = [UIImage imageNamed:@"FuelBar_red.png"];
        for(int i=0; i< 20; i++)
        {
            UIImageView *ful = [[UIImageView alloc] initWithImage:fl];
            fuel[i] = ful;
            [self.view addSubview:fuel[i]];
            fuel[i].center = CGPointMake(300.0,250+7.5*i);
            fuel[i].alpha = 0.6;
            [ful release];
        }
        
        for(int i=20; i<32; i++)
        {
            UIImageView *ful = [[UIImageView alloc] initWithImage:nfl];
            fuel[i] = ful;
            [self.view addSubview:fuel[i]];
            fuel[i].center = CGPointMake(300.0,250+7.5*i);
            fuel[i].alpha = 0.6;
            fuel[i].hidden = YES;
            [ful release];            
        }
        UIImageView *bar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"EmptyFuelBar.png"]];
        [self.view addSubview:bar];
        bar.center = CGPointMake(300, 322);
        
        
        self.angelView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dead.png"]]autorelease];
        [self.view addSubview:self.angelView];
        self.angelView.alpha = 0.0;
        self.winPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 280, 380)];        

        CGRect ckButtonFrame = CGRectMake(0.0, 0.0, 30, 50);
        self.ck_UpButton =  [[UIButton alloc] initWithFrame:ckButtonFrame];
        self.ck_UpButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.ck_UpButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.ck_UpButton setBackgroundImage:[UIImage imageNamed:@"u_green.png"] forState:UIControlStateNormal];
        [self.ck_UpButton addTarget:self action:@selector(moveOneToTheDirection:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.ck_UpButton];
        self.ck_UpButton.center = CGPointMake(260, 20);
        self.ck_UpButton.tag = CK_Up;
        self.ck_UpButton.alpha = 0.8;

        self.ck_DownButton =  [[UIButton alloc] initWithFrame:ckButtonFrame];
        self.ck_DownButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.ck_DownButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.ck_DownButton setBackgroundImage:[UIImage imageNamed:@"d_green.png"] forState:UIControlStateNormal];
        [self.ck_DownButton addTarget:self action:@selector(moveOneToTheDirection:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.ck_DownButton];
        self.ck_DownButton.center = CGPointMake(260, 90);
        self.ck_DownButton.tag = CK_Down;
        self.ck_DownButton.alpha = 0.8;

        ckButtonFrame = CGRectMake(0.0, 0.0,50.0, 30);
        self.ck_RightButton =  [[UIButton alloc] initWithFrame:ckButtonFrame];
        self.ck_RightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.ck_RightButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.ck_RightButton setBackgroundImage:[UIImage imageNamed:@"r_green.png"] forState:UIControlStateNormal];
        [self.ck_RightButton addTarget:self action:@selector(moveOneToTheDirection:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.ck_RightButton];
        self.ck_RightButton.center = CGPointMake(300, 55);
        self.ck_RightButton.tag = CK_Right;
        self.ck_RightButton.alpha = 0.8;
        
        self.ck_leftButton = [[UIButton alloc] initWithFrame:ckButtonFrame];
        self.ck_leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.ck_leftButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.ck_leftButton setBackgroundImage:[UIImage imageNamed:@"l_green.png"] forState:UIControlStateNormal];
        [self.ck_leftButton addTarget:self action:@selector(moveOneToTheDirection:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.ck_leftButton];
        self.ck_leftButton.center = CGPointMake(220, 55);
        self.ck_leftButton.tag = CK_Left;
        self.ck_leftButton.alpha = 0.8;
        


        
        CGRect arButtonFrame = CGRectMake(0.0, 0.0, 40, 40);        
        self.addCircleButton = [[UIButton alloc] initWithFrame:arButtonFrame];
        self.addCircleButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.addCircleButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.addCircleButton setBackgroundImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
        [self.addCircleButton addTarget:self action:@selector(addOrRemove_A_Circle:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.addCircleButton];
        self.addCircleButton.center = CGPointMake(220, 155);
        self.addCircleButton.tag = AR_Add;
        self.addCircleButton.alpha = 0.8;
    
        self.removeCircleButton = [[UIButton alloc] initWithFrame:arButtonFrame];
        self.removeCircleButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.removeCircleButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.removeCircleButton setBackgroundImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
        [self.removeCircleButton addTarget:self action:@selector(addOrRemove_A_Circle:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:self.removeCircleButton];
        self.removeCircleButton.center = CGPointMake(290, 155);
        self.removeCircleButton.tag = AR_Remove;
        self.removeCircleButton.alpha = 0.8;
        
        
        [self showHideCrossKeys];
        
        
        CGRect textframe = CGRectMake(0, 0.0, 320, 100);        
        self.reportedStrategy = [[UITextView alloc] initWithFrame:textframe];
        self.reportedStrategy.scrollEnabled = YES;
        self.reportedStrategy.editable = NO;
        self.reportedStrategy.textColor = [UIColor blackColor];
        self.reportedStrategy.font = [UIFont systemFontOfSize:17.0];
        self.reportedStrategy.backgroundColor = [UIColor whiteColor];
        self.reportedStrategy.text =@"Why did you choose this route?";
        self.reportedStrategy.autocorrectionType = UITextAutocorrectionTypeNo;
        self.reportedStrategy.keyboardType = UIKeyboardTypeDefault;
        self.reportedStrategy.returnKeyType = UIReturnKeyDone;
        [self.reportedStrategy setAccessibilityLabel:NSLocalizedString(@"reoprtedStrategy", @"")];
        [self.view addSubview:self.reportedStrategy];
        self.reportedStrategy.hidden = YES;
        self.reportedStrategy.center = CGPointMake(160, 40);
        
        
            if(allCoverd /*&& totalPower != 0*/)
            {
            gameEnded = YES;
            }
            else 
            {
            gameEnded = NO;
            }
        
        }
    @catch (NSException *exception) {
        
        [self deleteTree];
        treeID =0;
        NSUserDefaults *userDefault;
        [userDefault setObject:[NSNumber numberWithInt:treeID] forKey:@"treeID"];
        exit(1);
    }
    @finally {
        NSLog(@"Finally");
    }
    

    
}


-(void)viewWillAppear:(BOOL)animated
{
}

- (void)viewDidUnload
{
    if(lamp_light)
        lamp_light =nil;
    
    if(street_lamp)
        street_lamp = nil;
    
    if(gb)
        gb =nil;
    
    if(glh)
        glh = nil;
    
    if(grh)
        grh = nil;    
    
    if(tPowerArray)
        [tPowerArray release];
    
    if(powerLayer)
        [powerLayer release];
    
    
    if(report)
        [report release];
    
    
    if(angelView)
        [angelView release];
    
    if(winPath)
        winPath = nil;
    
    //[winPath release];
    
    if(sndControler)
        [sndControler release];
    
    sndControler = nil;
    
    for(int i=0; i< MAX_Node_Num; i++)
    {
        if(nodeImages[i])
            [nodeImages[i] release];
        nodeImages[i] = nil;
        
        if(ghostImages[i])
            [ghostImages[i] release];
        ghostImages[i] = nil;
        
    }
    if(character)
        [character release];
    
    if(refresh)
        [refresh release];
    
    if(rightArrow)
        [rightArrow release];
    
    if(leftArrow)
        [leftArrow release];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)shownGhostCheck
{
    for(int i=0; i< NoNodes; i++)
    {
        if(ghostImages[i].hidden == NO)
        {
            if(ghostImages[i].animating)
            [ghostImages[i] moveGhost];
            else if(ghostImages[i].applause_animating)
            [ghostImages[i] applause];
        }
    }


}
#pragma mark Utility Methods

-(void)stageCleared
{
    NSString *statement = [NSString stringWithFormat:@"UPDATE STAGE SET STAGE_CLEARED=1, Stage_ClearedScore=MAX(%f,Stage_ClearedScore) , Stage_TreeID=%d WHERE Stage_ID=%d", (100.00 - (totalPower/o_dctTotalPower)*100.00f), treeID, stageNum];
    [sql query:statement];
    [[self delegate] stageIsUpdated:self];
}//end of stageCleared

-(BOOL)isGameProgress
{
    BOOL someCovered = false;

    for(int i=1; i< NoNodes; i++)
    {
        if(nodeList[i].parent_id != -2)
            someCovered = true;
    }
    
    return someCovered;
}

-(void)initRandomSeed
{
    if(stageNum < 0)
        srand(time(NULL));
    else
        srand(stageNum+1);
}

-(BOOL)duplicatedPointCheck:(CGPoint)point
{
    for(int i =0; i<NoNodes; i++)
    {
    if(nodeImages[i].center.x == point.x && nodeImages[i].center.y == point.y)
        return true;
        
    }
    
    return false;
}

-(CGPoint)generateRandomPoint
{
    CGPoint p = CGPointMake(rand() % 320, rand() % 420);
    while ([self duplicatedPointCheck:p] == YES) {
        p = CGPointMake(rand() % 320, rand() % 420);
    }
    return p;
}

-(double)calcDistance:(float)inX :(float)outX :(float)inY :(float)outY
{return (inX - outX)*(inX - outX) + (inY - outY)*(inY - outY);}//end of calcDistance

-(double)getScaleFromPower:(double)power
{
    return (sqrt(power))/250;
}
-(double)calcScale:(float)inX :(float)outX :(float)inY :(float)outY
{
    return (sqrt((inX - outX)*(inX - outX) + (inY - outY)*(inY - outY)))/250;
}



-(void)generateTree
{
    
    gameEnded = NO;
    refreshConfirmed = NO;
    actualStep = step =0;
    //    NoNodes = 10;
    totalPower =0;
    //prevTreeCovereage = treeCoverage = 0;
    
    [self removePowerLayers];
    NoNodes = NextTree_NoNodes;
    
    curStageNum = stageNum;
    if(curStageNum - 1 > lastClearedStage)
        lastClearedStage = curStageNum - 1;
    
    
    for(int i=0; i< NoNodes; i++)
    {
        ghostImages[i].center = nodeImages[i].center = [self generateRandomPoint];
        copy[i].id_number = nodeList[i].id_number = i;
        copy[i].parent_id =  nodeList[i].parent_id = -2;
        copy[i].power = nodeList[i].power =0;
        copy[i].x = nodeList[i].x = nodeImages[i].center.x;
        copy[i].y = nodeList[i].y = nodeImages[i].center.y;
        copy[i].inTree = nodeList[i].inTree = NO;
        ghostImages[i].hidden = nodeImages[i].hidden = NO;
    }//end of for
    
    if(NoNodes< MAX_Node_Num)
    {
        for(int i = NoNodes; i< MAX_Node_Num; i++)
        {
            nodeImages[i].hidden = ghostImages[i].hidden = YES;
        }//end of for
    }//end of if
    
    for(int i =0; i< NoNodes; i++)
    {
        for(int j=0; j<NoNodes; j++)
        {
            o_pMat[i][j] = pMat[i][j] = [self calcDistance:nodeList[i].x :nodeList[j].x :nodeList[i].y :nodeList[j].y];
        }//end of for j
    }//end of for i
    
    
    DCT *dctest = [[[DCT alloc] copyNodesOver:nodeList] autorelease];
    
    [dctest displayArea:NoNodes];
    dctTotalPower = [dctest constructDCT:NoNodes];
    o_dctTotalPower = dctTotalPower;
    dctTotalPower = (dctTotalPower * (0.15 * Difficulty)) + dctTotalPower;
    struct nodeType dctAnswer[NoNodes];
    [dctest copyDctAnswer:dctAnswer];

    treeID = [sql createTree:stageNum];
    NSUserDefaults	*userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSNumber numberWithInt:treeID] forKey:@"treeID"];
	
    
    NSString *dctTreeStatement = @"";
    for(int i=0; i< NoNodes; i++)
    {
        dctTreeStatement = [NSString stringWithFormat:@"%@ INSERT INTO DCTNodeList (DCTNL_NodeID, DCTNL_ParentID, DCTNL_Power, DCTNL_Tree_ID) VALUES (%d, %d, %f, %d);", dctTreeStatement, dctAnswer[i].id_number, dctAnswer[i].parent_id, dctAnswer[i].power, treeID];
    }//end of for
    
    [sql query:dctTreeStatement];
    
    
    NSString *nodeListStatement = @"";
    for(int i=0; i< NoNodes; i++)
    {
        NSString *appendString = [NSString stringWithFormat: @"INSERT INTO NodeList(NL_NodeID, NL_X , NL_Y, NL_Tree_ID) VALUES (%d, %d, %d, %d);", i, nodeList[i].x, nodeList[i].y, treeID];
        nodeListStatement = [nodeListStatement stringByAppendingString:appendString];
        //NSString *test = [myString stringByAppendingString:@" is just a test"];
    }//end of for
    [sql query:nodeListStatement];
    
    
    copy[0].parent_id = nodeList[0].parent_id = -1;
    relyingNode = 0;
    if(nodeList[0].y < 240)
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y + 20);//[self generateRandomPoint];
    else
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y - 20);//[self generateRandomPoint];    
    
    [self updateNodes];
    
    if(self.character.killed)
    {
        [self.character reviveAnimation];
        [sndControler playSoundEffect:ReviveSound];
    }
 
}

-(void)deleteTree
{
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
//    dispatch_async(queue, ^(void) { 

    //Most likely this is causing report to be blank. 
    //This method is banned until better solution is found. 
    /*
    if([sql isPending:treeID] == NO && sendingReport == NO)
    {
    NSString *statement=@"";
        statement = [NSString stringWithFormat:@"%@DELETE FROM Tree WHERE Tree_ID=%d;",statement,treeID];
        statement = [NSString stringWithFormat:@"%@DELETE FROM UserLog WHERE UL_Tree_ID=%d;DELETE FROM NodeList WHERE NL_Tree_ID=%d;DELETE FROM DCTNodeList WHERE DCTNL_Tree_ID=%d;DELETE FROM UserNodeList Where UNL_Tree_ID=%d;",statement, treeID,treeID,treeID, treeID];
        
    
    [sql query:statement];
        
        //NSLog(@"deleted?");    
    }   
    */
//});    
}



-(void)game_won{

    
    if(totalPower < o_dctTotalPower)
    {
        if(stageNum > 0)
        starNum += 10;
        
        [self.sndControler playSoundEffect:AchievementSound];
        [self game_won_animation];
        
        NSString *statement=@"";
        for(int i =0; i< NoNodes; i++)
        {
            statement = [NSString stringWithFormat:@"%@ INSERT INTO UserNodeList (UNL_NodeID, UNL_ParentID, UNL_Power, UNL_Tree_ID) VALUES (%d, %d, %f, %d);",statement, nodeList[i].id_number, nodeList[i].parent_id, nodeList[i].power, treeID];
        }//end of for
        
        statement = [NSString stringWithFormat:@"%@ UPDATE Tree SET Tree_ClearedScore=%f WHERE TREE_ID=%d;",statement,(100.00 - (totalPower/o_dctTotalPower)*100.00f), treeID ];               
        [sql query:statement];
        
            wonAnimating = YES;
        
    }
    else
    {
        if(stageNum-1 > lastClearedStage)
        starNum += 1;
        
        [self.sndControler playSoundEffect:WinningSound];  
        [self game_draw_animation];

        NSString* statement =@"";
        statement = [NSString stringWithFormat:@"%@ UPDATE Tree SET Tree_ClearedScore=%f WHERE TREE_ID=%d;",statement,(100.00 - (totalPower/o_dctTotalPower)*100.00f), treeID ];               
        [sql query:statement];
        
        drawAnimating = YES;
        wonAnimating = YES;
    }
    [self computeScoreAndReportIt];
}


-(void)game_won_alert
{
        [self gameEndedAlert:@"Amazing!! You win!!! We would love to see how you did it!!! Would you be able to submit your game log for our research?" TAG:GameWin];
    
}//end of game_won_alert

-(void)game_lost_alert
{
    [self gameEndedAlert:@"You lose...." TAG:GameLose];
    
}//end of game_won_alert

-(void)game_draw_alert
{
    [self gameEndedAlert:@"Stage Clear!!" TAG:GameDraw];
}//end of game_won_alert

-(void)done_reporting_alert
{
    if(stageNum >= 0)
    stageNum ++;
    
    [self.sndControler playSoundEffect:RefreshSound];
    [self renewTheGame];
}

-(void)game_draw
{
    
    
    drawAnimating = YES;
    [self game_draw_animation];

    NSString* statement =@"";
    statement = [NSString stringWithFormat:@"%@ UPDATE Tree SET Tree_ClearedScore=%f WHERE TREE_ID=%d;",statement,(100.00 - (totalPower/o_dctTotalPower)*100.00f), treeID ];   
    
    [sql query:statement];

    
}//end of game_draw

-(void)game_lost
{

    NSString* statement=[NSString stringWithFormat:@"SELECT LoseCounter From Tree WHERE Tree_ID=%d", treeID];
    int loseCounter = [self.sql getInteger:statement];
    loseCounter++;
    statement = [NSString stringWithFormat:@"UPDATE Tree SET LoseCounter=%d WHERE TREE_ID=%d", loseCounter, treeID];
    [self.sql query:statement];
    
    _gameTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(turnOffLights) 
            userInfo:nil repeats:NO];
    NSTimer *_timer, *_timer2, *_timer3, *_timer4, *_timer5, *_timer6, *_timer7;
    _timer= [NSTimer scheduledTimerWithTimeInterval:0.18 target:self selector:@selector(turnOnLights) userInfo:nil repeats:NO];
    _timer2= [NSTimer scheduledTimerWithTimeInterval:0.85 target:self selector:@selector(turnOffLights) userInfo:nil repeats:NO];
    _timer3= [NSTimer scheduledTimerWithTimeInterval:0.95 target:self selector:@selector(turnOnLights) userInfo:nil repeats:NO];    

    _timer4= [NSTimer scheduledTimerWithTimeInterval:1.50 target:self selector:@selector(turnOffLights) userInfo:nil repeats:NO];
    _timer5= [NSTimer scheduledTimerWithTimeInterval:1.55 target:self selector:@selector(turnOnLights) userInfo:nil repeats:NO];    
    _timer6= [NSTimer scheduledTimerWithTimeInterval:1.60 target:self selector:@selector(turnOffLights) userInfo:nil repeats:NO];
    _timer7= [NSTimer scheduledTimerWithTimeInterval:1.00 target:self selector:@selector(moveGhost) userInfo:nil repeats:NO]; 
}//end of game_lost



-(void)gameEnded:(enum GameResult)tag
{
    powerShown = NO;
    if(tag != GameLose)
    {
        if(!retrying)
        consecutiveWinCounter++;
        
        
        switch (characterLevel) {
            case 0:
                if(consecutiveWinCounter >= 10)
                    {
                    characterLevel =1;//grown up, get a horse
                    consecutiveWinCounter =0;
                    }
                break;
            case 1:
                if(consecutiveWinCounter >= 15)
                    {
                    characterLevel = 2;//horse grown up, ride on it
                    consecutiveWinCounter =0;
                    }
                break;
            case 2:
                if(consecutiveWinCounter >= 20 )
                    {//get armed
                    characterLevel = 3;//can't go anything higher than this
                    }
                break;
            default:
                break;
        }
        

        if(stageNum >= 0)
        {
        [self stageCleared];
        }
        consecutiveLoseCounter=0;
    }
    else
    {
        consecutiveWinCounter = 0;
        consecutiveLoseCounter++;
        if(consecutiveLoseCounter== 5 && Difficulty==VeryHard)
        {
            UIAlertView *alert;
                alert = [[UIAlertView alloc] initWithTitle:nil message:@"FYI: You can change the difficulty level in Game Configuration Menu." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];      
            
            [alert show];
            [alert release];
        }
    }
    
    switch (tag) {
        case GameWin://win
            [self game_won];
            break;//end of GameWin
        case GameDraw://draw
            [self computeScoreAndReportIt];
            if(Difficulty == VeryHard)
            {
                starNum += 1;
                [self.sndControler playSoundEffect:WinningSound];   
            }
            else
                [self.sndControler playSoundEffect:LosingSound];
                        
            [self game_draw];
            break;//end of GamwDraw
        case GameLose://lose
            [self.sndControler playSoundEffect:LosingSound];            
            [self game_lost];
            
            [self.character removeFromSuperview];
            [self.view insertSubview:self.character belowSubview:ghostImages[0]];
            
            break;//end of GameLose
        default:
            break;
    }


    if(o_dctTotalPower == totalPower)
        tag = GameDraw;
    else if(o_dctTotalPower < totalPower)
        tag = GameLose;

    [self anonymousReportProcedure:tag];
}

-(void)anonymousReportProcedure:(enum GameResult)tag
{
    
    
    AnonymousReportItem *anItem = [[[AnonymousReportItem alloc] init] autorelease];
    //self.reqObj
    anItem.stageNum = stageNum;
    anItem.stepNum = step;
    anItem.indicatorFlag= tag;
    NSLog(@"step %d      stage %d", anItem.stepNum, anItem.stageNum);
    
    if(!self.reqObj)
        self.reqObj = [[SubmissionRequestor alloc] init] ;    
    if(autoReporting)
    [self.reqObj submitAnonymousReport:anItem];

}


-(void)gameEndedAlert:(NSString *)message TAG:(enum GameResult)tag
{
    if(tag == GameLose)
    {
        UIAlertView *alert;
        if(stageNum != -1)
        {
            alert = [[UIAlertView alloc] initWithTitle:nil message:message
                                              delegate:self cancelButtonTitle:@"Quit" otherButtonTitles:@"Retry", nil];      
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:nil message:message
                                              delegate:self cancelButtonTitle:@"Quit" otherButtonTitles:@"Retry", nil];   
        }
        alert.tag = Alert_GameEndedConfirmation_Lost;
        [alert show];
        [alert release];
        
    }
    else if(tag == GameDraw)
    {
        UIAlertView *alert;
        if(stageNum != -1)
        {
            alert = [[UIAlertView alloc] initWithTitle:nil message:message
                     delegate:self cancelButtonTitle:@"Quit" otherButtonTitles:@"Next Stage", @"Retry", nil];      
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:nil message:message
                                              delegate:self cancelButtonTitle:@"Quit" otherButtonTitles:@"New Game", @"Retry", nil];   
        }
        alert.tag = Alert_GameEndedConfirmation;
        [alert show];
        [alert release];
    }
    else if(tag == GameWin)
    {
        UIAlertView *alert;
        if(stageNum != -1)
        {
            alert = [[UIAlertView alloc] initWithTitle:nil message:message
                    delegate:self cancelButtonTitle:@"Quit" otherButtonTitles:@"Next Stage",@"Overall",@"Stepwise" , nil];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:nil message:message
                    delegate:self cancelButtonTitle:@"Quit" otherButtonTitles:@"New Game", @"Overall",@"Stepwise" , nil];

        }
        
        if(autoReporting)
        {
            if(!self.reqObj)
                self.reqObj = [[SubmissionRequestor alloc] init] ;
        
            [self.reqObj submitStrategy:treeID strategy:@"" score:winningPercentage stepNumber:actualStep stg:stageNum rptType:RegularStepSubmission];
        }
        alert.tag = Alert_WonDCTConfirmation;
        [alert show];
        [alert release];
    }
}//end of gameEndedAlert


-(void)renewTheGame
{
    mostAdcancedStep =0;
    if(stageNum != -1)
    [[self delegate] stageWillRenew:self];
    
    
    if(stageNum != -1 )
    {
        //stageNum++;

        [self updateStageNumber];
    }
        [self initRandomSeed];
    
        
    [self deleteTree];
    [self generateTree];
    [self updateFuelBar:totalPower];
}//renewTheGame


-(void)restartTheGame
{
    mostAdcancedStep=0;
    refreshConfirmed = NO;
    gameEnded = NO;
    actualStep = step =0;
    totalPower =0;
    
    [self removePowerLayers];
    
    for(int i=0; i< NoNodes; i++)
    {
        ghostImages[i].center = nodeImages[i].center;
        copy[i].parent_id =  nodeList[i].parent_id = -2;
        copy[i].power = nodeList[i].power =0;
        copy[i].inTree = nodeList[i].inTree = NO;
    }//end of for
    NSString *statement = [NSString stringWithFormat: @"DELETE FROM UserLog WHERE UL_Tree_ID=%d",treeID];
    [sql query:statement];
    
    
    copy[0].parent_id = nodeList[0].parent_id = -1;
    relyingNode = 0;
    if(nodeList[0].y < 240)
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y + 20);//[self generateRandomPoint];
    else
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y - 20);//[self generateRandomPoint];    
    if(self.character.killed)
    {
        [self.character reviveAnimation];
        [sndControler playSoundEffect:ReviveSound];
    }

    [self updateNodes];
    [self updateFuelBar:totalPower];
    
}//end of renewTheGame


-(int)getGhostNumberForStage
{
    if(stageNum == 0)
    {
        return 3;
    }
    else if(stageNum > 0 && stageNum < 5)
    {
        return 5;
    }
    else if( stageNum < 10)
    {
        return stageNum + 1;
    }
    else if(stageNum < 20)
    {
        return 10;
    }
    else if (stageNum < 50)
    {
        return 15;
    }
    else if(stageNum < 85)
    {
        return 20;
    }
    else if(stageNum < 95)
    {
        return 25;
    }
    else if(stageNum < 100)
    {
        return 30;
    }    

    return 0;

}
-(void)computeScoreAndReportIt
{


    BOOL clearedStage[100];
    double clearedScore[100];
    [sql copyStageResult:clearedStage score:clearedScore ];    
    int score =0;
    int completionCount=0;
    BOOL oneThousandReported =false;
    for(int i =0; i< 100; i++)
    {
        if(clearedStage[i])
        {
            score++;
            if(clearedScore[i] >= 0)
                score +=2;
            if(clearedScore[i]> 0)
            {   
                if(!oneThousandReported)
                {
                [self reportAchievementIdentifier:@"1000" percentComplete:100];
                    oneThousandReported = YES;
                }
                
                score+= 5;
            }   
            completionCount += [self getGhostNumberForStage];
        }
    }

    [self reportScore:score forCategory:@"1001"];
    [self reportScore:completionCount forCategory:@"1002"];
    
}






#pragma mark GUI update methods
-(void)updateStageNumber
{
    if(stageNum == -1)
        self.stageLabel.text = @"Rndm";
    else
        self.stageLabel.text = [NSString stringWithFormat:@"Stage.%d",stageNum + 1];
    
}

-(void)modifyViewFor:(enum GameMode)tag
{
    
    if(tag == GameMode_Random)
    {
        if(!showingReportDetail)
        self.refresh.hidden = NO;
        
    }else
    {
        
        self.refresh.hidden = YES;
    }

}
-(void)stopApplause
{
    for(int i =0; i< NoNodes; i++)
    {
        [ghostImages[i] setPosition:DefaultPosition];
    }//end of for
}//end of stopApplause



-(void)updateFuelBar:(double)currentPower
{
    
    int hideNum = (int)(currentPower/(dctTotalPower/20));
    if(hideNum>40)
        hideNum = 40;
    
    for(int j=0; j< 20; j++)
    {
        fuel[j].hidden = NO;
    }//end of for
    
    for(int j=20; j< 32; j++)
    {
        fuel[j].hidden = YES;
    }//end of for
    
    for(int j=0; j< hideNum; j++)
    {
        fuel[j].hidden=YES; 
        if(j >= 20)
            fuel[j].hidden = NO;
        
    }//end of for
            
    self.powerPercentage.text = [NSString stringWithFormat:@"%02.2f %%",((dctTotalPower-currentPower)/o_dctTotalPower)*100];
}


-(void)add_A_PowerLayer:(double)power
{
    double scale = [self getScaleFromPower:power];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.500];
    [UIView setAnimationDelegate:self];
    CGAffineTransform transform1 = CGAffineTransformMakeScale(prevScale, prevScale);
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    powerLayer.transform = transform1;
    powerLayer.transform = transform;
    [UIView commitAnimations];
    prevScale = scale;
    
    powerLayer.hidden = NO;
    [tPowerArray addObject:powerLayer];
    TransmissionPower *newPower=[[TransmissionPower alloc] init];
    powerLayer = newPower;
    [newPower retain];
    [self.view insertSubview:powerLayer atIndex:0];
    powerLayer.hidden = YES;
}//end of add_A_PowerLayer

-(void)removePowerLayers
{
    self.powerLayer.hidden = YES;
    while([tPowerArray count] > 0)
    {
        [self.powerLayer removeFromSuperview];
        [self.powerLayer release];
        self.powerLayer = nil;
        self.powerLayer = [tPowerArray objectAtIndex:[tPowerArray count] - 1];
        self.powerLayer.hidden = YES;
        [tPowerArray removeObjectAtIndex:[tPowerArray count] -1];
    }//end of while
    
    
}//end of removePowerLayers


-(void)updateNodes
{
    for(int i=0; i< NoNodes/*NoNodes*/; i++)
    {
        [self updateNodeAndGhost:i parent:nodeList[i].parent_id];
        
    }//end of for
    
}//end of updateNodes

-(void)updateNodeAndGhost:(int)selfid parent:(int)parent_id
{
    [nodeImages[selfid] updateImage:parent_id];
    if(parent_id > -2)
    {
        
        ghostImages[selfid].hidden = YES;
        ghostImages[selfid].animating = NO;
        
    }
    else
    {
        ghostImages[selfid].alpha = 1.0;
        
        ghostImages[selfid].animating = YES;
        ghostImages[selfid].hidden = NO;
        [ghostImages[selfid] hideGhost:NO];
        [ghostImages[selfid] moveGhost];
        
    }
}


-(void)turnOnLights{
    UIImageView *layer;
    for(int i=0; i< [tPowerArray count]; i++)
    {
        layer = [tPowerArray objectAtIndex:i];
        layer.hidden = NO;
    }//end of while
}
-(void)turnOffLights{
    UIImageView *layer;
    for(int i=0; i< [tPowerArray count]; i++)
    {
        layer = [tPowerArray objectAtIndex:i];
        layer.hidden = YES;
    }//end of while
    
}

#pragma mark Stepwise Strategy Report
-(void)show_StepwiseReport
{
    
if(!self.stepStrategyReport)
    {
        self.stepStrategyReport = [[StepStrategyReport alloc] initWithNibName:nil bundle:nil];
        self.stepStrategyReport.delegate = self;
    }
    makingStepwiseReport = YES;
    
    
    self.refresh.hidden = YES;    
    NSLog(@"  step %d     most advanced %d", step, mostAdcancedStep);
    int stepnum = step;
    
    initializingTree = YES;
    for(int i=0; i<= stepnum; i++)
    {
        [self restorePreviousTree];
    }
    initializingTree = NO;
    [self.view addSubview:self.stepStrategyReport.view];
    self.stepwiseReportShown =YES;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please use gray and blue arrow to go back and forth\n\n\n" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    UIImageView *next = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_blue.png"]] autorelease];
    [alert addSubview:next];
    next.center = CGPointMake(180, 100);
    UIImageView *prev = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"left_gray.png"]] autorelease];
    [alert addSubview:prev];
    prev.center = CGPointMake(100, 100);
    
    [alert show];
    [alert release];
  
    [self.stepStrategyReport updateLabel:0 output:mostAdcancedStep];
}

#pragma mark Animation methods

-(void)moveCharacterTo:(CGPoint)location time:(float)duration;
{
    if(!initializingTree)
    [sndControler playSoundEffect:ChangeRelyingNodeSound];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    character.center = location;
    [UIView commitAnimations];
    
    if(!initializingTree)
    [character moveAnimation:1.0];
    
}//end of moveCharacterTo


-(void)game_won_animation{
    

    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = YES;
    
	pathAnimation.path = self.winPath.CGPath;    
    pathAnimation.duration = 3.0;
    pathAnimation.repeatCount = 2;
    pathAnimation.delegate = self;
    [self.character.layer addAnimation:pathAnimation forKey:@"won_animation"];    
    self.character.center = P(280.0f, 220.0f);
    [self.character moveAnimation:6.0];
    
    
    NSTimer *_timer;//, *_timer2, *_timer3, *_timer4, *_timer5;
    win_ghostIndex=0;
    
    for(int i =0; i< NoNodes; i++)
    {
        ghostImages[i].ghostBody.hidden = NO;
        
        _timer= [NSTimer scheduledTimerWithTimeInterval:/*0.2*/(3.0/NoNodes)*(i+1) target:self selector:@selector(game_won_ghostAnimation) userInfo:nil repeats:NO];
        
    }
    
}//end of game_won_animation


-(void)game_won_ghostAnimation
{
    wonAnimating = YES;
    
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = YES;
    
	pathAnimation.path = self.winPath.CGPath;    
    pathAnimation.duration = 4.0 - (0.2 * (win_ghostIndex + 1));
    pathAnimation.repeatCount = 3;
    pathAnimation.delegate = self;
    ghostImages[win_ghostIndex].alpha=1.0;
    ghostImages[win_ghostIndex].hidden = NO;
    ghostImages[win_ghostIndex].animating = YES;
    [ghostImages[win_ghostIndex].layer addAnimation:pathAnimation forKey:@"won_animation"];    
    self.character.center = P(280.0f, 220.0f);
    
    win_ghostIndex++;
    
}


-(void)game_draw_animation
{
    
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = YES;
    
	UIBezierPath *trackPath = [UIBezierPath bezierPath];
    
    srandom(time(NULL));
	[trackPath moveToPoint:P(self.character.center.x, self.character.center.y)];
	[trackPath addCurveToPoint:P(random() % 20, random() % 152)
				 controlPoint1:P(90, 86)
				 controlPoint2:P(20, 170)];
    
	[trackPath addCurveToPoint:P(random() % 160, random() % 352)
				 controlPoint1:P(90, 286)
				 controlPoint2:P(160, 220)];
    
	[trackPath addCurveToPoint:P(300, 286)
				 controlPoint1:P(160, 352)
				 controlPoint2:P(300, 420)];
    
	[trackPath addCurveToPoint:P(random() % 160, random() % 86)
				 controlPoint1:P(210, 152)
				 controlPoint2:P(160, 220)];
    
	[trackPath addCurveToPoint:P(160, 220)
				 controlPoint1:P(160, -80)
				 controlPoint2:P(160, 20)];

    
	pathAnimation.path = trackPath.CGPath;    
    pathAnimation.duration = 5.0;
    pathAnimation.delegate = self;
    [self.character.layer addAnimation:pathAnimation forKey:@"draw_animation"];    
    self.character.center = P(160.0f, 220.0f);
    [self.character moveAnimation:5.0];
    
}//end of game_draw_animation



-(void)moveGhost
{   
    [self.character horseFadeout];
    [self.character moveAnimation:10.0]; 
    
    CGFloat xOffset, yOffset;
    [UIView beginAnimations:@"MoveGhostPart1" context:NULL];
    [UIView setAnimationDuration:2.5];
    for(int i =0; i< NoNodes; i++)
    {
        [self updateNodeAndGhost:i parent:-2];
        xOffset = character.center.x - nodeImages[i].center.x;
        yOffset = character.center.y - nodeImages[i].center.y;
        if(yOffset*yOffset > xOffset*xOffset)
        {
            if(yOffset > 0)
        {
        float ratio = (480.0f - nodeImages[i].center.y)/yOffset;
        
        ghostEndLocation[i] = CGPointMake(nodeImages[i].center.x + xOffset * ratio + 15, nodeImages[i].center.y + yOffset * ratio);
        } 
            else
        {
        float ratio = (nodeImages[i].center.y)/yOffset;
        ghostEndLocation[i] = CGPointMake(nodeImages[i].center.x - xOffset * ratio, nodeImages[i].center.y - yOffset * ratio);
            }
        } else if(yOffset*yOffset < xOffset*xOffset)
        {
            if(xOffset > 0)
            {
            float ratio = (320.0f - nodeImages[i].center.x)/xOffset;
            ghostEndLocation[i] = CGPointMake(nodeImages[i].center.x + xOffset * ratio + 15, nodeImages[i].center.y + yOffset * ratio);
            } else
            {
                float ratio = (nodeImages[i].center.x)/xOffset;
                ghostEndLocation[i] = CGPointMake(nodeImages[i].center.x - xOffset * ratio, nodeImages[i].center.y - yOffset * ratio);
            
            }
        }
        else
        {
            ghostEndLocation[i] = CGPointMake(320, 480);
        }

        ghostImages[i].center = character.center;//location[i];//character.center;
    }//end of for

    [UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];    

}//end of moveGhost



-(void)moveGhostPt2
{
    [UIView beginAnimations:@"MoveGhostPart_2" context:NULL];
    [UIView setAnimationDuration:0.5];
    for(int i =0; i< NoNodes; i++)
    {
        ghostImages[i].center = ghostEndLocation[i];
        ghostImages[i].alpha = 0.00;
    }//end of for
    [UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];
    
    
}//end of moveGhostPt2


-(void)moveAngel
{
    [sndControler playSoundEffect:GoHeavenSound];
    self.angelView.alpha = 1.0;
    [UIView beginAnimations:@"angel_part1" context:NULL];
    [UIView setAnimationDuration:1.5];
    self.angelView.alpha = 0.0;
    self.angelView.center = CGPointMake(angelView.center.x, 0);
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];

}//end of moveAngel


-(void)moveAngelPt2{
    [UIView beginAnimations:@"angel_part2" context:NULL];
    [UIView setAnimationDuration:.5];
    self.angelView.alpha = 0.0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];

}//moveAngelPt2


- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	
    
    if([animationID isEqualToString:[NSString stringWithFormat:@"MoveGhostPart1"]])
    {
        character.center = CGPointMake(character.center.x, character.center.y + 40);
        [self moveGhostPt2];
        [self.character killedAnimation];
        [self.sndControler playSoundEffect:KilledSound];
    }
    else if([animationID isEqualToString:[NSString stringWithFormat:@"MoveGhostPart_2"]])
    {
        //[self.character reviveAnimation];
        [self.character removeFromSuperview];
        self.angelView.center = self.character.center;
        [self.view insertSubview:self.character aboveSubview:ghostImages[MAX_Node_Num - 1]];        
        [self moveAngel];
        
    }else if([animationID isEqualToString:[NSString stringWithFormat:@"angel_part1"]])
    {

        [self gameEndedAlert:@"You lose....." TAG:GameLose];
    
    }
} //end of - (void)growAnimationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context


- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    if(drawAnimating && wonAnimating)
    {
        [self gameEndedAlert:@"Stage Clear!!" TAG:GameDraw];
        wonAnimating = NO;
        drawAnimating = NO;
        
        for(int i=0; i< NoNodes; i++)
            [ghostImages[i] setPosition:DefaultPosition];
        
        
    }//end of drawAnimating
    else if (drawAnimating && !wonAnimating)
    {
        [self gameEndedAlert:@"Stage Clear!!" TAG:GameDraw];

        drawAnimating = NO;
    }
    else if(wonAnimating)
    {        
        [sndControler playSoundEffect:ApplauseSound];
        for(int i =0; i< NoNodes; i++)
        {
            [ghostImages[i] setPosition:ApplausePosition];
            [ghostImages[i] applause];
            ghostImages[i].hidden = NO;
            
        }
        wonAnimating = NO;
        NSTimer *showWinningAlert;
        showWinningAlert = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(game_won_alert) userInfo:nil repeats:NO];
        
    }
}//end of animationDidStop



#pragma mark - Event Handling Procedures 

#pragma mark touches delegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(showingReportDetail == YES)// || makingStepwiseReport == YES)
        return;
    
    UITouch *touch = [touches anyObject];
    touchLocation = [touch locationInView:self.view];
    
    if([touch tapCount]==3)
    {
        [self showHideCrossKeys];
    } 

    if ([touch tapCount] >= 1 && ([touch view] == character  || (self.character.uma != NULL && ([touch view] == self.character.uma ) )  ) ){
        startPoint = [touch locationInView:self.view];
        powerShown = YES;        
        powerLayer.hidden = NO;
        powerLayer.center = nodeImages[relyingNode].center;//startPoint;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationDelegate:self];
        CGAffineTransform transform1 = CGAffineTransformMakeScale(0.01, 0.01);
        CGAffineTransform transform = CGAffineTransformMakeScale(0.05, 0.05);
        powerLayer.transform = transform1;
        powerLayer.transform = transform;
        [UIView commitAnimations];
        prevScale = 0.05;
    }//end of if

    bool found = false;
    
    for(int i=0; i< NoNodes; i++)
    {
        if([touch view] == nodeImages[i] && copy[i].parent_id > -2 && relyingNode != i)
        {
            relyingNode = i;
            [self moveCharacterTo:nodeImages[i].center time:1.0f];
            found = true;
            
        }//end of if
    }//end of for
    
    if([touch view] != character && !found)
    {
        [self instruction];
    }
    
    
}//end of touchesBegan

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(showingReportDetail)
        return;
    
    if(powerShown)
    {
        if(powerLayer.hidden)
            powerLayer.hidden = NO;
        if(powerLayer.center.x != character.center.x)
            powerLayer.center = character.center;
        
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInView:self.view];
        CGPoint oldLocation = startPoint;//[touch previousLocationInView:self];
        double dist = [self calcScale:location.x :oldLocation.x :location.y :oldLocation.y];
        double currentPower = [self calcDistance:nodeList[relyingNode].x :location.x :nodeList[relyingNode].y :location.y];
        copy[relyingNode].power = currentPower;
        
        [self updateFuelBar:(currentPower + totalPower - nodeList[relyingNode].power)];
        
        
        for(int i=1; i< NoNodes; i++)
        {   
            if(currentPower > pMat[relyingNode][i] && copy[i].parent_id < 0)
            {
                if(copy[i].parent_id == -2)
                {
                    copy[i].parent_id = relyingNode;
                }
                //[nodeImages[i] updateImage:relyingNode];
                [self updateNodeAndGhost:i parent:relyingNode];
                
                [self.sndControler playSoundEffect:CoinSound];
                
            }//end of if
            else if(currentPower < pMat[relyingNode][i] && copy[i].parent_id == relyingNode && nodeList[i].parent_id != relyingNode)
            {                
                copy[i].parent_id = -2;//relyingNode;
                //[nodeImages[i] updateImage:-2];
                [self updateNodeAndGhost:i parent:-2];
            }//end of else if
            
        }//end of for
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.005];
        [UIView setAnimationDelegate:self];
        CGAffineTransform transform1 = CGAffineTransformMakeScale(prevScale, prevScale);
        CGAffineTransform transform = CGAffineTransformMakeScale(dist, dist);
        powerLayer.transform = transform1;
        powerLayer.transform = transform;
        [UIView commitAnimations];
        prevScale = dist;
    }//end of i
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(showingReportDetail)
        return;
        
    
    BOOL parent_modified = NO;
    for(int i=0; i< NoNodes; i++)
    {
        if(nodeList[i].parent_id != copy[i].parent_id)
        {
            parent_modified = YES;
            break;
        }
    }
    
    if(parent_modified)
    {
            [self applyChangesConfirmed];
        [self instruction];
    }
    else
    {
        [self updateFuelBar:totalPower];    
        powerShown = NO;
        powerLayer.hidden = YES;        
    }
     
    

}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{

}

#pragma mark Event Actions
-(void)restorePreviousTree
{

    
    if(step>0)
    {
        self.powerLayer.hidden = YES;
        [self.powerLayer release];
        self.powerLayer = nil;
        self.powerLayer = [tPowerArray objectAtIndex:[tPowerArray count] - 1];
        [tPowerArray removeObjectAtIndex:[tPowerArray count] - 1];
        self.powerLayer.hidden = YES;

        [self.sndControler playSoundEffect:PrevStepSound];
        
        step--;
        int tempRelyingNode,rel, dest;
        rel =0;
        
        if(showingReportDetail == NO)
        [sql copyResult:copy Step:step Previous:step-1 
                 TreeID:treeID RelyingNode:&tempRelyingNode totalPower:&totalPower];
        else
        [report_sql copyResult:copy Step:step Previous:step-1 
                     TreeID:treeID RelyingNode:&tempRelyingNode totalPower:&totalPower];
        
        
        NSString *stat = @"";
        
        if(makingStepwiseReport == NO && showingReportDetail == NO)
        {
        stat = [NSString stringWithFormat:@"INSERT INTO UserLog(UL_ActualStep, UL_RelyingNodeID, UL_DestinationNodeID, UL_Power, UL_Action_ID, UL_Tree_ID, UL_Step) VALUES (%d, %d, %d, %f, 2, %d, -2);", actualStep , prevRelyingNode, prevDestNode, totalPower, treeID];
            
        actualStep++;
                     
        [sql query:stat];
        }
        else if(initializingTree == NO && showingReportDetail == NO)
        {
            [self.stepStrategyReport updateLabel:step output:mostAdcancedStep];
            NSString *prevComment = [sql getText:[NSString stringWithFormat:@"SELECT comment FROM StepwiseLog where treeID=%d AND stepNum=%d",treeID, step]];

            prevComment = [prevComment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if([prevComment length] == 0)
                prevComment = @"Why did you choose this route?";
            NSLog(@"prev is %@", prevComment);
                
            NSString *currentComment = [self.stepStrategyReport replaceAndReturn:prevComment];
            NSLog(@"current is %@", currentComment);
            
            
            int updateCheck = [sql getInteger:[NSString stringWithFormat:@"SELECT COUNT(treeID) FROM StepwiseLog WHERE treeID=%d AND stepNum=%d ", treeID, step +1]];
            
            if([self isCurrentCommentDefaultText:currentComment])
                currentComment = @"";
            
            
            if(updateCheck==0)
            {
            stat = [NSString stringWithFormat:@"INSERT INTO StepwiseLog(treeID, stepNum, comment) VALUES(%d, %d, '%@');", treeID, step+1, currentComment];
            }
            else
            {
            stat = [NSString stringWithFormat:@"UPDATE StepwiseLog SET comment='%@' WHERE treeID=%d AND stepNum=%d;", currentComment, treeID, step+1];
            }
            
            NSLog(@"statement %@",stat);
            [sql query:stat];
        }

        
        
        if(step > 0)
        {
            if(showingReportDetail == NO)
            [sql restoreStep:step -1 TreeID:treeID relyingNode:&rel destinationNode:&dest totalPower:&totalPower];
            else
            [report_sql restoreStep:step -1 TreeID:treeID relyingNode:&rel destinationNode:&dest totalPower:&totalPower];
            
            double currentPower = o_pMat[rel][dest];
            copy[rel].power = currentPower;
            for(int i =1; i< NoNodes; i++)
            {
                if(o_pMat[rel][i] <= currentPower && copy[i].parent_id == -2)
                    copy[i].parent_id = rel;
            }//end of for
        }//end of if
        else
            totalPower = 0;//since it is the initial stage;
        
        [self updateFuelBar:totalPower];        
        for(int i=0; i < NoNodes; i++)
        {
            
            nodeList[i].parent_id = copy[i].parent_id;
            nodeList[i].power = copy[i].power;
            
            //[nodeImages[i] updateImage:nodeList[i].parent_id];
            [self updateNodeAndGhost:i parent:nodeList[i].parent_id];
        }//end of for
        
        if(relyingNode != rel && rel >= 0 && rel <= NoNodes)
        {
            relyingNode = rel;
            CGPoint loc;
            if(nodeImages[relyingNode].center.y < 240)
                loc = CGPointMake(nodeImages[relyingNode].center.x,nodeImages[relyingNode].center.y +20);
            else
                loc = CGPointMake(nodeImages[relyingNode].center.x,nodeImages[relyingNode].center.y -20);
            [self moveCharacterTo:loc time:1.0];
                        
        }//
        
        if(showingReportDetail == YES && [self.currentReport.strategy length] == 0)
        {
            [self updateStepwiseReport_forReportDetail];
        }
    }
}

-(void)restoreNextTree
{
            
    
    int rel, dest;
    if( mostAdcancedStep > step )
    {
        
        [self.sndControler playSoundEffect:NextStepSound];    
        if(showingReportDetail == NO)
        [sql restoreStep:step TreeID:treeID relyingNode:&rel destinationNode:&dest totalPower:&totalPower];
        else       
        [report_sql restoreStep:step TreeID:treeID relyingNode:&rel destinationNode:&dest totalPower:&totalPower];
        
        step++;
        
        NSString *stat = @"";
        
        if(makingStepwiseReport == NO && showingReportDetail == NO)
        {
        stat = [NSString stringWithFormat:@"INSERT INTO UserLog(UL_ActualStep, UL_RelyingNodeID, UL_DestinationNodeID, UL_Power, UL_Action_ID, UL_Tree_ID, UL_Step) VALUES (%d, %d, %d, %f, 3, %d, -1);", actualStep , rel, dest, totalPower, treeID];
        actualStep++;
        [sql query:stat];
        }
        else if(showingReportDetail == NO)
        {
            [self.stepStrategyReport updateLabel:step output:mostAdcancedStep];
            NSString *prevComment = [sql getText:[NSString stringWithFormat:@"SELECT comment FROM StepwiseLog where treeID=%d AND stepNum=%d",treeID, step]];
            prevComment = [prevComment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if([prevComment length] == 0)
                prevComment = @"Why did you choose this route?";
            
            NSLog(@"stored prev is %@", prevComment);
            NSString *currentComment = [self.stepStrategyReport replaceAndReturn:prevComment];
            NSLog(@"stored current is %@", currentComment);
            int updateCheck = [sql getInteger:[NSString stringWithFormat:@"SELECT COUNT(treeID) FROM StepwiseLog WHERE treeID=%d AND stepNum=%d ", treeID, step-1]];
            
            if([self isCurrentCommentDefaultText:currentComment])
                currentComment = @"";
            
            if(updateCheck==0)
            {
                stat = [NSString stringWithFormat:@"INSERT INTO StepwiseLog(treeID, stepNum, comment) VALUES(%d, %d, '%@');", treeID, step-1, currentComment];
            }
            else
            {
                stat = [NSString stringWithFormat:@"UPDATE StepwiseLog SET comment='%@' WHERE treeID=%d AND stepNum=%d;", currentComment, treeID, step-1];            
            }
                        NSLog(@"statement %@",stat);
            [sql query:stat];
        }
        
        [self updateFuelBar:totalPower];

        double currentPower = o_pMat[rel][dest];

        self.powerLayer.center = nodeImages[rel].center;
        [self add_A_PowerLayer:currentPower];

        copy[rel].power = currentPower;
        for(int i =1; i< NoNodes; i++)
        {
            if(o_pMat[rel][i] <= currentPower && copy[i].parent_id == -2 )
            {   
            copy[i].parent_id = rel;
            }//end of if
        }//end of for
        for(int i=0; i < NoNodes; i++)
        {
            nodeList[i].parent_id = copy[i].parent_id;
            nodeList[i].power = copy[i].power;
            [self updateNodeAndGhost:i parent:nodeList[i].parent_id];
        }//end of for
        
        if(relyingNode != rel && rel >= 0 && rel <= NoNodes)
        {
            relyingNode = rel;
            CGPoint loc;
            if(nodeImages[relyingNode].center.y < 240)
                loc = CGPointMake(nodeImages[relyingNode].center.x,nodeImages[relyingNode].center.y +20);
            else
                loc = CGPointMake(nodeImages[relyingNode].center.x,nodeImages[relyingNode].center.y -20);
            [self moveCharacterTo:loc time:1.0];
        }//        
        
        
        if(showingReportDetail == YES && [self.currentReport.strategy length] == 0)
        {
            [self updateStepwiseReport_forReportDetail];
        }

    }

}


-(void)refreshTree
{
    if(refreshConfirmed)
    {
        mostAdcancedStep =0;
        
        [self removePowerLayers];
        
        if(NextTree_NoNodes != 0)
            NoNodes = NextTree_NoNodes;
        [self.sndControler playSoundEffect:RefreshSound];
        [self updateStageNumber];
        
        for(int i=0; i< MAX_Node_Num; i++)
            if(nodeImages[i] != NULL )
            {
                nodeImages[i].hidden = YES;
                ghostImages[i].hidden = YES;
            }
        
        CGRect frame = CGRectMake(0, 0, 60.0-(0.3*NoNodes), 60.0-(0.3*NoNodes));        
        for(int i=0; i< NoNodes; i++)
        {
         
            nodeImages[i].hidden = NO;
            ghostImages[i].hidden = NO;
            [nodeImages[i] updateFrame:frame];
        }
        
        
        
        [self deleteTree];
        [self generateTree];
        [self updateFuelBar:totalPower];
        
    }

    else 
    {
        // open a alert with an OK and cancel button
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do you want to renew the game?"
                                                       delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        alert.tag = Alert_TreeRefreshConfirmation;
        [alert release];
    }//end of else if
    
}//end of refreshTree



#pragma mark EnemyView Delegate
- (void)moveDidFinish:(EnemyView *)ghost
{
    [ghost moveGhost];
}

- (void)applauseDidFinish:(EnemyView *)ghost
{
    [ghost applause];
//    [ghost setPosition:DefaultPosition];
}


#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(actionSheet.tag == Alert_ApplyChangesConfirmation)
    {
        if (buttonIndex == 1)
        {
            
            [self applyChangesConfirmed];
        }
        else
        {
            for(int i=0; i< NoNodes; i++)
            {
                copy[i].parent_id = nodeList[i].parent_id;
                copy[i].power = nodeList[i].power;
                [self updateNodeAndGhost:i parent:nodeList[i].parent_id];
            }//end of for
            [self updateFuelBar:totalPower];
            powerShown = NO;
            powerLayer.hidden = YES;
        }
    }//end of if tag is 1
    else if(actionSheet.tag == Alert_FinalTreeConfirmation)
    {
        
        if (buttonIndex == 1)
        {
            [self finalTreeConfirmed];
        }
        else
        {
            mostAdcancedStep--;
            [self restorePreviousTree];
        }//not the final tree
        
    }//end of if tag 2
    else if(actionSheet.tag == Alert_GameEndedConfirmation || actionSheet.tag == Alert_GameEndedConfirmation_Lost)
    {

        
        [self characterLevelControl];
        
        
        for(int i=0; i< NoNodes; i++)
        {
            //[ghostImages[i] stopGhostTimer];
            [ghostImages[i] setPosition:DefaultPosition];
            ghostImages[i].applause_animating = NO;
            [ghostImages[i] hideGhost:YES];
        }
            
        if(actionSheet.tag == Alert_GameEndedConfirmation)
        {
            [self stopApplause];
            switch (buttonIndex) {
                case GameEnded_NewGame:
                        retrying = NO;
                    
                if(stageNum >= 0 && stageNum< 100){stageNum++;}
                [self renewTheGame];
                break;
                    
                case GameEnded_Retry:
                    retrying = YES;
                [self restartTheGame];
                break;
                
                case GameEnded_Quit:
                    retrying = NO;
                    
                [self.navigationController popViewControllerAnimated:YES];
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
                dispatch_async(queue, ^(void) { 
                    if(stageNum< 100){stageNum++;}
                    [self renewTheGame];
                });

                break;
                default:
                break;
                }
            }
            else
            {
                switch (buttonIndex) {
                    case 1:
                        [self restartTheGame];
                                retrying = YES;
                        break;
                        
                    case GameEnded_Quit:
                                retrying = NO;
                        [self.navigationController popViewControllerAnimated:YES];
                        
                        break;
                    default:
                        break;
                }

                
                
            }
    }
    else if(actionSheet.tag == Alert_TreeRefreshConfirmation)
    {
                
        if(buttonIndex==1)
        {
            retrying = NO;
            refreshConfirmed = YES;
            [self refreshTree];
        }//end of if
        
    }//end of else if tag Alert_TreeRefreshConfirmation
    else if(actionSheet.tag == Alert_WonDCTConfirmation)
    {
        retrying = NO;
        for(int i=0; i< NoNodes; i++)
            [ghostImages[i] setPosition:DefaultPosition];
        
        [self stopApplause];
        switch (buttonIndex) {
            case WD_Renew:
                if(stageNum< 100){stageNum++;}                
                [self renewTheGame];
                break;
            case WD_Quit:
                [self.navigationController popViewControllerAnimated:YES];
                [self renewTheGame];
                break;
                
            case WD_Log_Overall:
                NSLog(@"general");
                if(!autoReporting)
                {
                    if(!self.reqObj)
                        self.reqObj = [[SubmissionRequestor alloc] init] ;
                    
                    [self.reqObj submitStrategy:treeID strategy:@"" score:winningPercentage stepNumber:actualStep stg:stageNum rptType:RegularStepSubmission];
                }
                
                if(!self.report)
                {
                    self.report = [[StrategyReport alloc] initWithNibName:nil bundle:nil];
                    self.report.delegate = self;
                }
                [self.navigationController pushViewController:self.report animated:YES];                
                
                break;
            case WD_Log_Stepwise:
                if(!autoReporting)
                {
                    if(!self.reqObj)
                        self.reqObj = [[SubmissionRequestor alloc] init] ;
                    
                    [self.reqObj submitStrategy:treeID strategy:@"" score:winningPercentage stepNumber:actualStep stg:stageNum rptType:RegularStepSubmission];
                }
                
                NSLog(@"stepwise");
                [self show_StepwiseReport];
                break;
                
            default:
                break;
        }
    }
    else if(actionSheet.tag == Alert_ReportType)
    {
        NSLog(@"Button index %d", buttonIndex);
        
        if(buttonIndex == RA_Cancelled)
        {
            if(stageNum >= 0 && stageNum< 100){stageNum++;}
            [self renewTheGame];
        }
        else if(buttonIndex ==  RA_Stepwise)
        {
            NSLog(@"stepwise");
            [self show_StepwiseReport];
        }
        else
        {
            NSLog(@"general");
            if(!self.report)
            {
                self.report = [[StrategyReport alloc] initWithNibName:nil bundle:nil];
                self.report.delegate = self;
            }
            [self.navigationController pushViewController:self.report animated:YES];

            
            
        }
    
    }
}


-(void)applyChangesConfirmed
{
    double power= - DBL_MAX;
    int dest;
    
    for(int i=0; i< NoNodes; i++)
    {
        nodeList[i].parent_id = copy[i].parent_id;
        
        if(copy[i].parent_id == relyingNode && power < o_pMat[i][relyingNode])
        {
            power = o_pMat[i][relyingNode];
            dest = i;
        }//end of if
    }//end of for 
    totalPower += power - nodeList[relyingNode].power;
    nodeList[relyingNode].power = power;
    
    [self add_A_PowerLayer :power];
    [self updateFuelBar:totalPower];
    
    winningPercentage = ((o_dctTotalPower-totalPower)/o_dctTotalPower)*100;
    NSString *stat = @"";
    stat = [NSString stringWithFormat:@"INSERT INTO UserLog(UL_Step, UL_RelyingNodeID, UL_DestinationNodeID, UL_Power, UL_Action_ID, UL_Tree_ID, UL_ActualStep) VALUES (%d, %d, %d, %f, 1, %d, %d);",step, relyingNode, dest, totalPower, treeID, actualStep];
    

    step++;
    mostAdcancedStep = step;
    actualStep++;
    prevRelyingNode = relyingNode;
    prevDestNode = dest;
    
    if(!sql)
    {
        sql = [[SQLManager alloc] init];
    }
    [sql query:stat];
    
    BOOL allInTree = YES;
    
    for(int i=0; i< NoNodes; i++)
    {
        if(nodeList[i].parent_id == -2)
            allInTree = NO;
    }//end of for
    
    if(allInTree)
    {//all tree are included
        
            gameEnded = YES;
            [self finalTreeConfirmed];
    }
    else if(totalPower > dctTotalPower)
    {
        [self finalTreeConfirmed];
    }

}//end apply changes confirmed


-(void)finalTreeConfirmed
{
    gameEnded = YES;

    if(totalPower > dctTotalPower)
    {
        [self gameEnded:GameLose];  
    }
    else if(totalPower < dctTotalPower)
    {
        [self gameEnded:GameWin];
    }
    else
    {
        //game draw
        [self gameEnded:GameDraw];
    }//end of if else if else

}//end of finalTreeConfirmed


#pragma mark Reporting Methods

-(void)updateStrategyReportTreeID
{
    if(self.report)
    {
        [self.report updateReportedTreeID];
        [self.report initializeForm];
    }
}

#pragma mark GameKit reporting methods
- (void) reportScore: (int64_t) score forCategory: (NSString*) category
{
    if(gameCenterOff)
        return;
    
    GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:category] autorelease];
    scoreReporter.value = score;
    
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
        if (error != nil)
        {
            NSLog(@"%@", error);
            // handle the reporting error
        }
    }];
}
- (void) reportAchievementIdentifier: (NSString*) identifier percentComplete: (float) percent
{
    if(gameCenterOff)
        return;
    
    GKAchievement *achievement = [[[GKAchievement alloc] initWithIdentifier: identifier] autorelease];
    if (achievement)
    {
        achievement.percentComplete = percent;
        [achievement reportAchievementWithCompletionHandler:^(NSError *error)
         {
             if (error != nil)
             {
                 NSLog(@"error? %@", error);
                 // Retain the achievement object and try again later (not shown).
             }
           //  else
           //      NSLog(@"achivement worked?");
             
         }];
    }
}

#pragma mark Character Level Handling

-(void)characterLevelControl
{
//    if(characterLevel == 0 || )
        [character makeMeTaller];

    
}
#pragma mark Strategy Report Delegate


-(void)reportSubmittedSucessfully:(StrategyReport *)strtgy
{
    sendingReport = NO;
    
    if(!gameCenterOff)
    [self reportAchievementIdentifier:@"999" percentComplete:100.00];
}

-(void)stepStrategyRequestCompleted:(StepStrategyReport *)req
{
    if(!gameCenterOff)
        [self reportAchievementIdentifier:@"999" percentComplete:100.00];
    
    //NSLog(@"removing....");
    if(!serverAvailable)
    {
        NSString *statement = @"SELECT PR_TREE_ID from PendingReport ORDER BY PR_TREE_ID DESC Limit 1;";
        int idnum = [self.sql getInteger:statement];
        statement = [NSString stringWithFormat:@"UPDATE PendingReport SET ReportType=1 Where PR_TREE_ID=%d",idnum];
        [self.sql query:statement];
    }
    [self removeStepWiseReportView];
}


-(void)stepStrategySubmissionRequested:(StrategyReport *)strtgy
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Thank you very much for submitting your strategy!! We truly appreciate your contribution!!." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alert show];
    [alert release];
    
    NSString *currentComment = self.stepStrategyReport.stepMemo.text;
    
    int updateCheck = [sql getInteger:[NSString stringWithFormat:@"SELECT COUNT(treeID) FROM StepwiseLog WHERE treeID=%d AND stepNum=%d ", treeID, step]];

    NSString* stat;
    if(updateCheck==0)
    {
        stat = [NSString stringWithFormat:@"INSERT INTO StepwiseLog(treeID, stepNum, comment) VALUES(%d, %d, '%@');", treeID, step, currentComment];
    }
    else
    {
        stat = [NSString stringWithFormat:@"UPDATE StepwiseLog SET comment='%@' WHERE treeID=%d AND stepNum=%d;", currentComment, treeID, step];            
    }
//    NSLog(@"statement %@",stat);
    [sql query:stat];

    
}

-(BOOL)isCurrentCommentDefaultText:(NSString*)comment
{
    comment = [comment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([comment isEqualToString:@"Why did you choose this route?"])
            return YES;
        else
        return NO;

}
-(void)removeStepWiseReportView
{
    [self.stepStrategyReport.view removeFromSuperview];
    [self.stepStrategyReport replaceAndReturn:@"Why did you choose this route?"];
    [stepStrategyReport release];
    makingStepwiseReport = NO;
    if(stageNum == -1)
    refresh.hidden = NO;
    self.stepwiseReportShown = NO;
    if(stageNum >= 0 && stageNum< 100){stageNum++;}
    [self renewTheGame];

}
#pragma mark Cross Key related methods
-(void)showHideCrossKeys
{
    BOOL hidden;
    if(self.ck_UpButton.hidden)
        hidden = NO;
    else
        hidden = YES;
    
    
    self.ck_UpButton.hidden = hidden;
    self.ck_DownButton.hidden = hidden;
    self.ck_RightButton.hidden = hidden;
    self.ck_leftButton.hidden = hidden;
    self.addCircleButton.hidden = hidden;
    self.removeCircleButton.hidden = hidden;
    
}

-(int)findTheClosestInTheDirection:(int)direction
{
    int nodeID=-1;
    CGPoint current = nodeImages[relyingNode].center;
    double minimum_distance = MAXFLOAT;
    switch (direction) {
        case CK_Up:
            for(int i =0; i< NoNodes; i++)
            {
                if(i != relyingNode  && (current.y - nodeImages[i].center.y > 0 && nodeList[i].parent_id  != -2 ) )
                    if(minimum_distance > o_pMat[relyingNode][i])
                    {
                        minimum_distance =o_pMat[relyingNode][i];
                        nodeID = i;
                    }
            }
            break;

            case CK_Right:
            for(int i =0; i< NoNodes; i++)
            {
                if(i != relyingNode  && (nodeImages[i].center.x - current.x > 0 && nodeList[i].parent_id  != -2 ) )
                    if(minimum_distance > o_pMat[relyingNode][i])
                    {
                        minimum_distance =o_pMat[relyingNode][i];
                        nodeID = i;
                    }
            }
            break;

            case CK_Down:
            for(int i =0; i< NoNodes; i++)
            {
                if(i != relyingNode  && (nodeImages[i].center.y - current.y> 0 && nodeList[i].parent_id  != -2 ) )
                    if(minimum_distance > o_pMat[relyingNode][i])//if(closest.y - current.y >= nodeImages[i].center.y - current.y)
                    {
                        minimum_distance =o_pMat[relyingNode][i];
                        nodeID = i;
                    }
            }
            break;

            case CK_Left:
            for(int i =0; i< NoNodes; i++)
            {
                if(i != relyingNode  && (current.x - nodeImages[i].center.x > 0 && nodeList[i].parent_id  != -2 ) )
                    if(minimum_distance > o_pMat[relyingNode][i])
                    {
                        minimum_distance =o_pMat[relyingNode][i];
                        nodeID = i;
                    }
            }
            break;

            default:
            break;
    }
    return  nodeID;
}

-(void)moveOneToTheDirection:(UIButton*)btn
{
    int newLoc =  [self findTheClosestInTheDirection:btn.tag];
    if(newLoc > -1)
    {
    [self moveCharacterTo:CGPointMake(copy[newLoc].x, copy[newLoc].y) time:1.0];
    relyingNode = newLoc;
    }
}


#pragma mark AddRemoveMethod

-(void)add_A_Circle_withButton
{
    int min_distanced_node=0;
    double min_distanced_power = MAXFLOAT;
    BOOL duplicated = NO;
    for(int i =0; i<NoNodes; i++)
    {
        if(o_pMat[relyingNode][i] <= min_distanced_power && copy[i].parent_id == -2)
        {
            if(o_pMat[relyingNode][i] == min_distanced_power)
                duplicated = YES;
            
            min_distanced_node = i;
            min_distanced_power = o_pMat[relyingNode][i];
        }
    
    }
    powerLayer.center = nodeImages[relyingNode].center;
    
    if(duplicated)
    {
        for(int i =0; i<NoNodes; i++)
        {
        if(o_pMat[relyingNode][i] == min_distanced_power)
            {
                copy[i].parent_id = relyingNode;
            }
        }
    }
    else
    {
        copy[min_distanced_node].parent_id = relyingNode;
    }
    
    [self.sndControler playSoundEffect:CoinSound];
    
    [self applyChangesConfirmed];
    [self updateNodes];    

    
    
}

-(void)remove_A_Circle_withButton
{
    [self restorePreviousTree];
}

-(void)addOrRemove_A_Circle:(UIButton*)btn
{
    if(btn.tag == AR_Add)
    {
        [self add_A_Circle_withButton];
    }
    else
    {
        [self remove_A_Circle_withButton];
    }

}
#pragma mark Report Detail Methods

-(void)updateStepwiseReport_forReportDetail
{
    NSString *stepwise_strtgy = [report_sql getText:[NSString stringWithFormat:@"SELECT comment FROM StepwiseLog where treeID=%d AND stepNum=%d",treeID, step]];
    self.reportedStrategy.text = stepwise_strtgy;
}
-(void)showHideReportedStrategy
{
    if(reportedStrategy.hidden == NO)
    {
        reportedStrategy.hidden = YES;
    }
    else
    {
        if(currentReport.strategy.length > 0)
        {
            reportedStrategy.text = currentReport.strategy;        
        }
        else
        {
            [self updateStepwiseReport_forReportDetail];
        }

        
        reportedStrategy.hidden = NO;
    }
}
-(BOOL)reorganizeForReportID:(ReportItem*)reportObj
{
 
    if(!showingReportDetail)
    {
        actualTreeID = treeID;
        actualNoNodes = NoNodes;
    }
    
    
    if(report_sql)
    {
        [report_sql release];
        report_sql = nil;
    }
    
    self.report_sql = [[SQLManager alloc] initWithReportID:reportObj.reportID];
    
    NSString* statement=[NSString stringWithFormat:@"Select count(*) FROM NodeList WHERE NL_TREE_ID=%d", reportObj.treeID];
    int local_noNodes  = [self.report_sql getInteger:statement];

    if(local_noNodes < 1)
        return NO;
    else
        NoNodes = local_noNodes;
    
    self.currentReport = reportObj;
    reportedStrategy.hidden=YES;

    
    treeID = reportObj.treeID;
    showingReportDetail =YES;
    int xarray[NoNodes], yarray[NoNodes];
    reorganizedForReport = YES;
    
    [self removePowerLayers];
    
    [self.report_sql copyTree:xarray y:yarray tree:reportObj.treeID];
    
    for(int i =0; i< MAX_Node_Num; i++)
    {
        //[self.view addSubview:ghostImages[i]];
        if(i >= NoNodes)
        {
            nodeImages[i].hidden = YES;
            ghostImages[i].hidden = YES;
        }
        else
        {
            nodeImages[i].hidden = NO;
            ghostImages[i].hidden = YES;
        }
            
    }

    for(int i=0; i < NoNodes; i++)
    {
        ghostImages[i].center = nodeImages[i].center = CGPointMake((float)xarray[i], (float)yarray[i]);
        //[self generateRandomPoint];
        copy[i].id_number = nodeList[i].id_number = i;
        copy[i].parent_id =  nodeList[i].parent_id = -2;
        copy[i].power = nodeList[i].power =0;
        copy[i].x = nodeList[i].x = nodeImages[i].center.x;
        copy[i].y = nodeList[i].y = nodeImages[i].center.y;
        copy[i].inTree = nodeList[i].inTree = NO;
    }//end of for

    for(int i =0; i< NoNodes; i++)
    {
        for(int j=0; j<NoNodes; j++)
        {
            o_pMat[i][j] = pMat[i][j] = [self calcDistance:nodeList[i].x :nodeList[j].x :nodeList[i].y :nodeList[j].y];
        }//end of for j
    }//end of for i
    
    DCT *dctest = [[[DCT alloc] copyNodesOver:nodeList] autorelease];
    [dctest displayArea:NoNodes];
    dctTotalPower = [dctest constructDCT:NoNodes];
    
    o_dctTotalPower = dctTotalPower;
    if(stageNum != 0)
        dctTotalPower = (dctTotalPower * 0.15 * Difficulty) + dctTotalPower;
    
    struct nodeType dctAnswer[NoNodes];
    [dctest copyDctAnswer:dctAnswer];

    copy[0].parent_id = nodeList[0].parent_id = -1;
    relyingNode = 0;
    
    if(nodeList[0].y < 240)
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y + 20);//[self generateRandomPoint];
    else
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y - 20);//[self generateRandomPoint];
    
    [self updateNodes];

    step =0; 
    statement = [NSString stringWithFormat:@"select UL_Step From UserLog WHERE UL_TREE_ID=%d ORDER BY UL_Step DESC Limit 1;",reportObj.treeID ];
    mostAdcancedStep = [report_sql getInteger:statement]  + 1;
    
    if(reportObj.stageNum == -1)
        self.stageLabel.text = @"Rndm";
    else
        self.stageLabel.text = [NSString stringWithFormat:@"Stage.%d",reportObj.stageNum + 1];

    totalPower =0;
    [self updateFuelBar:totalPower];
    
    refresh.hidden = YES;
    self.showStrategyButton.hidden = NO;

    statement = [NSString stringWithFormat:@"SELECT ReportType FROM ReportList WHERE ID=%d;", reportObj.reportID];
    int type = [self.sql getInteger:statement];
    if(type == 2)
    {
        self.showStrategyButton.hidden = YES;
        self.reportedStrategy.hidden = YES;
    }
    return YES;
}


-(void)reorganizeForGeneralGame
{
    treeID= actualTreeID;
    NoNodes = actualNoNodes;
    
    
    int xarray[NoNodes], yarray[NoNodes];
    reorganizedForReport = YES;
    
    [self removePowerLayers];
    
    [self.sql copyTree:xarray y:yarray tree:treeID];
    
    for(int i =0; i< MAX_Node_Num; i++)
    {
        if(i >= NoNodes)
        {
            nodeImages[i].hidden = YES;
            ghostImages[i].hidden = YES;
        }
        else
        {
            nodeImages[i].hidden = NO;
            ghostImages[i].hidden = YES;
        }
        
    }
    
    for(int i=0; i < NoNodes; i++)
    {
        ghostImages[i].center = nodeImages[i].center = CGPointMake((float)xarray[i], (float)yarray[i]);
        copy[i].id_number = nodeList[i].id_number = i;
        copy[i].parent_id =  nodeList[i].parent_id = -2;
        copy[i].power = nodeList[i].power =0;
        copy[i].x = nodeList[i].x = nodeImages[i].center.x;
        copy[i].y = nodeList[i].y = nodeImages[i].center.y;
        copy[i].inTree = nodeList[i].inTree = NO;
    }//end of for
    
    for(int i =0; i< NoNodes; i++)
    {
        for(int j=0; j<NoNodes; j++)
        {
            o_pMat[i][j] = pMat[i][j] = [self calcDistance:nodeList[i].x :nodeList[j].x :nodeList[i].y :nodeList[j].y];
        }//end of for j
    }//end of for i
    
    DCT *dctest = [[[DCT alloc] copyNodesOver:nodeList] autorelease];
    [dctest displayArea:NoNodes];
    dctTotalPower = [dctest constructDCT:NoNodes];
    
    o_dctTotalPower = dctTotalPower;
    if(stageNum != 0)
        dctTotalPower = (dctTotalPower * 0.15 * Difficulty) + dctTotalPower;
    
    struct nodeType dctAnswer[NoNodes];
    [dctest copyDctAnswer:dctAnswer];
    
    copy[0].parent_id = nodeList[0].parent_id = -1;
    relyingNode = 0;
    
    if(nodeList[0].y < 240)
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y + 20);//[self generateRandomPoint];
    else
        character.center = CGPointMake(nodeList[0].x, nodeList[0].y - 20);//[self generateRandomPoint];
    
    [self updateNodes];
    
    step =0; 
    NSString *statement = [NSString stringWithFormat:@"select UL_Step From UserLog WHERE UL_TREE_ID=%d ORDER BY UL_Step DESC Limit 1;",treeID ];
    step = mostAdcancedStep = [self.sql getInteger:statement]  + 1;
    NSLog(@"most advanced %d", mostAdcancedStep );
    
    
    
    int rel, dest;
    for(int i =0; i< step; i++)
    {
        [sql restoreStep:i TreeID:treeID relyingNode:&rel destinationNode:&dest totalPower:&totalPower];
        
        double currentPower = o_pMat[rel][dest];
        
        self.powerLayer.center = nodeImages[rel].center;
        [self add_A_PowerLayer:currentPower];
        copy[rel].power = currentPower;
        for(int i =1; i< NoNodes; i++)
        {
            if(o_pMat[rel][i] <= currentPower && copy[i].parent_id == -2)
                copy[i].parent_id = rel;
        }//end of for
        
    }//end of for
    
    
    
    for(int i=0; i < NoNodes; i++)
    {
        nodeList[i].parent_id = copy[i].parent_id;
        nodeList[i].power = copy[i].power;
        
        [self updateNodeAndGhost:i parent:nodeList[i].parent_id];
    }//end of for
    
    [self updateFuelBar:totalPower];

    [self updateStageNumber];
    
    self.showStrategyButton.hidden = YES;
    
}


#pragma mark instruction for initial stage
-(void)instruction
{
    if(stageNum == 0 && step < 2 && (dctTotalPower - totalPower) >= 0)
    {
        UIAlertView *alert;
        if(step == 0)
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please Double Tap & Drag the main chatacter."
                                        delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        else
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Tap another light pole to move main character."
                                        delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        alert.tag = Alert_DblTp_Instrctn;
        [alert show];
        [alert release];
    }
}

@end
