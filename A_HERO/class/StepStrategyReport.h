//
//  StepStrategyReport.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * StepStrategyReport class. 
 * Provide GUI for stepwise strategy report
 *******************************************/


#import <UIKit/UIKit.h>
#import "SubmissionRequestor.h"
@protocol StepStrategyReportDelegate;
@interface StepStrategyReport : UIViewController<UITextViewDelegate, SubmissionRequestorDelegate, UIAlertViewDelegate>
{
    UITextView *stepMemo;
    UIToolbar *bottomBar;
    UIButton *addANoteButton, *submitButton;
    UIButton *memoDoneButton;
    UILabel *stepLabel;
    BOOL textShown;
    SubmissionRequestor *requestorObj;
    id<StepStrategyReportDelegate>delegate;
    
}
@property(retain, nonatomic)UITextView *stepMemo;
@property(retain, nonatomic)UIToolbar *bottomBar;
@property(retain, nonatomic)UIButton *addANoteButton, *submitButton;
@property(retain, nonatomic)UIButton *memoDoneButton;
@property(retain, nonatomic)UILabel *stepLabel;
@property BOOL textShown;
@property(retain, nonatomic)SubmissionRequestor *requestorObj;
@property(assign, nonatomic)id<StepStrategyReportDelegate> delegate;

-(void)setUpStepStrategyReportView;
-(void)showText;
-(void)doneEditting;
-(void)updateLabel:(NSInteger)nstep output:(NSInteger)kstep;
-(void)askConfirmation;
-(void)submitAction;
-(NSString*)replaceAndReturn:(NSString*)passedComment;



@end


@protocol StepStrategyReportDelegate <NSObject>
@optional
-(void)stepStrategyRequestCompleted:(StepStrategyReport*)req;
-(void)stepStrategySubmissionRequested:(StepStrategyReport *)strtgy;

@end


