//
//  EncryptorClass.h
//
//  Created by Yuto Fujikawa on 7/30/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * EncryptorClass class. 
 * Provides static encryption methods
 *******************************************/
#import <Foundation/Foundation.h>


@interface EncryptorClass : NSObject {
    
}
+(NSString*) sha1:(NSString*)input;
+ (NSString *) md5:(NSString *) input;
+(NSString*)getCurrentDate;
+(NSString*)makeShortHash;
@end
