//
//  ReportTypeView.h
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReportTypeView class. 
 * Provides GUI for table that lists the
 * type of reports
 *******************************************/

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "SQLManager.h"
#import "ReportList.h"
@interface ReportTypeView : UIViewController<UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate>
{
    ReportList *list;

}
@property(retain, nonatomic)ReportList *list;
-(void)setUpReportTypeView;
@end
