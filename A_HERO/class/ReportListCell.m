//
//  ReportListCell.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 1/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReportListCell class. 
 * Provides GUI for each row for ReportList
 * table
 *******************************************/

#import "ReportListCell.h"

@implementation ReportListCell
@synthesize stageNumLabel, dateTimeLabel;

-(void)dealloc{
    [stageNumLabel release];
    [dateTimeLabel release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier
{

    if(self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier])
    {
        
		UIView *myContentView = self.contentView;
		[myContentView setBackgroundColor:[UIColor clearColor]];
        self.stageNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.stageNumLabel.font = [UIFont systemFontOfSize:15.0];
        self.stageNumLabel.textAlignment = UITextAlignmentCenter;
        self.stageNumLabel.textColor = [UIColor blackColor];
        self.stageNumLabel.backgroundColor = [UIColor clearColor];
        self.stageNumLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        [myContentView addSubview:self.stageNumLabel];
        self.stageNumLabel.center = CGPointMake(200, 30);
        
        self.dateTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.dateTimeLabel.font = [UIFont systemFontOfSize:15.0];
        self.dateTimeLabel.textAlignment = UITextAlignmentCenter;
        self.dateTimeLabel.textColor = [UIColor blackColor];
        self.dateTimeLabel.backgroundColor = [UIColor clearColor];
        self.dateTimeLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        [myContentView addSubview:self.dateTimeLabel];
        self.dateTimeLabel.center = CGPointMake(200, 30);
    }
    
    return self;
}


-(void)setData:(ReportItem*)reportObj
{
    
    if(!self.stageNumLabel)
    {
        UIView *myContentView = self.contentView;
		[myContentView setBackgroundColor:[UIColor clearColor]];
        self.stageNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.stageNumLabel.font = [UIFont systemFontOfSize:15.0];
        self.stageNumLabel.textAlignment = UITextAlignmentCenter;
        self.stageNumLabel.textColor = [UIColor blackColor];
        self.stageNumLabel.backgroundColor = [UIColor clearColor];
        self.stageNumLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        [myContentView addSubview:self.stageNumLabel];
        self.stageNumLabel.center = CGPointMake(210, 15);
        
        self.dateTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
        self.dateTimeLabel.font = [UIFont systemFontOfSize:15.0];
        self.dateTimeLabel.textAlignment = UITextAlignmentCenter;
        self.dateTimeLabel.textColor = [UIColor blackColor];
        self.dateTimeLabel.backgroundColor = [UIColor clearColor];
        self.dateTimeLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        [myContentView addSubview:self.dateTimeLabel];
        self.dateTimeLabel.center = CGPointMake(210, 35);
    }
    
    NSString *stageNumStr;
    if(reportObj.stageNum >= 0)
        stageNumStr = [NSString stringWithFormat:@"Stage %d",reportObj.stageNum + 1];
    else
        stageNumStr = @"Random";
    self.stageNumLabel.text = stageNumStr;
    self.dateTimeLabel.text = reportObj.reportDate;
}

@end
