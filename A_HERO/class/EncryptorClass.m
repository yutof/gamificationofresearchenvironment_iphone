//
//  EncryptorClass.m
//
//  Created by Yuto Fujikawa on 7/30/11.
//  Copyright 2011 South Dakota State University. All rights reserved.
//
/*******************************************
 * Class Description:
 * EncryptorClass class. 
 * Provides static encryption methods
 *******************************************/


#import "EncryptorClass.h"
#import <CommonCrypto/CommonDigest.h>

@implementation EncryptorClass


+(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

+(NSString*)getCurrentDate
{
    NSString* now=@"";
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    now = [[dateFormatter stringFromDate:[NSDate date]] description]; 
    return now;    
}

+(NSString*)makeShortHash
{
    NSString *hash = [self getCurrentDate];

    hash = [NSString stringWithFormat:@"hash%@5characters", hash];
    
    if(rand()%2)
        hash = [self sha1:hash];
    else
        hash = [self md5:hash];
    
    NSInteger rangeStart;
    
    rangeStart = rand()%[hash length];
    
    if(rangeStart > [hash length] - 10)
        rangeStart = [hash length] - ([hash length] - 10);
    
    NSRange range = NSMakeRange(rangeStart,5);
    hash =[hash substringWithRange:range];
            
    return hash;
}


@end
