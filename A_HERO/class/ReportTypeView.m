//
//  ReportTypeView.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 12/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * ReportTypeView class. 
 * Provides GUI for table that lists the
 * type of reports
 *******************************************/

#import "ReportTypeView.h"

@implementation ReportTypeView
@synthesize list;

-(void)dealloc{

    if(list)
        [list release];
    [super dealloc];
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setUpReportTypeView];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle

-(void)setUpReportTypeView
{ 
    UITableView *typeTable = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStyleGrouped];	
	typeTable.delegate = self;
	typeTable.dataSource = self;
	typeTable.autoresizesSubviews = YES;
	[self.view addSubview:typeTable];
	typeTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 00.0, 320.0, 90.0)];
    [typeTable release];
    


}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        return 3;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
    
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    if(indexPath.section == 0)
        cell.textLabel.text = @"General Strategy";
    else if(indexPath.section == 1)
        cell.textLabel.text = @"Stepwise Strategy";
    else
        cell.textLabel.text = @"Movement Log";
    
    
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = @"";
    return title;
	
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(!list)
        list = [[ReportList alloc] initWithReportListTag:indexPath.section];
    else
        [list resetUpReportList:indexPath.section];
    
    
    [self.navigationController pushViewController:list animated:YES];
    if(indexPath.section == 0)
    {

        NSLog(@"general");
    }
    else
    {
        NSLog(@"stepwise");
    }
    
}//end of didSelect

@end
