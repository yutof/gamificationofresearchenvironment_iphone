//
//  A_HEROAppDelegate.m
//  A_HERO
//
//  Created by Yuto Fujikawa on 10/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
/*******************************************
 * Class Description:
 * Delegete class. 
 * Store and initialize default values for 
 * application variables. Initialize the 
 * application at launch time.
 * It also checkes whether there is unsubmitted
 * reports or not
 *******************************************/

#import "A_HEROAppDelegate.h"
@implementation A_HEROAppDelegate

@synthesize window;
@synthesize rootNav, menu;

- (void)applicationDidFinishLaunching:(UIApplication *)application {
    [self initVariables];
    reporting = false;    
    window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.menu = [[MenuViewController alloc] initWithNibName:nil bundle:nil];
    
    self.rootNav= [[UINavigationController alloc] initWithRootViewController:menu];
    self.rootNav.delegate = self;
    [self.window addSubview:self.rootNav.view];    
	[self.window addSubview:self.menu.view];
    
    [window makeKeyAndVisible];

    viewTagNum = Tag_MenuView;
    [self pendingCheck_AND_update];

}


- (void)applicationWillResignActive:(UIApplication *)application {
    [self saveVariables];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self activateGameView];

}


- (void)dealloc {
    
    if(menu)
        [menu release];
    if(rootNav)
        [rootNav release];
	[window release];
    
	[super dealloc];
}


- (void)applicationWillTerminate:(UIApplication *)notification {
    [self saveVariables];
    
}



- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self saveVariables];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self initVariables];
    
}



-(void)initVariables
{
    NSUserDefaults	*userDefault = [NSUserDefaults standardUserDefaults];
	NSNumber	*stepNumber = (NSNumber*)[userDefault objectForKey:@"step"];
	if ( stepNumber != nil )
		step = [stepNumber intValue];
	else
		step = 0;
    
	NSNumber	*treeNumber = (NSNumber*)[userDefault objectForKey:@"treeID"];
	if ( treeNumber != nil )
		treeID = [treeNumber intValue];
	else
		treeID = 0;
    
    
    
	NSNumber	*nodesNumber = (NSNumber*)[userDefault objectForKey:@"NoNodes"];
	if ( nodesNumber != nil )
		NoNodes = [nodesNumber intValue];
	else
		NoNodes = DefaultNodeNum;
    //	[nodesNumber release];
    
	NSNumber	*difNumber = (NSNumber*)[userDefault objectForKey:@"Difficulty"];
	if ( difNumber != nil )
		Difficulty = [difNumber intValue];
	else
		Difficulty = VeryHard;
    
	NSNumber	*starNumber = (NSNumber*)[userDefault objectForKey:@"Star"];
	if ( starNumber != nil )
		starNum = [starNumber intValue];
	else
		starNum = 0;
    
    
	NSNumber	*dctNumber = (NSNumber*)[userDefault objectForKey:@"DctBeated"];
	if ( dctNumber != nil )
		dctBeatedNum = [dctNumber intValue];
	else
		dctBeatedNum = 0;
    
	NSNumber	*shMenu = (NSNumber*)[userDefault objectForKey:@"showReport"];
	if ( shMenu != nil )
		showReport = [shMenu boolValue];
	else
		showReport = NO;
    
    NSNumber    *gmCenter = (NSNumber*)[userDefault objectForKey:@"gameCenterOFF"];
    if(gmCenter != nil)
        gameCenterOff = [gmCenter boolValue];
    else 
        gameCenterOff = NO;
    
    NSNumber    *pdg = (NSNumber*)[userDefault objectForKey:@"pending"];
    if(pdg != nil)
        pending = [pdg boolValue];
    else 
        pending = NO;
    
    /*
    NSNumber    *cfm = (NSNumber*)[userDefault objectForKey:@"confirmationSetting"];
    if(cfm != nil)
        confirmationSetting = [cfm intValue];
    else 
        confirmationSetting =CFM_None;    
    */
    
    
    NSNumber    *a_step = (NSNumber*)[userDefault objectForKey:@"actualStep"];
    if(a_step != nil)
        actualStep = [a_step intValue];
    else 
        actualStep = 0;        
    
    NSNumber    *sNum = (NSNumber*)[userDefault objectForKey:@"stageNum"];
    if(sNum != nil)
        stageNum = [sNum intValue];
    else 
        stageNum = 0;        
    
    if(stageNum > -1)
        srand(stageNum + 1);
    else
        srand(time(NULL));
    
    NSNumber    *lcNum = (NSNumber*)[userDefault objectForKey:@"lastClearedStage"];
    if(lcNum != nil)
        lastClearedStage = [lcNum intValue];
    else 
        lastClearedStage = -1; 
    
    
    
    NSNumber    *nxTrNum = (NSNumber*)[userDefault objectForKey:@"nextTree"];
    if(lcNum != nil)
        NextTree_NoNodes = [nxTrNum intValue];
    else 
        NextTree_NoNodes = DefaultNodeNum; 
    
    
    NSNumber    *crStage = (NSNumber*)[userDefault objectForKey:@"currentStage"];
    if(crStage != nil)
        curStageNum = [crStage intValue];
    else 
        curStageNum = stageNum; 
    
    NSNumber    *cLevel = (NSNumber*)[userDefault objectForKey:@"characterLevel"];
    if(cLevel != nil)
        characterLevel = [cLevel intValue];
    else 
        characterLevel = 0;
    
    
    NSNumber    *cWin = (NSNumber*)[userDefault objectForKey:@"consecutiveWinCounter"];
    if(cWin != nil)
        consecutiveWinCounter = [cWin intValue];
    else 
        consecutiveWinCounter = 0;

    
    NSNumber    *cLose = (NSNumber*)[userDefault objectForKey:@"consecutiveLoseCounter"];
    if(cLose != nil)
        consecutiveLoseCounter = [cLose intValue];
    else 
        consecutiveLoseCounter = 0;

    

    NSNumber    *rtry = (NSNumber*)[userDefault objectForKey:@"retrying"];
    if(rtry != nil)
        retrying= [rtry boolValue];
    else 
        retrying = NO;

    
    NSNumber    *frst = (NSNumber*)[userDefault objectForKey:@"first"];
    if(frst != nil)
        firstTime= [frst boolValue];
    else 
        firstTime = YES;
    
    NSString *registered_name = (NSString*)[userDefault objectForKey:@"registeredUserName"];
    if(registered_name != nil)
        registeredUserName = registered_name;
    else
        registeredUserName = @"";

    NSString *registered_id = (NSString*)[userDefault objectForKey:@"registeredUserID"];
    if(registered_id != nil)
        registeredUserID = registered_id;
    else
        registeredUserID = @"";

    
    NSNumber    *sqlTag = (NSNumber*)[userDefault objectForKey:@"sqlVersion"];
    if(sqlTag != nil)
        sqlVersionTag= [sqlTag intValue];
    else 
        sqlVersionTag = -1;

    
    NSNumber    *atReport = (NSNumber*)[userDefault objectForKey:@"atReport"];
    if(atReport != nil)
        autoReporting= [atReport boolValue];
    else 
        autoReporting = YES;

    
}//end of initVariables


-(void)saveVariables
{
    NSUserDefaults	*userDefault = [NSUserDefaults standardUserDefaults];
    if(showingReportDetail)
    {
        [userDefault setObject:[NSNumber numberWithInt:actualStep] forKey:@"step"];
        [userDefault setObject:[NSNumber numberWithInt:actualTreeID] forKey:@"treeID"];
        [userDefault setObject:[NSNumber numberWithInt:actualNoNodes] forKey:@"NoNodes"];
    }
    else
    {
        [userDefault setObject:[NSNumber numberWithInt:step] forKey:@"step"];
        [userDefault setObject:[NSNumber numberWithInt:treeID] forKey:@"treeID"];
        [userDefault setObject:[NSNumber numberWithInt:NoNodes] forKey:@"NoNodes"];
    }
    
    [userDefault setObject:[NSNumber numberWithInt:Difficulty] forKey:@"Difficulty"];
    [userDefault setObject:[NSNumber numberWithInt:starNum] forKey:@"Star"];
    [userDefault setObject:[NSNumber numberWithInt:dctBeatedNum] forKey:@"DctBeated"];    
    [userDefault setObject:[NSNumber numberWithBool:showReport] forKey:@"showReport"];   
    [userDefault setObject:[NSNumber numberWithBool:gameCenterOff] forKey:@"gameCenterOFF"];
    [userDefault setObject:[NSNumber numberWithBool:pending] forKey:@"pending"];
    [userDefault setObject:[NSNumber numberWithInt:actualStep] forKey:@"actualStep"];
    [userDefault setObject:[NSNumber numberWithInt:stageNum] forKey:@"stageNum"];        
    [userDefault setObject:[NSNumber numberWithInt:NextTree_NoNodes] forKey:@"nextTree"];
    [userDefault setObject:[NSNumber numberWithInt:lastClearedStage] forKey:@"lastClearedStage"];        
    [userDefault setObject:[NSNumber numberWithInt:curStageNum] forKey:@"currentStage"];        
    [userDefault setObject:[NSNumber numberWithInt:characterLevel] forKey:@"characterLevel"];        
    [userDefault setObject:[NSNumber numberWithInt:consecutiveWinCounter] forKey:@"consecutiveWinCounter"];            
    [userDefault setObject:[NSNumber numberWithInt:consecutiveWinCounter] forKey:@"consecutiveLoseCounter"];                
    [userDefault setObject:[NSNumber numberWithBool:retrying] forKey:@"retrying"];    
    
    [userDefault setObject:[NSNumber numberWithBool:firstTime] forKey:@"first"];
    [userDefault setObject:registeredUserName forKey:@"registeredUserName"];    
    [userDefault setObject:registeredUserID forKey:@"registeredUserID"];        
    [userDefault setObject:[NSNumber numberWithInt:sqlVersionTag] forKey:@"sqlVersion"];        
    [userDefault setObject:[NSNumber numberWithBool:autoReporting] forKey:@"atReport"];
    [userDefault synchronize];
}//end of saveVariables


-(void)pendingCheck_AND_update
{
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH , 0ul);
    
    dispatch_async(queue, ^(void) {

        [ReachabilityUpdator updateServerAvailability];
        
        if(serverAvailable)
        {
        
            if([registeredUserID length] == 0)
            {
                
                SubmissionRequestor *rq = [[[SubmissionRequestor alloc] init] autorelease];
                [rq getRegisteredID];            
            }
            
        if(pending)
        {

            
                [self.menu submitPendingRecords];        
        }//end of if pending
        
        }
    });
    
}


#pragma mark Navigation control methods
-(void)activateGameView
{
    [self.menu callGaneViewController];
}

-(void)updateReportTreeID
{
    [self.menu update_StrategyTreeID];
}

#pragma mark UINavigation Delegate
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
    viewTagNum = viewController.view.tag;
    
    if(viewController.view.tag == Tag_GamView)
    {
        [self activateGameView];
        if(reporting)
        {
            
            reporting = NO;
            [self.menu showReportDoneAlert];
        }
    }
    else if (viewController.view.tag == Tag_StrategyReportView)
    {
        [self updateReportTreeID];
        reporting = YES;
    }
    else if(viewController.view.tag == Tag_MenuView)
    {
        if(gameCenterOff == NO)
            [self.menu authenticateUser];
    }
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewTagNum = viewController.view.tag;
    
}

@end
