<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once 'code/mdetect.php';
    $uagent_obj = new uagent_info();
    if ($uagent_obj->isTierIphone == $uagent_obj->false) 
    {
    header ("Location:../index.php");
    }
    
    require_once "../fb/fb_db.php"
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>Setsuden Mobile</title>
<script type="text/javascript" src="src/iscroll.js"></script>
<script type="text/javascript">
var myScroll;

function loaded() {
	myScroll = new iScroll('wrapper', {
		snap: true,
		momentum: false,
		hScrollbar: false,
		onScrollEnd: function () {
			document.querySelector('#indicator > li.active').className = '';
			document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
		}
	 });
}
document.addEventListener('DOMContentLoaded', loaded, false);
</script>

<style type="text/css" media="all">
body,ul,li {
	padding:10px;
	margin:0;
}

body {
	font-size:12px;
	-webkit-user-select:none;
    -webkit-text-size-adjust:none;
	font-family:helvetica;
}

#wrapper {
	width:300px;
	height:200px;
	top:45px; bottom:48px; left:0;
	float:left;
	position:relative;	/* On older OS versions "position" and "z-index" must be defined, */
	z-index:1;			/* it seems that recent webkit is less picky and works anyway. */
	overflow:hidden;

	background:#aaa;
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
	-o-border-radius:10px;
	border-radius:10px;
	background:#e3e3e3;
}

#scroller {
	width:2700px;
	height:100%;
	float:left;
	padding:0;
}

#scroller ul {
	list-style:none;
	display:block;
	float:left;
	width:100%;
	height:100%;
	padding:0;
	margin:0;
	text-align:left;
}

#scroller li {
	-webkit-box-sizing:border-box;
	-moz-box-sizing:border-box;
	-o-box-sizing:border-box;
	box-sizing:border-box;
	display:block; float:left;
	width:300px; height:360px;
	text-align:center;
	font-family:georgia;
	font-size:18px;
	line-height:140%;
}

#nav {
        padding-top: 45px;    
        width:300px;
	float:left;
}

#prev, #next {
	float:left;
	font-weight:bold;
	font-size:14px;
	padding:5px 0;
	width:80px;
}

#next {

	float:right;
	text-align:right;
}

#indicator, #indicator > li {
	display:block; float:left;
	list-style:none;
	padding:0; margin:0;
}

#indicator {
	width:110px;
	padding:12px 0 0 30px;
}

#indicator > li {
	text-indent:-9999em;
	width:8px; height:8px;
	-webkit-border-radius:4px;
	-moz-border-radius:4px;
	-o-border-radius:4px;
	border-radius:4px;
	background:#ddd;
	overflow:hidden;
	margin-right:4px;
}

#indicator > li.active {
	background:#888;
}

#indicator > li:last-child {
	margin:0;
}

#header {
	position:absolute;
	top:0; left:0;
	width:100%;
	height:45px;
	line-height:45px;
	background-color:#555555;/*#d51875*/
	background-image:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0, #555455/*#fe96c9*/), color-stop(0.05, #555555/*#d51875*/), color-stop(1, #111111/*#7b0a2e*/));
	background-image:-moz-linear-gradient(top, #aaaaaa/*#ffffff*/, #888888/*#dddddd*/ 5%, #234567/*#777777*/);/*(top, #fe96c9, #d51875 5%, #7b0a2e);*/
	background-image:-o-linear-gradient(top, #555455, #555555 5%, #432432/*#fe96c9, #d51875 5%, #7b0a2e*/);
	padding:0;
	color:#eee;
	font-size:20px;
	text-align:center;
}

#header a {
	color:#f3f3f3/*#f3f3f3*/;
	text-decoration:none;
	font-weight:bold;
	text-shadow:0 -1px 0 rgba(0,0,0,0.5);
}

#footer {
	position:absolute;
	bottom:0; left:0;
	width:100%;
	height:58px;
	background-color:#222;
	background-image:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0, #999), color-stop(0.02, #666), color-stop(1, #222));
	background-image:-moz-linear-gradient(top, #999, #666 2%, #222);
	background-image:-o-linear-gradient(top, #999, #666 2%, #222);
	padding:0;
	border-top:1px solid #444;
}

#footer span {
	padding: 29px 3px 3px;
	float: left;
        text-align: center;
	width: 54px;
	color: #fff;
	font-weight: bold;
	font-size: 10px;
	text-decoration: none;
	background: url("img/footer-buttons.png") no-repeat -2px -50px;
}

 #footer span.selected {
	padding: 29px 3px 3px;
	float: left;
        text-align: center;
	width: 54px;
	color: #fff;
	font-weight: bold;
	font-size: 10px;
	text-decoration: none;
	background: url("img/footer-buttons.png") no-repeat -2px 0px;
}

#footer a.active {
	padding-left: 0;
	padding-right: 0;
	color: #fff;
}
#footer a {
	padding-left: 0;
	padding-right: 0;
	color: #fff;
        border-style: none;
}
</style>
</head>
<body>