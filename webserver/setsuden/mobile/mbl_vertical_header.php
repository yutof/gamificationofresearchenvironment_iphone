<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once 'code/mdetect.php';
    $uagent_obj = new uagent_info();
    if ($uagent_obj->isTierIphone == $uagent_obj->false) 
    {
    header ("Location:../index.php");
    }
    
    require_once "../fb/fb_db.php"
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>Setsuden Mobile</title>

<link rel="stylesheet" type="text/css" href="src/scrollbar.css">
<script type="application/javascript" src="src/iscroll.js"></script>
<script type="text/javascript">
var myScroll;
function loaded() {myScroll = new iScroll('wrapper', { scrollbarClass: 'myScrollbar' });}
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', loaded, false);
</script>

<style type="text/css" media="all">
body,ul,li {
	padding:0;
	margin:0;
	border:0;
}

body {
	font-size:12px;
	-webkit-user-select:none;
    -webkit-text-size-adjust:none;
	font-family:helvetica;
}

#header {
	position:absolute;
	top:0; left:0;
	width:100%;
	height:45px;
	line-height:45px;
	background-color:#555555;/*#d51875*/
	background-image:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0, #555455/*#fe96c9*/), color-stop(0.05, #555555/*#d51875*/), color-stop(1, #111111/*#7b0a2e*/));
	background-image:-moz-linear-gradient(top, #aaaaaa/*#ffffff*/, #888888/*#dddddd*/ 5%, #234567/*#777777*/);/*(top, #fe96c9, #d51875 5%, #7b0a2e);*/
	background-image:-o-linear-gradient(top, #555455, #555555 5%, #432432/*#fe96c9, #d51875 5%, #7b0a2e*/);
	padding:0;
	color:#eee;
	font-size:20px;
	text-align:center;

        /*
	position:absolute;
	top:0; left:0;
	width:100%;
	height:45px;
	line-height:45px;
	background-image:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0, #fe96c9), color-stop(0.05, #d51875), color-stop(1, #7b0a2e));
	background-image:-moz-linear-gradient(top, #fe96c9, #d51875 5%, #7b0a2e);
	background-image:-o-linear-gradient(top, #fe96c9, #d51875 5%, #7b0a2e);
	padding:0;
	color:#eee;
	font-size:20px;
	text-align:center;
        */
}


#header a {
	color:#f3f3f3/*#f3f3f3*/;
	text-decoration:none;
	font-weight:bold;
	text-shadow:0 -1px 0 rgba(0,0,0,0.5);    
    /*
	color:#f3f3f3;
	text-decoration:none;
	font-weight:bold;
	text-shadow:0 -1px 0 rgba(0,0,0,0.5);*/
}

/*
#footer {
	position:absolute;
	bottom:0; left:0;
	width:100%;
	height:48px;
	background-image:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0, #999), color-stop(0.02, #666), color-stop(1, #222));
	background-image:-moz-linear-gradient(top, #999, #666 2%, #222);
	background-image:-o-linear-gradient(top, #999, #666 2%, #222);
	padding:0;
	border-top:1px solid #444;
}
*/

#footer {
	position:absolute;
	bottom:0; left:0;
	width:100%;
	height:58px;
	background-color:#222;
	background-image:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0, #999), color-stop(0.02, #666), color-stop(1, #222));
	background-image:-moz-linear-gradient(top, #999, #666 2%, #222);
	background-image:-o-linear-gradient(top, #999, #666 2%, #222);
	padding:0;
	border-top:1px solid #444;
}

#footer span {
	padding: 29px 3px 3px;
	float: left;
        text-align: center;
	width: 54px;
	color: #fff;
	font-weight: bold;
	font-size: 10px;
	text-decoration: none;
	background: url("img/footer-buttons.png") no-repeat -2px -50px;
}

 #footer span.selected {
	padding: 29px 3px 3px;
	float: left;
        text-align: center;
	width: 54px;
	color: #fff;
	font-weight: bold;
	font-size: 10px;
	text-decoration: none;
	background: url("img/footer-buttons.png") no-repeat -2px 0px;
}

#footer a.active {
	padding-left: 0;
	padding-right: 0;
	color: #fff;
}
#footer a {
	padding-left: 0;
	padding-right: 0;
	color: #fff;
        border-style: none;
}




#wrapper {
	position:absolute; z-index:1;
	top:45px; bottom:58px; left:0;
	width:100%;
	background:#555;
	overflow:auto;
}

#scroller {
	position:relative;
/*	-webkit-touch-callout:none;*/
	-webkit-tap-highlight-color:rgba(0,0,0,0);

	float:left;
	width:100%;
	padding:0;
}

#scroller ul {
	position:relative;
	list-style:none;
	padding:0;
	margin:0;
	width:100%;
	text-align:left;
}

#scroller li {
	padding:0 10px;
	height:40px;
	line-height:40px;
	border-bottom:1px solid #ccc;
	border-top:1px solid #fff;
	background-color:#fafafa;
	font-size:14px;
}

.strtgy_detail{
	padding:0 10px;
	min-height:340px;
	background-color:#fafafa;
	font-size:14px;    
}

#scroller_2 li {
	padding:0 10px;
	height:40px;
	line-height:100px;
	border-bottom:1px solid #ccc;
	border-top:1px solid #fff;
	background-color:#fafafa;
	font-size:14px;
}

#scroller li > a {display:block;}
.li_column1{
    width: 120px;
    float: left;
}
.ln_column2{
    float: left;
/*    width: 80px;*/
}
.li_column3
{float: right;
 margin-top: -40px;
/* width: 50px;*/

}

.ctrbtr_role
{
float: left;    
font-family: Georgia1, Georgia, serif;
font-size: 1.1em;
}
.ctrbtr_name
{
    float: right;
    font-family: Comic Sans MS, Comic Sans MS5, cursive;
    font-weight: bold;
    
}
</style>
</head>
<body>

